
// const s3utils = require('./utils/s3utils');
// const jp = require('jsonpath');

// module.exports = async () => {
//   const supplierConfig = await getConfigMaps();
//   const ConfigMap = {}
//   ConfigMap["MASTER"] = {
//     productIdentifier: "innerEAN",
//     productCodeIdentifier: "p_productCode"
//   }
//   ConfigMap["SUPPLIERS"] = supplierConfig;
//   ConfigMap["SUPPLIERS"]["MASTER"] = {
//     productLoadQueryString: "$['store'][*]",
//     productIdQueryString: "$['store'][*]['innerEAN']",
//     productIdentifier: "innerEAN"
//   }
//   ConfigMap["StatusCodes"] = {
//     OK: 0,
//     ERROR: 1,
//   }
//   return ConfigMap;
// }


// const getConfigMaps = async () => {
//   const mapBucket = process.env.mapBucket
//   const supplierConfig = {};
//   const params = {
//     bucket: mapBucket,
//     prefix: "supplierConfigs"
//   }
//   const listContent = await s3utils.s3List(params)
//   const list = jp.query(listContent.Contents, '$[*].Key')

//   for (let i = 0; i < list.length; i++) {
//     const key = list[i]
//     supplierFeed = key.split('/')[1]
//     if (supplierFeed !== '') {
//       try {
//         const s3Response = await s3utils.s3Get({ bucket: mapBucket, key: list[i] });
//         const supplierResponse = s3Response.Body.toString();
//         const supplierObject = JSON.parse(supplierResponse);
//         supplier = Object.keys(supplierObject)[0]
//         supplierConfig[supplier] = supplierObject[supplier]

//       } catch (err) {
//         console.log(`Error: ${supplierFeed} invalid, skipping import`)
//         console.log(`Debug: ${err}`)
//       }
//     }
//   }

//   return supplierConfig;

// }