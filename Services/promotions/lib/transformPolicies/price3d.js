module.exports.price3d = (params) => {
  let response ;
  var numNum = +params.value;
  try {
    if (isNaN(numNum))
    {
      response = 0;
    }
    else
    {
      response = numNum.toFixed(3);
    }

  } catch (err) {
    console.log(err)
    response = null
  }
  return response
}
