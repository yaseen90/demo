
module.exports.stddate = (params) => {
  // Updated with the correct 'isoDate' switch to set the output
  let response;
  let responseDate;
  let isDate = params.value;
  let bestwayDate = /[0-9]{7,8}[A-Z]{2}/;
  let normalDate = /[0-9]{7,8}/;
  let badDate = /[0]{8,12}/;
  // check for known formats that will not parse as dates
  // BESTWAY
  // Bestway date format is 101219XX. Leading zero chars are not included
  // so 1st will come as 11219XX. Letters do not have known context
  // Extract the numbers, add leading zero and then format.
  if (badDate.test(isDate)) {
    response = null;
  } else if (bestwayDate.test(isDate)) {
    isDate = isDate.match(/[0-9]{7,8}/)
    isDate = isDate[0].padStart(8, "0")
    isDate = isDate.substring(4, 8) + "-" + isDate.substring(2, 4) + "-" + isDate.substring(0, 2)
    responseDate = new Date(isDate)
    response = responseDate.toISOString()
  } else if (normalDate.test(isDate)) {
    isDate = isDate.padStart(8, "0")
    isDate = isDate.substring(4, 8) + "-" + isDate.substring(2, 4) + "-" + isDate.substring(0, 2)
    responseDate = new Date(isDate)
    response = responseDate.toISOString()
  } else {
    response = null;
  }
  return response
}
