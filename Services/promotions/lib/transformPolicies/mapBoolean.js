const booleanMap = {
  "True": true,
  "False": false,
  "Y": true,
  "N": false,
  "0": false,
  "1": true
}
module.exports.mapBoolean = (params) => {
  let response;
  const value = params.value;

  try {
    value = booleanMap.hasOwnProperty(params.value) ? booleanMap[params.value] : false
  } catch (err) {
    console.log(err)
    return params.value
  }
  return response
}
