
const rulesLoader = require('../../lib/rules/load');

module.exports.getRules = (params) => {
  const supplierName = params.supplierName;
  const bucket = params.bucket;
  let response;
  try {
    const supplierRuleFile = `maps/promotions/${supplierName}.csv`;
    response = rulesLoader.rulesLoaderS3({ bucket, mapKey: supplierRuleFile });
  } catch (err) {
    console.log(err);
    response = false;
  }

  return response
}
