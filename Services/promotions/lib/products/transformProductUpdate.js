
module.exports.transformUpdate = (order) => {
    let unTransformedOrder = {}
    for (var key in order) {
        unTransformedOrder[key] = order[key].mapped;
    }
    unTransformedOrder.supplierName = order.supplierName;
    unTransformedOrder.supplierId = order.supplierId
    return unTransformedOrder;
}