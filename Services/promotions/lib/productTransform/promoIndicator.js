// Checks to see if a promoRSP price exists and if it is a valid number
// if so, then p_priceType is set to be the appropriate value.
// Additional logic may be required for situations where the flag is set already


module.exports.promoIndicator = (params) => {
  const product = params.value
  try {
    let promoRSP
    product.p_priceType = { transformed: "R", policy: "promoIndicator", mapped: "R"}
    if (product.hasOwnProperty("p_promoRSP")) {
      promoRSP = product.p_promoRSP.transformed
      if (isNaN(parseInt(promoRSP))) {
        product.p_priceType = { transformed: "R", policy: "promoIndicator", mapped: "R" }
      } else {
        product.p_priceType = { transformed: "P", policy: "promoIndicator", mapped: "P"}
      }
    }

    if (product.hasOwnProperty("appliedFunctions")) {
      product.appliedFunctions.push("promoIndicator")
    }else {
      product.appliedFunctions = ["promoIndicator"]
    }
} catch(err) {
  console.log(err)

}
  return product
}
