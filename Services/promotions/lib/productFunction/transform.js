const _ = require('underscore')
const productTransformFunctions = require('../../lib/productTransform')
const productLib = require('../../lib/products');
//Transformation Policies
const tPolicies = require('../../lib/transformPolicies');
const jobs = require('../../lib/jobs');
const dynamoDBUtils = require('../../utils/dynamoDBUtils');

// Only required for redundancy check
module.exports.transform = async (params) => {
    const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE
    const bucket = process.env.bucket;
    const message = params
    const product = message.product;
    const customerName = message.customerName;
    const supplierName = message.supplierName;
    const supplierId = message.supplierId;
    const jobId = message.jobId;
    const jobCreatedAt = message.jobCreatedAt;
    const matchAttributes = message.matchAttributes;
    const configMap = message.config
    // Product Transform Attribute
    const rules = await productLib.getRules({ supplierName, bucket })
    if (rules === false) {
        const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: "Rules Not Loaded"})
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: "Rules not loaded",
            }, null, 2),
        }
    }
    let transformedProduct = attributeTransform({ product: product, rules })
    transformedProduct.supplierName = supplierName
    transformedProduct.supplierId = supplierId
    //Transform Product Record
    // const supplierConfigMap = configMap.SUPPLIERS[supplierName]
    let supplierFunctions = ["passthrough"]
    // console.log("config map",configMap.transform);
    if (configMap.hasOwnProperty("transform") && Array.isArray(configMap.transform)) {
        supplierFunctions = configMap.transform
    }
    let responseProduct = transformedProduct
    for (let i = 0; i < supplierFunctions.length; i++) {
        const appliedFunctionName = supplierFunctions[i]
        const appliedFunction = productTransformFunctions[appliedFunctionName]
        responseProduct = appliedFunction({ value: responseProduct })

    }
    const isRedundant = await checkRedundantObject({ responseProduct, customerName, DYNAMODB_TABLE, matchAttributes })
    

    message.product = responseProduct;

    return {
        success:true,
        statusCode: 200,
        body: message,
    }

}


function attributeTransform(params) {

    const rules = params.rules
    const product = params.product;
    const defaultPolicy = "passthrough";
    let transformedProduct = {};
    let transformedValue;
    delete product.supplierId;
    delete product.supplierName;
    delete product.appliedFunctions;
    delete product.p_vatRate;
    delete product.p_prePricedFlag;
    Object.keys(product).forEach(function (key, index) {
        let rule = rules.find(r => r.destField === key)
        const policiesString = rule && rule.policy ? rule.policy : defaultPolicy;
        const policies = policiesString.split('|')
        let prodValue = product[key]
        let appliedPolicies = []
        for (let i = 0; i < policies.length ; i++) {
            let policy = policies[i]

            const transformPolicy = tPolicies[policy];
            transformedValue = transformPolicy({ value: prodValue })
            if (transformedValue !== null) {
                prodValue = transformedValue
                appliedPolicies.push(policy)
            } else {
                break;
            }
    
        }
        if (transformedValue == null) {
            transformedProduct[key] = {
                mapped: product[key],
                appliedPolicies
            }
        } else {
            transformedProduct[key] = {
                mapped: product[key],
                transformed: transformedValue,
                appliedPolicies
            }
        }
        //check to see if returned value is a null - If so do not create transformed
    })

    return transformedProduct
}



const checkRedundantObject = async (params) => {
    let response = false

    const customerName = params.customerName;
    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    const responseProduct = params.responseProduct;
    const innerEAN = responseProduct.innerEAN.transformed
    const supplierName = responseProduct.supplierName
    const p_productCode = responseProduct.p_productCode.transformed
    const matchAttributes = params.matchAttributes
    const subsetParsedProduct = _.pick(responseProduct, matchAttributes);

    try {
        const dbProduct = await dynamoDBUtils.get({ TableName: DYNAMODB_TABLE, Key: { customerName, innerEAN } })
        const record = dbProduct.Item
        if (dbProduct.hasOwnProperty("Item") && record.hasOwnProperty("orders")) {
            const orderIndex = record.orders.findIndex(order => { return (order.supplierName === supplierName && order.p_productCode.transformed === p_productCode) })
            if (orderIndex !== -1) {
                const subsetDBProduct = _.pick(record.orders[orderIndex], matchAttributes);
                response = _.isEqual(subsetDBProduct, subsetParsedProduct)
            }
        }
    } catch (err) {
        console.log(err)
    }
    return response
}
