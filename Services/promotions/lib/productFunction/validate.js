const s3Utils = require("../../utils/s3utils");
const productLib = require('../../lib/products');
const aws = require('aws-sdk');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });

//Config Maps

module.exports.validate = async (params) => {
    const configMap = params.config
    const failBucket = process.env.bucket;
    const message = params;
    const product = message.product;
    const supplierName = message.supplierName
    const customerName = message.customerName
    const jobId = message.jobId;
    const jobCreatedAt = message.jobCreatedAt;
    // const supplierConfigMap = configMap.SUPPLIERS
    const productIdentifier = configMap.productIdentifier;
    const textField = configMap.textField;
    const mandatory = configMap.mandatory;
    const isValid = productLib.validateProduct({ product, textField, productIdentifier, mandatory });
    // Ideally sohuld be try catch exception block
    if (isValid.success === false) {
        let key = `Failed Product/${customerName}/${supplierName}/${jobId}`;
        product.message = isValid.message;
        let failProduct;
        try {
            const s3GetParams = { Bucket: failBucket, Key: key }
            const s3Response = await s3.getObject(s3GetParams).promise();
            failProduct = JSON.parse(s3Response.Body)
        } catch (error) {
            failProduct = {
                jobId,
                product: [],
                jobCreatedAt
              }
        }
        failProduct.product.push(product)
        const s3UploadParams = { bucket: failBucket, key: key, content: JSON.stringify(failProduct) }
        const uploadResponse = await s3Utils.s3Upload(s3UploadParams)
        console.log("product is invalid");
        // Add invalid handler
        return {
            success: false,
            statusCode: 500,
            body: "product is invalid",
        }
    }
    return {
        success: true,
        statusCode: 200,
        body: message,
    }

}
