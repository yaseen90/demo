const axios = require('axios');

module.exports.getAllByType = async (params) => {
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  const type = params.type
  const apiKey = process.env.DIRECTORY_API_KEY;
  const config = { "x-api-key": apiKey }
  let response
  try {
    const getAllResponse = await axios.get(`${DIRECTORY_BASEURL}/${type}`, { headers: config })
    if (getAllResponse.status !== 200) {
      response = { success: false, records: [], count: '-', status: getAllResponse.status, err: {} }
    }
    response = { success: true, records: getAllResponse.data.records, count: getAllResponse.data.count, status: getAllResponse.status, err: {} }
  } catch (err) {
    response = { success: false, err: err }
  }

  return response
}

module.exports.get = async (params) => {
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  const type = params.type
  const name = params.name
  const apiKey = process.env.DIRECTORY_API_KEY;
  const config = { "x-api-key": apiKey }
  let response
  try {
    const getResponse = await axios.get(`${DIRECTORY_BASEURL}/${type}/${name}`, { headers: config })
    if (getResponse.status !== 200) {
      response = { success: false, record: {}, message: `HTTP response Not ok ${getCustomerResponse.status}` }
    }
    response = { success: true, record: getResponse.data.record }
    if (getResponse.data.record === undefined) {
      response.message = `${type} Not Found`
    }
  } catch (err) {
    response = { success: false, record: {}, message: `HTTP response Not ok ${err.message}` }
  }

  return response
}