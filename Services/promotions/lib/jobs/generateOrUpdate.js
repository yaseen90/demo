const aws = require('aws-sdk');
const uuidv1 = require('uuid/v1');
const sqs = new aws.SQS({ region: "eu-west-1" })

module.exports.generate = (params) => {
  return generateOrUpdate({ ...params, statusCode: '-', status: "init", generate: true });
}

module.exports.update = (params) => {
  return generateOrUpdate({ ...params, generate: false })
}
module.exports.audit = async (params) => {
  const customerName = params.customerName;
  const jobId = params.jobId;
  const jobCreatedAt = params.jobCreatedAt;
  const AuditQueue = process.env.AuditQueue;
  const log = params.message;
  try {
    const SqsParams = {
      MessageBody : JSON.stringify({
        jobId, jobCreatedAt, customerName, log
      }),
      QueueUrl : AuditQueue
    }
    return sqs.sendMessage(SqsParams).promise().then(data => {
      return { success: true }
    })
  } catch (error) {
    console.log(error);
  }
}
const generateOrUpdate = (params) => {
  const UpsertQueue = process.env.UpsertQueue
  let item = (({ active, status, statusCode }) => ({ active, status, statusCode }))(params);
  item.id = params.generate ? uuidv1() : params.jobId;
  item.createdAt = params.generate ? new Date().getTime() : params.createdAt
  item.process = params.hasOwnProperty("jobProcess") ? params.source : "-"
  item.message = params.hasOwnProperty("message") ? params.message : "-"
  // console.log("item",item);
  const SqsParams = {
    MessageBody: JSON.stringify({ item }),
    QueueUrl: UpsertQueue
  }
  return sqs.sendMessage(SqsParams).promise().then(data => {
    return { success: true , item}
  })
}