'use strict';

const handlers = require("./handlers")

module.exports.insertPromotionsHandler = handlers.insert;
module.exports.insertApiPromotionsHandler = handlers.insertApi;
module.exports.getOnePromotionsHandler = handlers.getOne;
module.exports.getAllPromotionsHandler = handlers.getAll;
module.exports.updatePromotionsHandler = handlers.update;
