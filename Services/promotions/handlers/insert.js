//Config Maps
const dynamoDBUtils = require('../utils/dynamoDBUtils');
const productLib = require('../lib/products');
const productFunctions = require("../lib/productFunction");
const jobs = require('../lib/jobs');
const directory = require("../lib/directory");

module.exports.insert = async (event) => {
  const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;
  const messageString = event.Records[0].body
  const parseMessage = JSON.parse(messageString);
  let message = parseMessage.message;
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  let customerName = message.customerName;
  let supplierName = message.supplierName;
  let customerId = message.customerId;
  const response = await directory.getAllByType({ type: "MASTER" })
  const MASTER = response.records[0].ConfigMap.MASTER;
  
  const validate = await productFunctions.validate(message)
  if (!validate.success) {
    let msg = "Product is invalid"
    const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: msg })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: msg })
    return false;
  }
  const parse = await productFunctions.parse(validate.body);
  if (!parse.success) {
    let msg = "some thing went rong with parsing"
    const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: msg })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: msg })
    return false;
  }
  const transform = await productFunctions.transform(parse.body);
  if (!transform.success) {
    let msg = "Attributes Does Not Match"
    const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: msg })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: msg })
    return false
  }
  message = transform.body;
  const transformedProduct = message.product;
  const masterProductIdentifier = MASTER.productIdentifier
  const masterproductCodeIdentifier = MASTER.productCodeIdentifier;
  let productRecord = {}
  productRecord[masterProductIdentifier] = transformedProduct[masterProductIdentifier]["transformed"]
  productRecord.customerName = customerName;
  productRecord.customerId = customerId;
  productRecord = await productLib.addOrAppend({
    DYNAMODB_TABLE,
    productRecord,
    supplierName,
    customerName,
    masterProductIdentifier,
    transformedProduct,
    masterproductCodeIdentifier
  })

  const dynamoDBParams = {
    item: productRecord,
    DYNAMODB_TABLE
  }
  const dynamoDBResponse = await dynamoDBUtils.create(dynamoDBParams);
  console.log("dynamoDBResponse",dynamoDBResponse);
  let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: "ok" })
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "product added",
    }, null, 2),
  }

}
