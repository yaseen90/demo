const aws = require('aws-sdk');
const sqs = new aws.SQS({ region: "eu-west-1" })
const jobs = require("../lib/jobs")
const directory = require("../lib/directory")
const isEmpty = require('../functions/emptyObject');
const valid = require("../validation/productValidation");

module.exports.insertApi = async (event) => {
    const QUEUE_URL = process.env.QueueURL;
    let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : {};
    try {
        requestBody = JSON.parse(requestBody)
    } catch (error) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: error.message
            }, null),
        }
    }
    let errors = valid(requestBody);
    if (isEmpty(errors)) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                error: errors,
                message: "Validations Fail."
            }, null),
        }
    } else {
        const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL;
        const jobProcess = process.env.CUSTOM_DOMAIN + " - insert-api";
        const jobDetails = await jobs.generate({ active: 1, source: jobProcess })
        const jobId = jobDetails.success ? jobDetails.item.id : false
        const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false
        const customerName = requestBody.customerName;
        const getCustomer = await directory.get({ name: customerName, type: "customer" })
        const customer = getCustomer.record;
        if (!getCustomer.success || customer === undefined) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: getCustomer.message
                }, null)
            }
        }
        const supplierName = requestBody.supplierName;
        const getSupplier = await directory.get({ name: supplierName, type: "supplier" })
        const supplier = getSupplier.record;
        if (!getSupplier.success || supplier === undefined) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: getSupplier.message
                }, null)
            }
        }
        const getConfig = await directory.get({ name: supplierName, type: "supplierConfig" })
        const config = getConfig.record;
        if (!getConfig.success || config === undefined) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: getConfig.message
                }, null)
            }
        }
        requestBody.config = config
        requestBody.jobCreatedAt = jobCreatedAt;
        requestBody.jobId = jobId;

        try {
            const SqsParams = {
                MessageBody : JSON.stringify({
                  message:requestBody
                }),
                QueueUrl : QUEUE_URL
              }
              const sqsResponse = await sqs.sendMessage(SqsParams).promise().then(response => response);

        } catch (error) {
            return {
                statusCode: 500,
                body: JSON.stringify({
                    error: "cannot published  " + error
                }, null, 2)
            }
        }


        return {
            statusCode: 200,
            body: JSON.stringify({
                JobId: jobId,
                JobCreatedAt: jobCreatedAt,
                JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
            }, null, 2)
        }
    }
}