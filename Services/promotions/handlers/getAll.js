// Enduser Service to Access Product Records over Paginated API (HTTPS)

const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.getAll = async (event) => {
    const customerName = event.pathParameters.customerName;
    let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : "{}";
    try {
        requestBody = JSON.parse(requestBody)
    } catch (error) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: error.message
            }, null),
        }
    }
    const ExclusiveStartKey = requestBody.hasOwnProperty("ExclusiveStartKey") ? requestBody.ExclusiveStartKey : false
    const Limit = requestBody.hasOwnProperty("Limit") ? requestBody.Limit : 500
    const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;

    const params = {
        TableName: DYNAMODB_TABLE,
        KeyConditionExpression: 'customerName = :customerName',
        ExpressionAttributeValues: {
            ":customerName": customerName
        },
        Limit
    }
    if(ExclusiveStartKey){
        params.ExclusiveStartKey = ExclusiveStartKey
    }
    let response;
    try {
        const queryResponse = await dynamoDBUtils.query(params)
        response = { records: queryResponse.Items, count: queryResponse.Count }
        if (queryResponse.hasOwnProperty("LastEvaluatedKey")) {
            response.LastEvaluatedKey = queryResponse.LastEvaluatedKey;
        }
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({ error: error.message }),
        }
    }
    return {
        statusCode: 200,
        body: JSON.stringify(response, null, 2),
    }
}
