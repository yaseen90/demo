const jobs = require("../lib/jobs")
const directory = require("../lib/directory")
const productLib = require("../lib/products");
const productFunctions = require("../lib/productFunction");
const dynamoDBUtils = require('../utils/dynamoDBUtils');
const isEmpty = require('../functions/emptyObject');
const valid = require("../validation/updateValidation");

module.exports.update = async (event) => {

    const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;

    let productBody = event.hasOwnProperty("body") && event.body !== null ? event.body : {};
    try {
        productBody = JSON.parse(productBody)
    } catch (error) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: error.message
            }, null),
        }
    }
    let errors = valid(productBody);
    if (isEmpty(errors)) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                error: errors,
                message: "Validations Fail."
            }, null),
        }
    }
    const customerName = productBody.customerName;
    const innerEAN = productBody.innerEAN;
    const supplierName = productBody.supplierName;
    const masterproductCodeIdentifier = productBody.masterproductCodeIdentifier;
    const masterproductCodeIdentifierValue = productBody.masterproductCodeIdentifierValue;
    const product = productBody.product;

    const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL;
    const jobProcess = process.env.CUSTOM_DOMAIN + " - update-api";

    const jobDetails = await jobs.generate({ active: 1, source: jobProcess })
    const jobId = jobDetails.success ? jobDetails.item.id : false
    const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false

    const getCustomer = await directory.get({ name: customerName, type: "customer" })
    const customer = getCustomer.record;
    if (!getCustomer.success || customer === undefined) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: getCustomer.message
            }, null)
        }
    }
    const getConfig = await directory.get({ name: supplierName, type: "supplierConfig" })
    const config = getConfig.record;
    if (!getConfig.success || config === undefined) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: getConfig.message
            }, null)
        }
    }
    let productRecord;
    let UntransformedOrder;
    let supplierFound;
    try {
        const dbProduct = await dynamoDBUtils.get({ TableName: DYNAMODB_TABLE, Key: { customerName, innerEAN } })
        productRecord = dbProduct.Item
        if (productRecord === undefined) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: "Can't find product"
                }, null)
            }
        }
        const productOrders = productRecord.orders;
        supplierFound = productOrders.findIndex(obj => {
            const response = (obj.supplierName === supplierName &&
                obj[masterproductCodeIdentifier]["transformed"] === masterproductCodeIdentifierValue)
            if (response === false) { console.log(`${response} >> ${obj[masterproductCodeIdentifier]["transformed"]}`) }
            return response
        })
        if (supplierFound === -1) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: "Can't find Supplier product"
                }, null)
            }
        } else {
            const order = productOrders[supplierFound];
            UntransformedOrder = productLib.transformProductUpdate(order)
        }
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: error.message
            }, null)
        }
    }
    const productIdentifier = config.productIdentifier;
    const textField = config.textField;
    const validate = productLib.validateProductUpdate({ product, productIdentifier, textField });
    if (!validate.success) {
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: validate.message
            }, null)
        }
    }

    const parse = await productFunctions.parse({ jobCreatedAt, jobId, config, supplierName, product });
    if (!parse.success) {
        let msg = "some thing went rong with parsing"
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: msg
            }, null)
        }
    }
    const productData = parse.body.product

    const mergeProducts = { ...UntransformedOrder, ...productData }
    const supplierId = UntransformedOrder.supplierId;
    parse.body.product = mergeProducts;

    const transform = await productFunctions.transform({ ...parse.body, supplierId , customerName});

    const updatedProduct = transform.body.product;
    productRecord.orders[supplierFound] = updatedProduct;

    const dynamoDBParams = {
        item: productRecord,
        DYNAMODB_TABLE
    }
    const dynamoDBResponse = await dynamoDBUtils.create(dynamoDBParams);
    return {
        statusCode: 200,
        body: JSON.stringify({
            JobId: jobId,
            JobCreatedAt: jobCreatedAt,
            JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
        }, null, 2)
    }
}