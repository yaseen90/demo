const aws = require('aws-sdk');
const sns = new aws.SNS({apiVersion: '2010-03-31'});


module.exports.publish = async (params) => {
  return sns.publish(params).promise();
}
