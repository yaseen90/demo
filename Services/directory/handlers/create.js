const snsUtils = require('../utils/snsUtils');
const valid = require("../validations/createValidation");
const isEmpty = require('../functions/emptyObject');
const dynamoDBUtils = require("../utils/dynamoDBUtils");

module.exports.create = async (event) => {
  const RECORD_UPSERT_TOPIC = process.env.RECORD_UPSERT_TOPIC
  const CUSTOM_BASEURL = process.env.CUSTOM_BASEURL
  const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE;
  let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : "{}";
  try {
    requestBody = JSON.parse(requestBody)
  } catch (error) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: error.message
      }, null),
    }
  }
  let statusCode = 200
  let response = { item: {}, success: true, message: "pending" }
  let item = requestBody;
  let supplier = await dynamoDBUtils.getAll({
    TableName: CUSTOMER_DBTABLE,
    FilterExpression: "#itemtype = :itemtype",
    ExpressionAttributeNames: {
      "#itemtype": "type"
    },
    ExpressionAttributeValues: {
      ":itemtype": "supplier"
    }
  })
  let errors = valid(item, supplier);
  if (isEmpty(errors)) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        error: errors,
        message: "Validations Fail."
      }, null),
    }
  } else {
    const publishParams = {
      Message: JSON.stringify({
        item: item
      }),
      TopicArn: RECORD_UPSERT_TOPIC
    }
    try {
      console.log(publishParams)
      const publishResponse = await snsUtils.publish(publishParams)
      console.log(publishResponse);
      response.message = publishResponse
    }
    catch (err) {
      console.log(err)
      response.error = err
      response.success = false
    }

    response.item = `${CUSTOM_BASEURL}/record/${item.type}/${item.name}`


    return {
      statusCode: statusCode,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        response,
      }, null, 2),
    }
  }
}
