const uuidv1 = require('uuid/v1');
const dynamoDBUtils = require('../utils/dynamoDBUtils')

module.exports.upsertSNS = async (event) => {

  let response;
  const DYNAMODB_TABLE = process.env.CUSTOMER_DBTABLE;
  const messageString = event.Records[0].Sns.Message
  const message = JSON.parse(messageString)


  console.log(message);

  let item = (message.item);


  const dbParams = {
    item,
    type: item.type,
    DYNAMODB_TABLE
  }

  console.log(dbParams);
  try {
      response = await dynamoDBUtils.upsert(dbParams)
      console.log(response)
  }
  catch(err) {
    response = err
    console.log(err);
  }

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      response,
    }, null, 2),
  }

}
