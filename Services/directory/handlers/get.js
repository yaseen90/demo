'use strict'

const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.get = async (event) => {


    const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE;
    const type = event.pathParameters.type;
    let name = event.pathParameters.name ;


    const params = {
        TableName: CUSTOMER_DBTABLE,
        Key: {name, type }
    }

    let dynamoDBResponse = "";
    try {
       dynamoDBResponse = await dynamoDBUtils.get(params);
    } catch (err) {
      return {
        statusCode: 500,
        body: JSON.stringify({
          error: err,
        }, null, 2),
      }

    }

    const record = dynamoDBResponse.Item
    const statusCode = 200;
    return {
      statusCode: statusCode,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        record
      }, null, 2),
    }

};
