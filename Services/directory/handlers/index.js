module.exports.get = require('./get').get;
module.exports.getAllByType = require('./getAllByType').getAllByType;
module.exports.create = require('./create').create;
module.exports.update = require('./update').update;
module.exports.upsertSNS = require('./upsertSNS').upsertSNS;
