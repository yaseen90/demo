const snsUtils = require('../utils/snsUtils');
const uuidv1 = require('uuid/v1');
const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.update = async (event) => {

  let dynamoDBResponse = "";

  const RECORD_UPSERT_TOPIC = process.env.RECORD_UPSERT_TOPIC
  const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE;
  const CUSTOM_BASEURL = process.env.CUSTOM_BASEURL

  const name = event.pathParameters.name;
  const type = event.pathParameters.type;
  let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : "{}";
  try {
    requestBody = JSON.parse(requestBody)
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: error.message
      }, null),
    }
  }
  let statusCode = 200
  let response = { item: {}, success: true, message: "pending" }
  let item = requestBody;
  item.name = name;
  item.type = type


  const dbParams = {
    TableName: CUSTOMER_DBTABLE,
    Key: { name, type }
  }
  try {
    dynamoDBResponse = await dynamoDBUtils.get(dbParams);
  } catch (err) {

    return {
      statusCode: 500,
      body: JSON.stringify({
        error: err,
      }, null, 2),
    }

  }

  if (Object.keys(dynamoDBResponse).length === 0) {
    response.success = false
    response.message = "Not found, please create before update"
    console.log(response)
    return {
      statusCode: 404,
      body: JSON.stringify({
        response
      }, null, 2),
    }

  }

  const updatedItem = { ...dynamoDBResponse.Item, ...item }

  console.log(updatedItem)
  const publishParams = {
    Message: JSON.stringify({
      item: updatedItem
    }),
    TopicArn: RECORD_UPSERT_TOPIC
  }


  try {

    const publishResponse = await snsUtils.publish(publishParams)
    response.message = publishResponse
  }
  catch (err) {
    console.log(err)
    response.error = err
    response.success = false
  }

  response.item = `${CUSTOM_BASEURL}/record/${item.type}/${item.name}`


  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      response,
    }, null, 2),
  }

}
