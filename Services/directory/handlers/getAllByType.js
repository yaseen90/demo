const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.getAllByType = async (event) => {
  let ExclusiveStartKey = false
  const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE;





  if (event.queryStringParameters && event.queryStringParameters.LastEvaluatedKey) {
    ExclusiveStartKey = JSON.parse(event.queryStringParameters.LastEvaluatedKey)
  }

  console.log(ExclusiveStartKey)
  const type = event.pathParameters.type;

  VALID_TYPES = ["customer", "supplier","supplierConfig","MASTER","admin"];


  if (!(VALID_TYPES.includes(type))){
    return {
      statusCode: 500,
      body: JSON.stringify({
        error: "Invalid type",
      }, null, 2),
    }
  }

  const params = {
    TableName: CUSTOMER_DBTABLE,
    FilterExpression: "#itemtype = :itemtype",
    ExpressionAttributeNames: {
    "#itemtype": "type"
    },
    ExpressionAttributeValues: {
      ":itemtype": type
    }
  }

  if (ExclusiveStartKey) {
    params.ExclusiveStartKey = ExclusiveStartKey
  }
  const queryResponse = await dynamoDBUtils.getAllPage(params)
  let response = { records: queryResponse.Items, count: queryResponse.Count}
  if (queryResponse.hasOwnProperty("LastEvaluatedKey")) {
    response.LastEvaluatedKey = queryResponse.LastEvaluatedKey;
  }
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify(response, null, 2),
  }

}
