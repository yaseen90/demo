'use strict';


const handlers = require('./handlers');
module.exports.getRecordHandler = handlers.get
module.exports.getAllByTypeRecordHandler = handlers.getAllByType
module.exports.createRecordHandler = handlers.create
module.exports.updateRecordHandler = handlers.update
module.exports.upsertSNSRecordHandler = handlers.upsertSNS
