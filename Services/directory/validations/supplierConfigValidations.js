module.exports = function (item) {
    let errors = {};

    if (!item.hasOwnProperty("productLoadQueryString")) { errors.productLoadQueryString = "productLoadQueryString is Required for type supplierConfig" }
    else if (typeof (item.productLoadQueryString) !== "string") { errors.productLoadQueryString = "productLoadQueryString is Required as a String" }
    else if (item.productLoadQueryString.trim() === "") { errors.productLoadQueryString = "productLoadQueryString value is required" }
    else if (typeof (item.productIdQueryString) !== "string") { errors.productIdQueryString = "productIdQueryString is Required as a String" }
    else if (item.productIdQueryString.trim() === "") { errors.productIdQueryString = "productIdQueryString value is required" }
    if (!item.hasOwnProperty("productIdentifier")) { errors.productIdentifier = "productIdentifier is Required for type supplierConfig" }
    else if (typeof (item.productIdentifier) !== "string") { errors.productIdentifier = "productIdentifier is Required as a String" }
    else if (item.productIdentifier.trim() === "") { errors.productIdentifier = "productIdentifier value is required" }
    if (!item.hasOwnProperty("textField")) { errors.textField = "textField is Required for type supplierConfig" }
    if (!item.hasOwnProperty("convert")) { errors.convert = "convert is Required for type supplierConfig" }
    else if (typeof (item.convert) !== "string") { errors.convert = "convert is Required as a String" }
    else if (item.convert.trim() === "") { errors.convert = "convert value is required" }
    if (!item.hasOwnProperty("headers")) { errors.headers = "headers is Required for type supplierConfig" }
    if (!item.hasOwnProperty("filter")) { errors.filter = "filter is Required for type supplierConfig" }

    return errors;
}