const customerValidations = require("./customerValidations");
const supplierValidations = require("./supplierValidations")
const supplierConfigValidations = require("./supplierConfigValidations")


module.exports = function (item, data = []) {
    let type = ["customer", "supplier", "supplierConfig"];
    let errors = {};

    if (!item.hasOwnProperty("type")) {
        errors.type = "type is Required"
        return errors;
    }
    if (!type.includes(item.type)) { errors.type = `Invalid record type; valid options: ${type}` }
    if (!item.hasOwnProperty("name")) { errors.name = "name is Required" }
    else if (typeof (item.name) !== "string") { errors.name = "name is Required as a String" }
    else if (item.name.trim() === "") { errors.name = "name value is required" }
    switch (item.type) {
        case "supplier":
            errors = supplierValidations(item)
            break;
        case "customer":
            errors = customerValidations(item, data);
            break;
        case "supplierConfig":
            errors = supplierConfigValidations(item)
            break;
        default:
            return errors;
    }
    return errors;
}