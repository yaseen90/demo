
module.exports = function (item) {
    let errors = {};
    let accessType = ["ftp", "sftp","https"];
    let extArr = ["csv$", "xml$", "txt$"]

    if (!item.hasOwnProperty("accessType")) { return errors.accessType = "accessType is Required" }
    else if (!accessType.includes(item.accessType)) { return errors.accessType = `Invalid record accessType; valid options: [${accessType}]` }
    if (!item.hasOwnProperty(`${item.accessType}Hostname`)) { errors[`${item.accessType}Hostname`] = `${item.accessType}Hostname is Required` }
    else if (typeof (item[`${item.accessType}Hostname`]) !== "string") { errors[`${item.accessType}Hostname`] = `${item.accessType}Hostname is Required as a String` }
    else if (item[`${item.accessType}Hostname`].trim() === "") { errors[`${item.accessType}Hostname`] = `${item.accessType}Hostname value is required` }
    if (!item.hasOwnProperty(`${item.accessType}Prefix`)) { errors[`${item.accessType}Prefix`] = `${item.accessType}Prefix is Required` }
    else if (typeof (item[`${item.accessType}Prefix`]) !== "string") { errors[`${item.accessType}Prefix`] = `${item.accessType}Prefix is Required as a String` }
    else if (item[`${item.accessType}Prefix`].trim() === "") { errors[`${item.accessType}Prefix`] = `${item.accessType}Prefix value is required` }
    if (!item.hasOwnProperty(`${item.accessType}ProductFile`)) { errors[`${item.accessType}ProductFile`] = `${item.accessType}ProductFile is Required` }
    else if (typeof (item[`${item.accessType}ProductFile`]) !== "string") { errors[`${item.accessType}ProductFile`] = `${item.accessType}ProductFile is Required as a String` }
    else if (item[`${item.accessType}ProductFile`].trim() === "") { errors[`${item.accessType}ProductFile`] = `${item.accessType}ProductFile value is required` }
    else if (item[`${item.accessType}ProductFile`].lastIndexOf(".") === 4) {
        let ext = item[`${item.accessType}ProductFile`].split('.').pop();
        if (!extArr.includes(ext)) {
            errors[`${item.accessType}ProductFile`] = `${item.accessType}ProductFile extension must be ${extArr}`
        }
    }
    if (!item.hasOwnProperty(`${item.accessType}Port`)) { errors[`${item.accessType}Port`] = `${item.accessType}Port is Required` }
    else if (typeof (item[`${item.accessType}Port`]) !== "string") { errors[`${item.accessType}Port`] = `${item.accessType}Port is Required as a String` }
    else if (item[`${item.accessType}Port`].trim() === "") { errors[`${item.accessType}Port`] = `${item.accessType}Port value is required` }

    return errors;
}