module.exports = function (item, data = []) {
    let errors = {};

    if (!item.hasOwnProperty("activeSuppliers")) { return errors.activeSuppliers = "activeSuppliers is Required for type customer" }
    else if (!Array.isArray(item.activeSuppliers)) { return errors.activeSuppliers = "activeSuppliers is Required in Array Format" }
    if (!item.hasOwnProperty("status")) { return errors.status = "status is Required for type customer" }
    else if (typeof (item.status) !== "boolean") { return errors.status = "status is Required in Boolean" }
    if (!item.hasOwnProperty("supplierConfig")) { return errors.supplierConfig = "supplierConfig is Required" }
    else if (!Array.isArray(item.supplierConfig)) { return errors.supplierConfig = "supplierConfig is Required in Array Format" }
    for (var k = 0; k < item.supplierConfig.length; k++) {
        if (typeof (item.supplierConfig[k]) !== "object") {
            errors.supplierConfig = "supplierConfig must be an Array of Objects";
            break;
        }
    }
    // if (item.activeSuppliers.length !== item.supplierConfig.length) { return errors.activeSuppliersORsupplierConfig = "activeSuppliers Array Length must be equal to supplierConfig Array Length" }
    // else {
    for (let c = 0; c < item.activeSuppliers.length; c++) {
        let check = false;
        for (let index = 0; index < data.length; index++) {
            let element = data[index].name;
            if (element === item.activeSuppliers[c]) {
                check = true;
            }
        }
        if (check === false) {
            errors.supplierName = `supplier ${item.activeSuppliers[c]} Does Not Exist in Database`;
            break;
        }

    }
    // }|
    // for (var i = 0; i < item.activeSuppliers.length; i++) {
    //     // var flage = false;
    //     for (var j = 0; j < item.supplierConfig.length; j++) {
    //         // if (item.activeSuppliers[i] === item.supplierConfig[j].name) {
    //         //     flage = true;
    //         // }
    //         if (item.supplierConfig[j].KeyLocationType !== "s3") {
    //             errors.KeyLocationType = "Key Location Type value must be s3";
    //         }
    //     }
    //     // if (flage === false) {
    //     //     errors.activeSuppliersORsupplierConfig = `active supplier name ${item.activeSuppliers[i]} is Not exist in supplier Config`;
    //     //     break;
    //     // }
    // }

    if (!item.hasOwnProperty("schedule")) { return errors.schedule = "schedule is Required" }
    else if (typeof (item.schedule) !== "object") { return errors.schedule = "schedule is Required as a Object" }
    // let schedule = item.schedule;
    // if (!schedule.hasOwnProperty("running")) { errors.schedule_Running = "running property of schedule is Required" }
    // else if (typeof (schedule.running) !== "boolean") { errors.schedule_Running = "running property of schedule is Required as a Boolean" }
    // if (!schedule.hasOwnProperty("enabled")) { errors.schedule_Enabled = "enabled property of schedule is Required" }
    // else if (typeof (schedule.enabled) !== "boolean") { errors.schedule_Enabled = "enabled property of schedule is Required as a Boolean" }
    // if (!schedule.hasOwnProperty("lastIterationStatus")) { errors.schedule_lastIterationStatus = "lastIterationStatus property of schedule is Required" }
    // else if (typeof (schedule.lastIterationStatus) !== "string") { errors.schedule_lastIterationStatus = "lastIterationStatus property of schedule is Required as a String" }
    // if (!schedule.hasOwnProperty("nextIteration")) { errors.schedule_nextIteration = "nextIteration property of schedule is Required" }
    // else if (typeof (schedule.nextIteration) !== "number") { errors.schedule_nextIteration = "nextIteration property of schedule is Required as a Number" }
    // if (!schedule.hasOwnProperty("lastIteration")) { errors.schedule_lastIteration = "lastIteration property of schedule is Required" }
    // else if (typeof (schedule.lastIteration) !== "number") { errors.schedule_lastIteration = "lastIteration property of schedule is Required as a Number" }
    // if (!schedule.hasOwnProperty("interval")) { errors.schedule_interval = "interval property of schedule is Required" }
    // else if (typeof (schedule.interval) !== "number") { errors.schedule_interval = "interval property of schedule is Required as a Number" }

    return errors;
}