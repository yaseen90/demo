
// const s3utils = require('./utils/s3utils');
// const jp = require('jsonpath');

// module.exports = async () => {
//   const supplierConfig = await getConfigMaps();
//   const ConfigMap = {}
//   ConfigMap["MASTER"] = {
//     productIdentifier: "innerEAN",
//     productCodeIdentifier: "p_productCode"
//   }
//   ConfigMap["SUPPLIERS"] = supplierConfig;
//   ConfigMap["SUPPLIERS"]["MASTER"] = {
//     productLoadQueryString: "$['store'][*]",
//     productIdQueryString: "$['store'][*]['innerEAN']",
//     productIdentifier: "innerEAN"
//   }
//   ConfigMap["StatusCodes"] = {
//     OK: 0,
//     ERROR: 1,
//   }
//   return ConfigMap;
// }


// const getConfigMaps = async () => {
//   const mapBucket = process.env.mapBucket
//   const supplierConfig = {};
//   const params = {
//     bucket: mapBucket,
//     prefix: "supplierConfigs"
//   }
//   const listContent = await s3utils.s3List(params)
//   const list = jp.query(listContent.Contents, '$[*].Key')

//   for (let i = 0; i < list.length; i++) {
//     const key = list[i]
//     supplierFeed = key.split('/')[1]
//     if (supplierFeed !== '') {
//       try {
//         const s3Response = await s3utils.s3Get({ bucket: mapBucket, key: list[i] });
//         const supplierResponse = s3Response.Body.toString();
//         const supplierObject = JSON.parse(supplierResponse);
//         supplier = Object.keys(supplierObject)[0]
//         supplierConfig[supplier] = supplierObject[supplier]

//       } catch (err) {
//         console.log(`Error: ${supplierFeed} invalid, skipping import`)
//         console.log(`Debug: ${err}`)
//       }
//     }
//   }

//   return supplierConfig;

// }


// //    COSTCUTTER: {
// //      productLoadQueryString: "$['costcutter-response']['body']['full-download-response']['products']['product']",
// //      productIdQueryString: "$[*]['inner-barcode']['_text']",
// //      productIdentifier: "inner-barcode",
// //      textField: "_text",
// //      convert: "xml",
// //      filter: false,
// //    },
// //    NISA: {
// //        productLoadQueryString: "$['ocs-b2b-document-response']['body']['document-response']['docResponse']['catalogueResponse']['product']",
// //        productIdQueryString: "$[*]['innerEAN']['_text']",
// //        productIdentifier: "innerEAN",
// //        textField: "_text",
// //        convert: "xml",
// //        headers: "",
// //        filter: false,
// //    },
// //    BESTWAY: {
// //        productLoadQueryString: "$['products']",
// //        productIdQueryString: "$[*]['innerEAN']['_text']",
// //        productIdentifier: "retailer-barcode",
// //        textField: false,
// //        convert: "csv",
// //        filter: false,
// //        headers: [
// //          "product-code","description","product-category-level-1","prodocut-category-level-2","product-category-level-3",
// //          "field6","trading-units-in-case","field8","field9","field10",
// //          "field11","field12","retail-price","vat-code","pos-description",
// //          "field16","field17","consumer-product-end-date","field19","field20",
// //          "cost-price","pack-description","field23","field24","field25",
// //          "field26","field27","field28","field29","field30",
//   //        "promotional-sell-price","field32","supplier-code","retailer-barcode","deletion-flag",
// //          "field36"]
// //    },
// //    BUDGENS: {
// //      convert: "fixed",
// //      productLoadQueryString: "$['products']",
// //      productIdentifier: "ean",
// //      fullWidth: 259,
// //      filter: {operation: "startsWith", string: "D"},
// //      headers: [
// //        {name: "ean", start: 3 , width: 13,  type: "string"},
// //        {name: "sin", start: 16, width: 6,  type: "string"},
// //        {name: "cost-price-effective-date", start: 146, width: 8,  type: "string"},
// //        {name: "promotional-line-indicator", start: 184, width: 1,  type: "string" },
// //        {name: "cost-price", start: 139, width: 3,  type: "string"},
// //        {name: "vat-rate", start: 101, width: 2, type: "string"},
// //        {name: "commodity-budgens-group", start: 179, width: 4, type: "string"},
// //        {name: "case-size", start:22, width: 4, type: "string"},
// //        {name: "unit-weight", start:80, width: 3, type: "string"},
// //        {name: "unit-price-multiplier", start: 90, width: 4, type: "string"},
// //        {name: "unit-price-measure", start:86, width: 4, type: "string"},
// //        {name: "item-blocked", start: 107, width: 1, type: "string"},
// //        {name: "pack-desc", start:80, width:3 }
// //      ],
// //      textField: false,

// //    },

// //  }
// //};
