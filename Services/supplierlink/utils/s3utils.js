
//For S3 read/writes
const aws = require('aws-sdk');
const jp = require('jsonpath');

// For AWS Services
const s3 = new aws.S3({ apiVersion: '2006-03-01' });


module.exports.s3Copy = (params) => {


  const Bucket = params.Bucket;
  const sourceKey = params.sourceKey;
  const exportKey = params.exportKey;

  let copyParams = {
    Bucket,
    CopySource: `${Bucket}/${sourceKey}`,
    Key: exportKey
  }

  return s3.copyObject(copyParams).promise()


}

module.exports.s3Copies = (params) => {


  const sourceBucket = params.sourceBucket;
  const destBucket = params.destBucket;
  const sourceKey = params.sourceKey;
  const destKey = params.destKey;

  let copyParams = {
    destBucket,
    CopySource: `${sourceBucket}/${sourceKey}`,
    Key: destKey
  }

  return s3.copyObject(copyParams).promise()


}


module.exports.put = (params) => {

  // Sample Scheuma: {Body, Bucket, Key, ContentType}
  // upload json
  const   putParam = params
  return s3.putObject(putParam).promise();

}

module.exports.upload =  (params) => {

  // Sample Scheuma: {Body, Bucket, Key, ContentType}
  return s3.upload(params).promise();
}

module.exports.s3Upload = (params) => {

  const content = params.content;
  const bucket = params.bucket;
  const key = params.key;
  // upload json
  const   putParam = {
       Body: content,
       Bucket: bucket,
       Key: key,
       ContentType: "application/json"
     }
  return s3.putObject(putParam).promise();

}

module.exports.s3UploadStream = (params) => {
  // Params Schema: {Body, Bucket, Key}
  return s3.upload(params).promise();

}

module.exports.s3Get = (params) => {
  const bucket = params.bucket;
  const key = params.key;

  const getParams = {
    Bucket: bucket,
    Key: key
  }
  return s3.getObject(getParams).promise();
}


module.exports.s3List = (params) => {
  const bucket = params.bucket;
  const prefix = params.prefix;

  const listParams = {
    Bucket: bucket,
    Prefix: prefix
  }
  return s3.listObjects(listParams).promise();

}

module.exports.s3DeleteFolder = (params) => {
  const bucket = params.bucket;
  const prefix = params.prefix;
  const listParams = {
    Bucket: bucket,
    Prefix: prefix
  }

  return s3.listObjects(listParams).promise().then( data => {
    let Objects = [];
    let files = jp.query(data,'$.Contents[*].Key')
    for (let i=0; i < files.length ; i++){
      Objects.push({Key: files[i]})
    }
    const deleteParams = {
      Bucket: bucket,
      Delete:{
        Objects,
        Quiet: false
      }
    };
    return s3.deleteObjects(deleteParams).promise(data => { return data})
    //return deleteParams
  })
}
//const deleteParams = {
//  Bucket: Bucket,
//  Key: MERGED_PREFIX
//}
//const deleteResponse = await S3.deleteObject(deleteParams).promise().then(data => { return data})
