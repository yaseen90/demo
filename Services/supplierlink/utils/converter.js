// For XML to JSON and JSON to XML
const convert = require('xml-js');
module.exports.isEmpty = isEmpty;
module.exports.xmltojson = xmltojson;
module.exports.jsontoxml = jsontoxml;

function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

function xmltojson(xml) {
  const jsonDoc = convert.xml2json(xml, { compact: true, spaces: 4 });
  return jsonDoc
}

function jsontoxml(json) {
  const xmlDoc = convert.json2xml(json, { compact: true, ignoreComment: true, spaces: 4 })
  return xmlDoc
}
