const axios = require('axios');

const URL = "https://proto6.api.htec-labs.io/customer/query/api/subsetCustomer";

const getProducts = async (params) => {
  const response = await axios(params)
  return response.data.message;
}
;(async () => {

  let LastEvaluatedKey = false;
  var iteration = 1
  do {
    const params = {
      url: URL,
      method: "post",
      data: {
        customerName: "sampleCustomer",
        startDateString: "2019-07-21 00:00:00"
      }
    }
    if (LastEvaluatedKey) { params.data.LastEvaluatedKey = LastEvaluatedKey}

    const response = await getProducts(params)
    LastEvaluatedKey = response.hasOwnProperty("LastEvaluatedKey") ? response.LastEvaluatedKey : false

    console.log(`Iteration Counter: ${iteration}: ${LastEvaluatedKey.innerEAN}`);
    iteration++;
  }
  while (LastEvaluatedKey)

})()
