

const feedHandlers = require('./handlers/feed');
module.exports.convertFeedHandler = feedHandlers.convert
module.exports.loadFeedHandler = feedHandlers.load
module.exports.loadPromotionsFeedHandler = feedHandlers.promotionLoad;
module.exports.retrieveFeedHandler = feedHandlers.retrieve


const customerHandlers = require('./handlers/customer');
module.exports.exportCustomerProductsHandler = customerHandlers.export
module.exports.importCustomerProductsHandler = customerHandlers.import
module.exports.queryCustomerProductsHandler = customerHandlers.query
module.exports.retrieveCustomerHandler = customerHandlers.retrieve
module.exports.queryAPICustomerProductsHandler = customerHandlers.queryAPI
module.exports.getAllCustomerHandler = customerHandlers.getAll
module.exports.upsertCustomerHandler = customerHandlers.upsert


//const productHandlers = require('./handlers/product');
//module.exports.getAllProductHandler = productHandlers.getAll;
//module.exports.getProductHandler = productHandlers.get;
//module.exports.deleteProductHandler = productHandlers.delete;
//module.exports.validateProductHandler = productHandlers.validate;
//module.exports.parseProductHandler = productHandlers.parse;
//module.exports.transformProductHandler = productHandlers.transform;
//module.exports.upsertProductHandler = productHandlers.upsert;



//const pocHandlers = require('handlers/poc');
//module.exports.pocHandler = pocHandlers.poc;

const ScheduleHandlers = require('./handlers/schedule');
module.exports.retrieveScheduleHandler = ScheduleHandlers.retrieveSchedule;


const exportHandlers = require('./handlers/export');
module.exports.mergeExportHandler = exportHandlers.merge
module.exports.queryExportHandler = exportHandlers.query


//const supplierHandlers = require('handlers/supplier')
//module.exports.upsertSupplierHandler = supplierHandlers.upsert
//module.exports.getSupplierHandler = supplierHandlers.get
//module.exports.getAllSupplierHandler = supplierHandlers.getAll
