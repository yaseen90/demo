
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const s3utils = require('../../utils/s3utils');
const productService = require('../../lib/products/productServiceApi');

module.exports.query = async (event) => {
  // const queryFunctions = {
  //   "query": queryDb,
  //   "export": exportDb
  // }

  const PRODUCT_QUERY_TOPIC_ARN = process.env.PRODUCT_QUERY_TOPIC_ARN;
  const EXPORT_MERGE_TOPIC_ARN = process.env.EXPORT_MERGE_TOPIC_ARN;
  // const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;
  const limitRecordSet = 500;
  const message = JSON.parse(event.Records[0].Sns.Message);
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const queryType = message.queryType ? message.queryType : "export"
  const Bucket = message.Bucket;
  const Key = message.Key;
  const customerName = message.customerName;
  const dateStamp = message.dateStamp;
  let sequence = message.sequence
  let productCount = message.productCount;
  const ExclusiveStartKey = message.ExclusiveStartKey ? message.ExclusiveStartKey : false;
  // const startDateString = message.startDateString ? message.startDateString : false
  // const startDate = new Date(startDateString)
  // const startDate_dts = startDate.getTime()
  // const result = await queryFunctions[queryType]({ DYNAMODB_TABLE, IndexName: "LSIByLastUpdated", customerName, startDate_dts, limitRecordSet, ExclusiveStartKey })
  const result = await productService({ customerName, body: { ExclusiveStartKey, Limit: limitRecordSet } });
  const currentProductCount = result.Count;
  productCount = productCount + currentProductCount;
  let products = result.records.map(obj => {
    let rObj = obj
    rObj.orders = obj.orders.map(order => {
      let newOrder = {};
      Object.keys(order).forEach((key) => {
        newOrder[key] = order[key]["transformed"]
      })
      newOrder.supplierName = order.supplierName
      newOrder.supplierId = order.supplierId
      return newOrder;
    })
    return rObj
  })

  const exportKey = `exports/${customerName}/${dateStamp}/export-${sequence}-${currentProductCount}-${productCount}.json`

  // Upload Products to Export File
  const uploadParams = {
    bucket: Bucket, key: exportKey,
    content: JSON.stringify({ products: products }, 0, 4)
  }
  const uploadResponse = await s3utils.s3Upload(uploadParams).then(response => { return response }).catch(err => { console.log(err) })

  if (result.LastEvaluatedKey) {
    sequence = sequence + 1
    let publishParams = {
      Message: JSON.stringify({
        dateStamp,
        Bucket,
        Key,
        customerName,
        ExclusiveStartKey: result.LastEvaluatedKey,
        sequence,
        startDateString,
        productCount,
        queryType,
        jobId,
        jobCreatedAt
      }),
      TopicArn: PRODUCT_QUERY_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response }).catch(err => { console.log(err) })
  } else {
    const publishParams = {
      Message: JSON.stringify({ Bucket, customerName, timeStamp: dateStamp, done: false, files: "pending", next: 0, productCount, jobId, jobCreatedAt }),
      TopicArn: EXPORT_MERGE_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "response",
    }, null, 2),
  }
}

// const exportDb = async (params) => {
//   const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
//   const customerName = params.customerName;
//   const limitRecordSet = params.limitRecordSet;
//   const ExclusiveStartKey = params.ExclusiveStartKey ? params.ExclusiveStartKey : false;

//   let dbParams = {
//     TableName: DYNAMODB_TABLE,
//     KeyConditionExpression: 'customerName = :customerName',
//     ExpressionAttributeValues: { ':customerName': customerName },
//     Limit: limitRecordSet
//   }

//   if (ExclusiveStartKey) {
//     dbParams.ExclusiveStartKey = params.ExclusiveStartKey;
//   }

//   const result = await dynamoDBUtils.query(dbParams)

//   return result;

// }

// const queryDb = async (params) => {
//   // Get Data from DB
//   const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
//   const IndexName = params.IndexName ? params.IndexName : false;
//   const customerName = params.customerName;
//   const startDate_dts = params.startDate_dts;
//   const limitRecordSet = params.limitRecordSet;
//   const ExclusiveStartKey = params.ExclusiveStartKey ? params.ExclusiveStartKey : false;

//   let dbParams = {
//     TableName: DYNAMODB_TABLE,
//     KeyConditionExpression: 'customerName = :customerName and lastUpdated > :startDate',
//     ExpressionAttributeValues: { ':customerName': customerName, ':startDate': startDate_dts },
//     Limit: limitRecordSet
//   }

//   if (IndexName) {
//     dbParams.IndexName = IndexName
//   }
//   if (ExclusiveStartKey) {
//     dbParams.ExclusiveStartKey = params.ExclusiveStartKey;
//   }

//   const result = await dynamoDBUtils.query(dbParams)

//   return result;
// }
