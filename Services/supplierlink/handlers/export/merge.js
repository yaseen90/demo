const es = require('event-stream')
const JSONStream = require('JSONStream');
const AWS = require('aws-sdk');
const sns = new AWS.SNS({ apiVersion: '2010-03-31' });
const exportLib = require('../../lib/export');
const commons = require('../../lib/commons');
const combinedStream = require('combined-stream');
const { Readable } = require('stream')
const jobs = require('../../lib/jobs');


module.exports.merge = async (event) => {
  const EXPORT_MERGE_TOPIC_ARN = process.env.EXPORT_MERGE_TOPIC_ARN;
  let message = JSON.parse(event.Records[0].Sns.Message);
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  // Parse sns message
  const Bucket = message.Bucket;
  const customerName = message.customerName
  const timeStamp = message.timeStamp;
  const LAST_MERGED_KEY = message.LAST_MERGED_KEY
  let next = message.next;
  const done = message.done
  let files = message.files;
  const productCount = message.productCount;
  const prefix = `exports/${customerName}/${timeStamp}`;
  const MERGED_PREFIX = `merged/${customerName}/${timeStamp}`
  const FINAL_MERGED_KEY = `transformed/reports/${customerName}/products.json`
  let MERGED_KEY = `${MERGED_PREFIX}/merged.${next}.json`;
  let fileList = [];
  let initialize = false;


  // All files merged, copy to final export and delete the transient merges
  if ((done === true) || ((Array.isArray(files)) && !(files.length))) {
    const jobDetails = await jobs.update({ active: 0, statusCode: 0, status: "done", jobId, createdAt: jobCreatedAt })
    return exportLib.finalExport({ Bucket, LAST_MERGED_KEY, FINAL_MERGED_KEY, MERGED_PREFIX, prefix })
  }
  // Get files for first iteration
  if (files === "pending") {
    files = await exportLib.getFileList({ Bucket, prefix })
    initialize = true;
  }
  // Return if no files to merge
  if (!files) {
    const jobDetails = await jobs.update({ active: 0, statusCode: 127, status: "error", jobId, createdAt: jobCreatedAt, message: "Files not found" })
    return commons.returnError({ message: "Files not found" })
  }
  // Populate Merge Stream Array
  if ((initialize) && (files.length === 1)) {
    fileList.push(files.shift());
  } else if (initialize) {
    fileList.push(files.shift());
    fileList.push(files.shift());
  } else {
    fileList.push(`${MERGED_PREFIX}/merged.${next - 1}.json`);
    fileList.push(files.shift());
  }
  const mergeStream = exportLib.createMergedReadStreams({ Bucket, files: fileList });
  const { writeStream, uploadPromise } = exportLib.createWriteStream(Bucket, MERGED_KEY);
  const wrapperMergedStream = combinedStream.create();
  const headerStream = getWrapperStartStream();
  const footerStream = getWrapperEndStream(productCount);
  const bodyStream = mergeStream.pipe(JSONStream.parse('products.*')).pipe(es.mapSync(function (data) {
    //  console.error(JSON.stringify(data,0,4))
    return `${JSON.stringify(data, 0, 4)}`;
  })).pipe(es.join(',\n'))

  wrapperMergedStream.append(headerStream);
  wrapperMergedStream.append(bodyStream);
  wrapperMergedStream.append(footerStream);
  wrapperMergedStream.pipe(writeStream);
  const uploadResponse = await uploadPromise
  message.files = files;
  message.next++;
  if (files.length === 0) {
    message.done = true;
    message.LAST_MERGED_KEY = MERGED_KEY;
    message.PRODUCT_EXPORTS_KEY = FINAL_MERGED_KEY
  }
  publishParams = {
    Message: JSON.stringify(message),
    TopicArn: EXPORT_MERGE_TOPIC_ARN
  }
  publishResponse = await sns.publish(publishParams).promise().then(response => { return response })


  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Response",
    }, null, 2)
  }

}

function getWrapperStartStream() {
  const wrapperStartStream = new Readable({ read() { } })
  wrapperStartStream.push("{\n")
  wrapperStartStream.push("\"products\": [\n")
  wrapperStartStream.push(null)
  return wrapperStartStream;
}
function getWrapperEndStream(counter) {

  const wrapperEndStream = new Readable({ read() { } })
  wrapperEndStream.push("],\n");
  wrapperEndStream.push(`"count": ${counter} }`)
  wrapperEndStream.push(null);
  return wrapperEndStream;
}
