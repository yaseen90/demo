const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const s3utils = require('../../utils/s3utils');

//const dynamoDB = new aws.DynamoDB.DocumentClient();

module.exports.export = async (event) => {
  const PRODUCT_REPORT_TOPIC_ARN = process.env.PRODUCT_REPORT_TOPIC_ARN;
  const EXPORT_MERGE_TOPIC_ARN = process.env.EXPORT_MERGE_TOPIC_ARN;

  const message = JSON.parse(event.Records[0].Sns.Message);
  const Bucket = message.Bucket;
  const Key = message.Key;
  const customerName = message.customerName
  const limitRecordSet = 50;
  const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;
  let dateStamp = message.dateStamp;
  // Get Data from DB
  let dbParams = {
    TableName: DYNAMODB_TABLE,
    KeyConditionExpression: 'customerName = :customerName',
    ExpressionAttributeValues: { ':customerName': customerName },
    Limit: limitRecordSet
  }
  let rangeStart = 1
  let rangeEnd = limitRecordSet;

  if (message.ExclusiveStartKey) {
    dbParams.ExclusiveStartKey = message.ExclusiveStartKey;
    rangeStart = message.rangeStart;
    rangeEnd = message.rangeEnd;

  }
  const result = await dynamoDBUtils.query(dbParams)

  products = {};
  products[`${rangeStart}-${rangeEnd}`] = result.Items.map(obj => {
    let rObj = obj
    rObj.orders = obj.orders.map(order => {
      let newOrder = {};
      Object.keys(order).forEach((key) => {
        newOrder[key] = order[key]["transformed"]
      })
      newOrder.supplierName = order.supplierName
      return newOrder;
    })
    return rObj
  })
  // Upload Products to Export File
  const uploadParams = {
    bucket: Bucket, key: `exports/${customerName}/${dateStamp}/export-${rangeStart}-${rangeEnd}.json`,
    content: JSON.stringify(products, 0, 4)
  }
  const uploadResponse = await s3utils.s3Upload(uploadParams).then(response => { return response }).catch(err => { console.log(err) })

  if (result.LastEvaluatedKey) {
    rangeStart = rangeStart + limitRecordSet;
    rangeEnd = rangeEnd + limitRecordSet;
    let publishParams = {
      Message: JSON.stringify({ dateStamp, Bucket, Key, customerName, ExclusiveStartKey: result.LastEvaluatedKey, rangeStart, rangeEnd }),
      TopicArn: PRODUCT_REPORT_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response }).catch(err => { console.log(err) })
  } else {
    const publishParams = {
      Message: JSON.stringify({ Bucket, customerName, timeStamp: dateStamp, done: false, files: "pending", next: 0 }),
      TopicArn: EXPORT_MERGE_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "response",
    }, null, 2),
  }
}
