const dbUtils = require('../../utils/dynamoDBUtils')

module.exports.poc =  async (event) => {
  const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE
  const dbParams = {
    TableName: CUSTOMER_DBTABLE,
    Key: {
      name: "subsetCustomer",
      type: "customer"
    }
  }
  const dbResponse = await dbUtils.get(dbParams)
  const customer = dbResponse.Item;
  let schedule = customer.schedule;
  const currentDts = Date.now();
  const nextIteration = parseInt(schedule.nextIteration)
  console.log(`${currentDts} -- ${nextIteration}`)
  if ( currentDts >= nextIteration) {
    schedule.currentStatus = "processing";
    schedule.lastIteration = currentDts;
    schedule.forScheduledIteration = schedule.nextIteration;
    schedule.nextIteration =  schedule.nextIteration + schedule.interval;
    schedule.currentStatus = "waiting";
    schedule.lastIterationStatus = "ok"
    customer.schedule = {...schedule}
    const dbPutParams = {
      DYNAMODB_TABLE: CUSTOMER_DBTABLE,
      item: customer
    }
    const dbPutResponse = await dbUtils.create(dbPutParams);

  } else {
    console.log("No schedule to trigger....");
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Response",
    }, null, 2)
  }

}
