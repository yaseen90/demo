
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory');

module.exports.retrieve = async (event) => {
  const RETRIEVE_FEED_TOPIC_ARN = process.env.RETRIEVE_FEED_TOPIC_ARN
  const jobProcess = process.env.DOMAINNAME + " - Scheduler";
  let jobDetails = await jobs.generate({ active: 1, source: jobProcess })
  const jobId = jobDetails.success ? jobDetails.item.id : false
  const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false
  const allCustomerResponse = await directoryLib.getAllByType({ type: "customer" })
  const allCustomers = allCustomerResponse.records

  for (let i = 0; i < allCustomers.length; i++) {
    const activeCustomer = allCustomers[i]
    if (!(activeCustomer.hasOwnProperty("schedule")) || (!(activeCustomer.schedule.enabled))) {
      continue;
    }
    if (activeCustomer.schedule.running) {
      continue;
    }
    const currentDts = parseInt(Date.now())
    const nextIteration = parseInt(activeCustomer.schedule.nextIteration)
    if (nextIteration > currentDts) {
      const message = `Scheduler skipped as interval time not passed: ${activeCustomer.name}`
      continue;
    }

    activeCustomer.schedule.lastIteration = currentDts
    activeCustomer.schedule.nextIteration = currentDts + activeCustomer.schedule.interval
    activeCustomer.schedule.running = true
    const customerUpdateResponse = await directoryLib.upsert({ record: activeCustomer, type: "customer" })
    const customerUpdateStatus = customerUpdateResponse.success
    if (!customerUpdateStatus) {
      continue;
    }

    const activeSupplierList = activeCustomer.activeSuppliers

    for (let i = 0; i < activeSupplierList.length; i++) {
      const supplierName = activeSupplierList[i]
      const supplierConfigIndex = activeCustomer.supplierConfig.findIndex(s => { return s.name === supplierName })
      if (supplierConfigIndex !== -1) {
        const publishParams = {
          Message: JSON.stringify({
            customer: activeCustomer,
            supplierName,
            supplierConfigIndex,
            jobId,
            jobCreatedAt,
            origin: "schedule"
          }),
          TopicArn: RETRIEVE_FEED_TOPIC_ARN
        }
        console.log(publishParams,"myConsole")
        const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
      }

    }
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Response",
    }, null, 2)
  }
}
