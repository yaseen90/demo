
const stream = require('stream');
const es = require('event-stream')
const JSONStream = require ('JSONStream');
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const combinedStream = require('combined-stream');


function createWriteStream(Bucket, Key) {
    const writeStream = new stream.PassThrough()
    const uploadPromise = S3
        .upload({
            Bucket,
            Key,
            Body: writeStream
        })
        .promise()
    return { writeStream, uploadPromise }
}

module.exports.pocStream =  async (event) => {

  const Bucket = "htec-proto-transformed"
  const key_merged = "merged.json"
  const key_1 = "exports/sampleCustomer/2019-06-19_1207:31/export-0-1000.json"
  const key_2 = "exports/sampleCustomer/2019-06-19_1207:31/export-1000-2000.json"

  let getParams = {
    Bucket: Bucket,
    Key: key_1
  }
  const input_1 = S3.getObject(getParams).createReadStream();


  getParams = {
      Bucket: Bucket,
      Key: key_2
    }
  const input_2 = S3.getObject(getParams).createReadStream();

  const mergeStreams = combinedStream.create();
  mergeStreams.append(input_1);
  mergeStreams.append(input_2);
  const { writeStream, uploadPromise } = createWriteStream(Bucket, key_merged)

  mergeStreams.pipe(JSONStream.parse()).pipe(es.mapSync(function (data) {
    console.error(JSON.stringify(data,0,4))
    return JSON.stringify(data,0,4)
  })).pipe(writeStream)

  const uploadResponse = await uploadPromise

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Response",
    }, null, 2)
  }

}
