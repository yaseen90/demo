const jobs = require('../../lib/jobs');
const networkRetrieve = require('../../lib/network')
const directoryLib = require('../../lib/directory')
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });

module.exports.retrieve = async (event) => {
  const CONVERT_FEED_TOPIC_ARN = process.env.CONVERT_FEED_TOPIC_ARN;
  const feedBucket = process.env.Bucket;

  const message = JSON.parse(event.Records[0].Sns.Message);
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;

  const origin = message.hasOwnProperty("origin") ? message.origin : "api"
  const supplierName = message.supplierName;
  const customer = message.customer;
  const feedDelete = message.feedDelete;
  const customerSupplierConfig = customer.supplierConfig[message.supplierConfigIndex];

  const getResponse = await directoryLib.get({ name: supplierName, type: "supplier" })
  const getConfig = await directoryLib.get({ name: supplierName, type: "supplierConfig" })
  const config = getConfig.record
  const supplier = getResponse.record;

  const supplierConfig = { ...supplier, ...customerSupplierConfig }
  const bucketPrefix = `${customer.name}/${supplierConfig.name}`
  const accessType = supplierConfig.hasOwnProperty("accessType") ? supplierConfig.accessType : false

  let response;
  if (accessType === "sftp") {
    const sftpParams = {
      host: supplierConfig.sftpHostname,
      port: supplierConfig.sftpPort,
      username: supplierConfig.sftpUsername,
      KeyLocation: supplierConfig.KeyLocation,
      KeyLocationType: supplierConfig.KeyLocationType,
      productFile: supplierConfig.sftpProductFile,
      prefix: supplierConfig.sftpPrefix,
      bucketPrefix: bucketPrefix,
      Bucket: feedBucket
    }
    response = await networkRetrieve.sftp(sftpParams)
  }
  else if (accessType === "ftp") {
    const prefix = supplierConfig.hasOwnProperty("ftpPrefix") ? supplierConfig.ftpPrefix : "."
    const ftpParams = {
      host: supplierConfig.ftpHostname,
      user: supplierConfig.ftpUsername,
      password: supplierConfig.ftpPassword,
      productFile: supplierConfig.ftpProductFile,
      prefix: prefix,
      Bucket: feedBucket,
      bucketPrefix: bucketPrefix,
    }
    response = await networkRetrieve.ftp(ftpParams)
  }
  else if (accessType === "https") {
    const prefix = supplierConfig.hasOwnProperty("httpsPrefix") ? supplierConfig.httpsPrefix : "."
    const httpsParams = {
      host: supplierConfig.httpsHostname,
      user: supplierConfig.httpsUsername,
      xmlSchema : supplierConfig.credentials,
      password: supplierConfig.httpsPassword,
      productFile: supplierConfig.httpsProductFile,
      prefix: prefix,
      Bucket: feedBucket,
      bucketPrefix: bucketPrefix,
      supplierName : supplierName
    }
    response = await networkRetrieve.https(httpsParams)
    // return false
  }
  else {
    const message = `Unknown Access Type for Supplier: ${accessType}`
    const jobDetails = await jobs.update({ active: 0, statusCode: 1, status: "ERROR", jobId, createdAt: jobCreatedAt, message })
  }
  if (response.success) {
    for (let i = 0; i < response.uploaded.length; i++) {
      const key = response.uploaded[i]
      let publishParams = {
        Message: JSON.stringify({ bucket: feedBucket, key: key, feedDelete ,customerName: customer.name, customerId: customer.id, supplierId: supplierConfig.id, jobId, jobCreatedAt, origin ,config , supplierName}),
        TopicArn: CONVERT_FEED_TOPIC_ARN
      }
      const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
    }
  } else {
    const jobDetails = await jobs.update({ active: 0, statusCode: 1, status: "ERROR", jobId, createdAt: jobCreatedAt, message: response.message })
  }
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "Response",
    }, null, 2)
  }

}
