
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const sqs = new aws.SQS({ region: "eu-west-1" })
const s3 = new aws.S3({ apiVersion: '2006-03-01' });
const _ = require('lodash');
// const loadConfig = require('../../Config')
const jp = require('jsonpath');
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory')

module.exports.promotionLoad = async (event) => {
  
  const message = JSON.parse(event.Records[0].Sns.Message);
  const QUEUE_URL = process.env.PromotionsQueue;
  const config = message.config;
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const origin = message.hasOwnProperty("origin") ? message.origin : "api"
  const s3params = { Bucket: message.bucket, Key: message.key }
  // const PRODUCT_VALIDATE_TOPIC_ARN = process.env.PRODUCT_VALIDATE_TOPIC_ARN
  const PROMOTIONS_INSERT_TOPIC_ARN = process.env.PROMOTIONS_INSERT_TOPIC_ARN
  const LOAD_FEED_TOPIC_ARN = process.env.LOAD_FEED_TOPIC_ARN;
  const range = message.range
  const supplierName = message.supplierName
  const customerId = message.customerId
  const supplierId = message.supplierId
  const productLoadQueryString = config.productLoadQueryString;
  let s3Response = await s3.getObject(s3params).promise()
  // let publishParams = { TopicArn: PRODUCT_VALIDATE_TOPIC_ARN };
  let publishParams = { TopicArn: PROMOTIONS_INSERT_TOPIC_ARN };
  let snsMessage = { supplierName, customerName: message.customerName, supplierId, customerId, jobId, jobCreatedAt, config };
  const jsonResponse = JSON.parse(s3Response.Body.toString())
  let productPublishes = jp.query(jsonResponse, productLoadQueryString)
  let finalPublishes = productPublishes[0]
  if (finalPublishes && !(Array.isArray(finalPublishes))) {
    finalPublishes = [finalPublishes]
  }
  if (range.done) {
    const jobDetails = await jobs.update({ active: 0, statusCode: 0, status: "done", jobId, createdAt: jobCreatedAt, origin })
    if (origin === "schedule") {
      //  console.log(message.customerName)
      const getResponse = await directoryLib.get({ name: message.customerName, type: "customer" })
      const activeCustomer = getResponse.record;
      activeCustomer.schedule.lastIterationStatus = "0"
      activeCustomer.schedule.running = false;
      const updateCustomerResponse = await directoryLib.upsert({ record: activeCustomer, type: "customer" })
    }
    return {
      statusCode: 200,
      body: JSON.stringify({
        message: "response",
      }, null, 2),
    }

  }

  if (range.end >= finalPublishes.length) {
    range.end = finalPublishes.length
    range.done = true
  }
  for (let i = range.start; i < range.end; i++) {
    snsMessage.product = finalPublishes[i];
    const SqsParams = {
      MessageBody : JSON.stringify({
        message:snsMessage
      }),
      QueueUrl : QUEUE_URL
    }
    const sqsResponse = await sqs.sendMessage(SqsParams).promise().then(response => response);
    console.log(sqsResponse)
  }
  range.start = range.start + range.process
  range.end = range.end + range.process
  if (range.done) {
    console.log("All done")
  } else {
    console.log(`Offload  range: ${range.start} -- ${range.end}`)
  }
  publishParams = {
    Message: JSON.stringify({
      bucket: message.bucket, key: message.key,
      supplierName: message.supplierName, customerName: message.customerName, range, supplierId, customerId, jobId, jobCreatedAt, origin, config
    }),
    TopicArn: LOAD_FEED_TOPIC_ARN
  }
  const reloadResponse = await sns.publish(publishParams).promise().then(response => { return response })
  //console.log(reloadResponse)

}
