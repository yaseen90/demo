const _ = require('lodash');
const convertors = require('../../lib/convertors');
const jobs = require('../../lib/jobs');
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const s3 = new aws.S3({ apiVersion: '2006-03-01' });

module.exports.convert = async (event) => {
  const message = JSON.parse(event.Records[0].Sns.Message);
  const config = message.config;
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const jobProcess = process.env.DOMAINNAME + " - Feed-Convert";
  const feedBucket = message.bucket;
  const feedFile = message.key;
  const customerName = message.customerName;
  const feedDelete = message.feedDelete;
  const supplierName = message.supplierName;
  const archiveBucket = process.env.Bucket;

  if (config === undefined) {
    let jobDetails = await jobs.update({ active: 1, statusCode: 0, status: "ACTIVE", jobId, source: jobProcess, createdAt: jobCreatedAt, message: "Cannot get Supplier Config" })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: "Cannot get Supplier Config" })
    return false
  }
  const supplierConfigName = config.name;
  if (supplierConfigName !== supplierName) {
    let jobDetails = await jobs.update({ active: 1, statusCode: 0, status: "ACTIVE", jobId, source: jobProcess, createdAt: jobCreatedAt, message: "invalid Supplier Config Name" })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: "invalid Supplier Config Name" })
    return false
  }
  let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: "ok" })

  const convertBucket = process.env.Bucket;
  const feedfileName = `${feedFile.split('.')[0]}.json`;

  const fileName = feedFile.split('/')[2]
  //Load Products
  try {
    const feedResponse = await s3.getObject({ Bucket: feedBucket, Key: feedFile }).promise()
    const source = feedResponse.Body.toString();
    //const jsonDoc = convert.xml2json(responseBody, {compact: true, spaces: 4});
    const productConvert = await convertProduct({ source, convertBucket, feedfileName, message })
    if (config.promotionHeaders) {
      const promotionConvert = await convertPromotions({ source, convertBucket, feedfileName, message })
    }
    //archive the original feed filter
    let datenow = Date.now();
    const archiveFile = "archive/" + customerName + "/" + supplierName + "/" + datenow + "-" + fileName;
    //const copyResponse = await s3.s3Copies({sourceBucket: feedBucket, sourceKey: feedFile, destBucket: archiveBucket, destKey: archiveFile}).promise()
    let copyParams = {
      Bucket: archiveBucket,
      CopySource: `${feedBucket}/${feedFile}`,
      Key: archiveFile
    }
    const jobMsg = "Successful Conversion";
    let jobDetails = await jobs.update({ active: 1, statusCode: 0, status: "ACTIVE", jobId, source: jobProcess, createdAt: jobCreatedAt, message: jobMsg })
    await s3.copyObject(copyParams).promise()
    if (feedDelete === "true") {
      const deleteParams = {
        Bucket: feedBucket,
        Key: feedFile
      }
      await s3.deleteObject(deleteParams).promise();
    }

  } catch (err) {
    const jobMsg = "Conversion Failed " + err;
    let jobDetails = await jobs.update({ active: 0, statusCode: 1, status: "ERROR", jobId, source: jobProcess, createdAt: jobCreatedAt, message: jobMsg })
    let auditDetails = await jobs.audit({ jobId, jobCreatedAt, supplierName, customerName, message: jobMsg })
    // console.log(`Error: ${feedFile} import failed`)
    // console.log(`Debug: ${err}`);
    // console.log(jobDetails);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "response",
    }, null, 2),
  }

}

async function convertProduct(params) {
  const origin = params.message.hasOwnProperty("origin") ? params.message.origin : "api";
  const supplierId = params.message.supplierId;
  const customerId = params.message.customerId;
  const supplierName = params.message.supplierName;
  const jobId = params.message.jobId;
  const jobCreatedAt = params.message.jobCreatedAt;
  const customerName = params.message.customerName;
  const config = params.message.config;
  const LOAD_FEED_TOPIC_ARN = process.env.LOAD_FEED_TOPIC_ARN;
  const source = params.source;
  const filter = config.filter;
  const headers = config.headers;
  const fullWidth = config.fullWidth ? config.fullWidth : false
  const convertFunction = config.convert;
  const convertBucket = params.convertBucket;
  const feedfileName = params.feedfileName;
  const fileNameSlice = feedfileName.slice(6);
  const split = fileNameSlice.split("/")
  split.splice(2, 0, "product")
  const fileName = split.join('/')
  const convertKey = `convert/${fileName}`
  try {
    const convertorParams = { source, filter, headers, fullWidth };
    const content = await convertors[convertFunction](convertorParams)
    const writeParams = { Bucket: convertBucket, Key: convertKey, Body: content }
    const uploadResponse = await s3.upload(writeParams).promise();
    const range = { start: 0, end: 50, process: 50, done: false }
    publishParams = {
      Message: JSON.stringify({ bucket: convertBucket, key: convertKey, customerName, supplierName, range, customerId, supplierId, jobId, jobCreatedAt, origin, config }),
      TopicArn: LOAD_FEED_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
    return publishResponse;
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: "error in product convert" + error.message,
      }, null, 2),
    }
  }
}
async function convertPromotions(params) {
  const origin = params.message.hasOwnProperty("origin") ? params.message.origin : "api";
  const supplierId = params.message.supplierId;
  const customerId = params.message.customerId;
  const supplierName = params.message.supplierName;
  const jobId = params.message.jobId;
  const jobCreatedAt = params.message.jobCreatedAt;
  const customerName = params.message.customerName;
  const config = params.message.config;
  const LOAD_PROMOTIONS_FEED_TOPIC_ARN = process.env.LOAD_PROMOTIONS_FEED_TOPIC_ARN;
  const source = params.source;
  const filter = config.promotionFilter;
  const headers = config.promotionHeaders;
  const fullWidth = config.fullWidth ? config.fullWidth : false
  const convertFunction = config.convert;
  const convertBucket = params.convertBucket;
  const feedfileName = params.feedfileName;
  const fileNameSlice = feedfileName.slice(6);
  const split = fileNameSlice.split("/")
  split.splice(2, 0, "promotion")
  const fileName = split.join('/')
  const convertKey = `convert/${fileName}`
  console.log("convertKey", convertKey);
  console.log("convertFunction", convertFunction);

  try {
    const convertorParams = { source, filter, headers, fullWidth };
    const content = await convertors[convertFunction](convertorParams)
    console.log("content", content);

    const writeParams = { Bucket: convertBucket, Key: convertKey, Body: content }
    const uploadResponse = await s3.upload(writeParams).promise();
    const range = { start: 0, end: 50, process: 50, done: false }
    publishParams = {
      Message: JSON.stringify({ bucket: convertBucket, key: convertKey, customerName, supplierName, range, customerId, supplierId, jobId, jobCreatedAt, origin, config }),
      TopicArn: LOAD_PROMOTIONS_FEED_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
    return publishResponse
  } catch (error) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        message: "error in product convert" + error.message,
      }, null, 2),
    }
  }
}