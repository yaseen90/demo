
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const productLib = require('../../lib/products');
const productParser = require('../../lib/products/parser');
const loadConfig = require('../../Config');
const jobs = require('../../lib/jobs');

module.exports.parse = async (event) => {

  const PRODUCT_TRANSFORM_TOPIC_ARN = process.env.PRODUCT_TRANSFORM_TOPIC_ARN
  const mapBucket = process.env.mapBucket;
  const messageString = event.Records[0].Sns.Message
  const message = JSON.parse(messageString)
  const product = message.product;
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const supplierName = message.supplierName
  const configMap = await loadConfig()
  const supplierConfigMap = configMap.SUPPLIERS
  const textField = supplierConfigMap[supplierName].textField;
  // Parse the product record
  const rules = await productLib.getRules({ supplierName, mapBucket })
  const matchAttributes = rules.map(rule => { return rule.destField })
  console.log(message,"message>>>")
  if (rules === false) {
    let jobDetails = await jobs.update({ active: 1, statusCode: 400, status: "false", jobId, createdAt: jobCreatedAt, message: "rules not loaded" })
  }
  const parsedProduct = await parse({ product, textField, supplierName, rules })
  if (parsedProduct === false) {
    let jobDetails = await jobs.update({ active: 1, statusCode: 400, status: "false", jobId, createdAt: jobCreatedAt, message: "some thing went rong with parsing" })
  }
  message.product = parsedProduct;
  message.matchAttributes = matchAttributes;
  const publishParams = {
    Message: JSON.stringify(message),
    TopicArn: PRODUCT_TRANSFORM_TOPIC_ARN
  }
  const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "parsed",
    }, null, 2),
  }

}

let parse = async (params) => {
  const supplierName = params.supplierName;
  const textField = params.textField;
  const product = params.product
  let rules = params.rules;
  return productParser.productSupplierParser({ textField, supplierName, rules, productRecord: product })
}
