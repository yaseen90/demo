//Config Maps
const loadConfig = require('../../Config');
const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const productLib = require('../../lib/products');
const productFunctions = require("../../lib/productFunction");
const jobs = require('../../lib/jobs');

module.exports.upsert = async (event) => {
  const configMap = await loadConfig()
  const masterConfigMap = configMap.MASTER

  const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;

  const messageString = event.Records[0].Sns.Message
  let message = JSON.parse(messageString);
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  // const validate = await productFunctions.validate(message, configMap)
  // if (!validate.success) {
  //   const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: "Product is invalid" })
  //   return false;
  // }
  // const parse = await productFunctions.parse(validate.body, configMap);
  // if (!parse.success) {
  //   const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: "some thing went rong with parsing" })
  //   return false;
  // }
  // const transform = await productFunctions.transform(parse.body, configMap);
  // if (!transform.success) {
  //   const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: "Attributes Does Not Match" })
  //   return false
  // }
  // message = transform.body;

  const transformedProduct = message.product;
  const customerName = message.customerName;
  const supplierName = message.supplierName;
  const customerId = message.customerId;
  const masterProductIdentifier = masterConfigMap.productIdentifier
  const masterproductCodeIdentifier = masterConfigMap.productCodeIdentifier;

  let productRecord = {}
  productRecord[masterProductIdentifier] = transformedProduct[masterProductIdentifier]["transformed"]
  productRecord.customerName = customerName;
  productRecord.customerId = customerId;
  productRecord = await productLib.addOrAppend({
    DYNAMODB_TABLE,
    productRecord,
    supplierName,
    customerName,
    masterProductIdentifier,
    transformedProduct,
    masterproductCodeIdentifier
  })

  const dynamoDBParams = {
    item: productRecord,
    DYNAMODB_TABLE
  }
  const dynamoDBResponse = await dynamoDBUtils.create(dynamoDBParams);
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "product added",
    }, null, 2),
  }

}
