
const aws = require('aws-sdk');
const s3Utils = require("../../utils/s3utils");
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const productLib = require('../../lib/products');
const jobs = require('../../lib/jobs');

//Config Maps
const loadConfig = require('../../Config');

module.exports.validate = async (event) => {
  const configMap = await loadConfig()
  const PRODUCT_PARSE_TOPIC_ARN = process.env.PRODUCT_PARSE_TOPIC_ARN;
  const failBucket = process.env.failProductBucket;
  const messageString = event.Records[0].Sns.Message;
  const message = JSON.parse(messageString);
  const product = message.product;
  const supplierName = message.supplierName
  const customerName = message.customerName
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const supplierConfigMap = configMap.SUPPLIERS
  const productIdentifier = supplierConfigMap[supplierName].productIdentifier;
  const textField = supplierConfigMap[supplierName].textField;

  const isValid = productLib.validateProduct({ product, textField, productIdentifier });
  // Ideally sohuld be try catch exception block
  if (isValid === false) {
    let key = `${customerName}/${supplierName}/${jobId}`;
    product.jobCreatedAt = jobCreatedAt;
    product.jobId = jobId;
    const jobDetails = await jobs.update({ active: 0, statusCode: 500, status: "Error", jobId, createdAt: jobCreatedAt, message: "Product is invalid" })
    let failProduct = JSON.stringify(product);
    const s3UploadParams = { bucket: failBucket, key: key, content: failProduct }
    const uploadResponse = await s3Utils.s3Upload(s3UploadParams)
    console.log("product is invalid");
    // Add invalid handler
  } else {
    const publishParams = {
      Message: JSON.stringify(message),
      TopicArn: PRODUCT_PARSE_TOPIC_ARN
    }
    const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: "published",
    }, null, 2),
  }

}
