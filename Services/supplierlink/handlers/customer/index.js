//module.exports.getAll = require('./getAll').getAll;
//module.exports.upsert = require('./upsert').upsert;

module.exports.export = require('./export').export;
module.exports.import = require('./import').import;
module.exports.query = require('./query').query;
module.exports.retrieve = require('./retrieve').retrieve;
module.exports.queryAPI = require('./queryAPI').queryAPI;
