const jp = require('jsonpath');
const _ = require('lodash');
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const s3utils = require('../../utils/s3utils');
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory')


module.exports.import = async (event) => {

  const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL
  const CONVERT_FEED_TOPIC_ARN = process.env.CONVERT_FEED_TOPIC_ARN
  const jobDetails = await jobs.generate({ active: 1 })
  const jobId = jobDetails.success ? jobDetails.item.id : false
  const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false

  const customerName = event.pathParameters.customerName;
  const feedDelete = event.pathParameters.feedDelete
  const customerKey = event.multiValueHeaders.customer_api_key[0]
  if(customerKey === undefined){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Missing Customer Key"
      }, null)
    }
  }
  const feedBucket = process.env.Bucket;

  const getResponse = await directoryLib.get({ name: customerName, type: "customer" })
  const customer = getResponse.record;
  

  if (!getResponse.success || customer === undefined) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: getResponse.message
      }, null)
    }
  }else if (customer.Api_Key !== customerKey){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid customer Key"
      }, null)
    }
  }
  const params = {
    bucket: feedBucket,
    prefix: `feeds/${customerName}/`,
  }
  let supplierFeed;
  let listContent;
  try {
    listContent = await s3utils.s3List(params);
  } catch (err) {
    return {
      statusCode: 500,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: err,
      }, null, 2),
    }
  }
  const list = jp.query(listContent.Contents, '$[*].Key')
  for (let i = 0; i < list.length; i++) {
    const key = list[i];
    const spliceKey = key.slice(6)
    supplierFeed = spliceKey.split('/')[1]
    if (supplierFeed !== '') {
      const supplierName = supplierFeed.split('_')[0];
      const getResponse = await directoryLib.get({ name: supplierName, type: "supplier" })
      const supplier = getResponse.record;
      const getConfig = await directoryLib.get({ name: supplierName, type: "supplierConfig" })
      const config = getConfig.record
      publishParams = {

        Message: JSON.stringify({ bucket: feedBucket, key: key, feedDelete, customerName: customerName, customerId: customer.id, supplierId: supplier.id, jobId, jobCreatedAt, config , supplierName}),
        TopicArn: CONVERT_FEED_TOPIC_ARN
      }
      const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
    }
  }
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      fileList: list,
      JobId: jobId,
      JobCreatedAt: jobCreatedAt,
      JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
    }, null, 2)
  }

}
