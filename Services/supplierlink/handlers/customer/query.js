//Enduser Service to access product data as exported file in S3 -- Query based (POST)
const aws = require('aws-sdk');
const sns = new aws.SNS({apiVersion: '2010-03-31'});
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory')

module.exports.query = async (event) => {
  const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL

  const jobDetails = await jobs.generate({ active: 1, statusCode: 0, status: "retrieve"})
  const jobId = jobDetails.success ? jobDetails.item.id : false
  const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false

  const PRODUCT_QUERY_TOPIC_ARN = process.env.PRODUCT_QUERY_TOPIC_ARN;
  const bucket = process.env.Bucket;
  const keyName = process.env.transformedKey;

  let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : "{}";
  try {
    requestBody = JSON.parse(requestBody)
  } catch (error) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: error.message
      }, null),
    }
  }
  if (requestBody === null) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid Request",
      }, null, 2),
    }
  }

  const customerName = requestBody.customerName;
  const customerKey = event.multiValueHeaders.customer_api_key[0]
  if(customerKey === undefined){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Missing Customer Key"
      }, null)
    }
  }
  const getResponse = await directoryLib.get({ name: customerName, type: "customer" })
  const customer = getResponse.record;
  if (!getResponse.success || customer === undefined) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: getResponse.message
      }, null)
    }
  }else if (customer.Api_Key !== customerKey){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid customer Key"
      }, null)
    }
  }
  const startDateString = requestBody.startDate;
  const key = `${customerName}/${keyName}`;

  let dateStamp = new Date().toISOString().replace(/T/,'_').replace(/:/g,'').replace(/\..+/,'')

  const publishParams = {
    Message: JSON.stringify({
      Bucket: bucket,
      Key: key,
      customerName: customerName,
      ExclusiveStartKey: false,
      dateStamp,
      startDateString: startDateString,
      sequence: 1,
      productCount: 0,
      queryType: "query",
      jobId,
      jobCreatedAt
    }),
    TopicArn: PRODUCT_QUERY_TOPIC_ARN
  }

  const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
  console.log(publishResponse);
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
        bucket: bucket,
        key: `reports/${customerName}/products.json`,
        JobId: jobId,
        JobCreatedAt: jobCreatedAt,
        JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
    }, null, 2),
  }
}
