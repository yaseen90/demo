const dynamoDBUtils = require('../../utils/dynamoDBUtils');

module.exports.getAll = async () => {

  const CUSTOMER_DBTABLE = process.env.CUSTOMER_DBTABLE;
  const params = {
    TableName: CUSTOMER_DBTABLE,
    FilterExpression: "#itemtype = :itemtype",
    ExpressionAttributeNames: {
    "#itemtype": "type"
    },
    ExpressionAttributeValues: {
      ":itemtype": "customer"
    }
  }
  const queryResponse = await dynamoDBUtils.getAll(params)
  return {
    statusCode: 200,
    body: JSON.stringify({
      customers:  queryResponse,
    }, null, 2),
  }

}
