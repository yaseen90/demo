// Enduser Service to Access Product Records over Paginated API (HTTPS)

// const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const productService = require('../../lib/products/productServiceApi');
const directoryLib = require('../../lib/directory')

module.exports.queryAPI = async (event) => {
 
  let responseBody = event.body ? event.body : "{}";
  try {
    responseBody = JSON.parse(responseBody)
  } catch (error) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: error.message
      }, null),
    }
  }


  const ExclusiveStartKey = responseBody.hasOwnProperty("ExclusiveStartKey") ? responseBody.ExclusiveStartKey : false
  const customerName = event.pathParameters.customerName;
  const customerKey = event.multiValueHeaders.customer_api_key[0]
  if(customerKey === undefined){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Missing Customer Key"
      }, null)
    }
  }
  const Limit = responseBody.hasOwnProperty("Limit") ? responseBody.Limit : false
  const getResponse = await directoryLib.get({ name: customerName, type: "customer" })
  const customer = getResponse.record;
  if (!getResponse.success || customer === undefined) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: getResponse.message
      }, null)
    }
  }else if (customer.Api_Key !== customerKey){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid customer Key"
      }, null)
    }
  }
  try {
    // const result = await queryDb({
    //   DYNAMODB_TABLE,
    //   IndexName: "LSIByLastUpdated",
    //   customerName,
    //   startDate_dts,
    //   limitRecordSet,
    //   ExclusiveStartKey
    // })
    const result = await productService({ customerName, body: { ExclusiveStartKey, Limit } })
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: result,
      }, null, 2),
    }
  } catch (error) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: error.message,
      }, null, 2),
    }
  }

}

// const queryDb = async (params) => {
//   // Get Data from DB
//   const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
//   const IndexName = params.IndexName ? params.IndexName : false;
//   const customerName = params.customerName;
//   const startDate_dts = params.startDate_dts;
//   const limitRecordSet = params.limitRecordSet;
//   const ExclusiveStartKey = params.ExclusiveStartKey ? params.ExclusiveStartKey : false;

//   let dbParams = {
//     TableName: DYNAMODB_TABLE,
//     KeyConditionExpression: 'customerName = :customerName',
//     ExpressionAttributeValues: { ':customerName': customerName },
//     Limit: limitRecordSet
//   }


//   if (IndexName) {
//     dbParams.IndexName = IndexName
//   }
//   if (ExclusiveStartKey) {
//     dbParams.ExclusiveStartKey = params.ExclusiveStartKey;
//   }
// console.log(dbParams);
//   const result = await dynamoDBUtils.query(dbParams)
//   delete result.ScannedCount
//   return result;
// }
