// Enduser Service to Download Supplier Files and Process
const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory')


module.exports.retrieve = async (event) => {
  const jobProcess = process.env.DOMAINNAME + " - Customer-Retrieve";
  const RETRIEVE_FEED_TOPIC_ARN = process.env.RETRIEVE_FEED_TOPIC_ARN

  const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL
  const JOBSTORE_UPSERT_TOPIC = process.env.JOBSTORE_UPSERT_TOPIC

  const jobDetails = await jobs.generate({ JOBSTORE_UPSERT_TOPIC, active: 1, source: jobProcess })
  const jobId = jobDetails.success ? jobDetails.item.id : false
  const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false
  const customerName = event.pathParameters.customerName
  const customerKey = event.multiValueHeaders.customer_api_key[0]
  if(customerKey === undefined){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Missing Customer Key"
      }, null)
    }
  }
  const feedDelete = event.pathParameters.feedDelete
  const getResponse = await directoryLib.get({ name: customerName, type: "customer" })
  const customer = getResponse.record;
  if (!getResponse.success || customer === undefined) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: getResponse.message
      }, null)
    }
  } else if (customer.Api_Key !== customerKey) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid customer Key"
      }, null)
    }
  }

  const activeSupplierList = customer.activeSuppliers;

  for (let i = 0; i < activeSupplierList.length; i++) {

    const supplierName = activeSupplierList[i]
    const supplierConfigIndex = customer.supplierConfig.findIndex(s => { return s.name === supplierName })
    if (supplierConfigIndex !== -1) {
      const publishParams = {
        Message: JSON.stringify({
          customer,
          supplierName,
          supplierConfigIndex,
          jobId,
          jobCreatedAt,
          origin: "api",
          feedDelete
        }),
        TopicArn: RETRIEVE_FEED_TOPIC_ARN
      }
      const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })

    }

  }

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      JobId: jobId,
      JobCreatedAt: jobCreatedAt,
      JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
    }, null, 2)
  }
}
