'use strict'

const aws = require('aws-sdk');
const sns = new aws.SNS({ apiVersion: '2010-03-31' });
const jobs = require('../../lib/jobs');
const directoryLib = require('../../lib/directory')


module.exports.export = async (event) => {

  const JOBSTORE_BASEURL = process.env.JOBSTORE_BASEURL
  const jobDetails = await jobs.generate({ active: 1 })
  const jobId = jobDetails.success ? jobDetails.item.id : false
  const jobCreatedAt = jobDetails.success ? jobDetails.item.createdAt : false

  const PRODUCT_QUERY_TOPIC_ARN = process.env.PRODUCT_QUERY_TOPIC_ARN;
  //const PRODUCT_REPORT_TOPIC_ARN = process.env.PRODUCT_REPORT_TOPIC_ARN;
  //const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;
  const bucket = process.env.Bucket;
  const keyName = process.env.transformedKey;
  const customerName = event.pathParameters.customerName;
  const customerKey = event.multiValueHeaders.customer_api_key[0];
  if(customerKey === undefined){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Missing Customer Key"
      }, null)
    }
  }
  const getResponse = await directoryLib.get({ name: customerName, type: "customer" })
  const customer = getResponse.record;
  if (!getResponse.success || customer === undefined) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: getResponse.message
      }, null)
    }
  }else if (customer.Api_Key !== customerKey){
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: "Invalid customer Key"
      }, null)
    }
  }
  const key = `${customerName}/${keyName}`;

  let dateStamp = new Date().toISOString().replace(/T/, '_').replace(/:/g, '').replace(/\..+/, '')
  const publishParams = {
    Message: JSON.stringify({
      Bucket: bucket,
      Key: key,
      customerName: customerName,
      ExclusiveStartKey: false,
      dateStamp,
      sequence: 1,
      productCount: 0,
      queryType: "export",
      jobId,
      jobCreatedAt
    }),
    TopicArn: PRODUCT_QUERY_TOPIC_ARN
  }

  const publishResponse = await sns.publish(publishParams).promise().then(response => { return response })
  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      bucket: bucket,
      key: `reports/${customerName}/products.json`,
      JobId: jobId,
      JobCreatedAt: jobCreatedAt,
      JobStatusURL: `${JOBSTORE_BASEURL}\/job\/${jobId}\/${jobCreatedAt}`
    }, null, 2),
  }

}
