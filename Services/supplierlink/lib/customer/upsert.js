const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const axios = require('axios');

module.exports.upsertDB = async (params) => {
  const type = "customer";
  CUSTOMER_DBTABLE = params.CUSTOMER_DBTABLE;
  const customerRecord = params.customerRecord;
  let response
  try {
    response = await dynamoDBUtils.upsert({ DYNAMODB_TABLE: CUSTOMER_DBTABLE, item: customerRecord, type })

  } catch (err) {
    response = false
    console.log(err)
  }
  return response;
}

module.exports.upsert = async (params) => {
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  const customer = params.customer;
  let response;
  try {
    const customerUpdateResponse = await axios.post(`${DIRECTORY_BASEURL}/customer/${customer.name}`, customer)
    response = { success: customerUpdateResponse.status === 200 ? true : false, response: customerUpdateResponse, err: {} }
  } catch (err) {
    console.log(err)
    response = { success: false, response: {}, err: err };
  }
  return response;
}