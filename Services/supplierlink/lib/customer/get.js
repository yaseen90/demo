const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const axios = require('axios');

module.exports.getDB = async (params) => {
  const type = "customer";
  CUSTOMER_DBTABLE = params.CUSTOMER_DBTABLE;
  const customerName = params.customerName;
  let response
  try {
    const dbParams = {
      TableName: CUSTOMER_DBTABLE,
      Key: { type: type, name: customerName }
    }
    response = await dynamoDBUtils.get(dbParams)
  } catch (err) {
    response = false
  }
  return response;
}

module.exports.get = async (params) => {
  const type = "customer";
  const customerName = params.customerName;
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  let response = {}
  const apiKey = process.env.DIRECTORY_API_KEY;
  const config = { "x-api-key": apiKey }
  try {
    const getCustomerResponse = await axios.get(`${DIRECTORY_BASEURL}/${type}/${customerName}`, { headers: config })
    if (getCustomerResponse.status !== 200) {
      response = { success: false, record: {}, message: `HTTP response Not ok ${getCustomerResponse.status}` }
    }
    response = { success: true, record: getCustomerResponse.data.record }
  } catch (err) {
    response = { success: false, record: {}, message: `HTTP response Not ok ${err.message}` }
  }
  return response;
}
