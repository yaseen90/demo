const axios = require('axios');

module.exports.get = async (params) => {
  const type = "supplier";
  const customerName = params.customerName;
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  let response = {}
  const apiKey = process.env.DIRECTORY_API_KEY;
  const config = { "x-api-key": apiKey }
  try {
    const getCustomerResponse = await axios.get(`${DIRECTORY_BASEURL}/${type}/${customerName}`, { headers: config })
    if (getCustomerResponse.status !== 200) {
      response = { success: false, record: {} }
    }
    response = { success: true, record: getCustomerResponse.data.record }
  } catch (err) {
    response = { success: false, record: {} }
  }
  return response;
}
