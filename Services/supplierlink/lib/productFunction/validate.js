const s3Utils = require("../../utils/s3utils");
const productLib = require('../../lib/products');

//Config Maps

module.exports.validate = async (params, loadConfig) => {
    const configMap = loadConfig
    const failBucket = process.env.failProductBucket;
    const message = params;
    const product = message.product;
    const supplierName = message.supplierName
    const customerName = message.customerName
    const jobId = message.jobId;
    const jobCreatedAt = message.jobCreatedAt;
    const supplierConfigMap = configMap.SUPPLIERS
    const productIdentifier = supplierConfigMap[supplierName].productIdentifier;
    const textField = supplierConfigMap[supplierName].textField;

    const isValid = productLib.validateProduct({ product, textField, productIdentifier });
    // Ideally sohuld be try catch exception block
    if (isValid === false) {
        let key = `${customerName}/${supplierName}/${jobId}`;
        product.jobCreatedAt = jobCreatedAt;
        product.jobId = jobId;
        let failProduct = JSON.stringify(product);
        const s3UploadParams = { bucket: failBucket, key: key, content: failProduct }
        const uploadResponse = await s3Utils.s3Upload(s3UploadParams)
        console.log("product is invalid");
        // Add invalid handler
        return {
            success: false,
            statusCode: 500,
            body: "product is invalid",
        }
    }

    return {
        success: true,
        statusCode: 200,
        body: message,
    }

}
