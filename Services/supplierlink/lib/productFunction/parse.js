
const productLib = require('../../lib/products');
const productParser = require('../../lib/products/parser');
const jobs = require('../../lib/jobs');

module.exports.parse = async (params, loadConfig) => {

    const mapBucket = process.env.mapBucket;
    const message = params;
    const product = message.product;
    const jobId = message.jobId;
    const jobCreatedAt = message.jobCreatedAt;
    const supplierName = message.supplierName
    const configMap = loadConfig;
    const supplierConfigMap = configMap.SUPPLIERS
    const textField = supplierConfigMap[supplierName].textField;
    // Parse the product record
    const rules = await productLib.getRules({ supplierName, mapBucket })
    const matchAttributes = rules.map(rule => { return rule.destField })
    if (rules === false) {
        let jobDetails = await jobs.update({ active: 1, statusCode: 400, status: "false", jobId, createdAt: jobCreatedAt, message: "rules not loaded" })
    }
    const parsedProduct = await parse({ product, textField, supplierName, rules })
    if (parsedProduct === false) {
        let jobDetails = await jobs.update({ active: 1, statusCode: 400, status: "false", jobId, createdAt: jobCreatedAt, message: "some thing went rong with parsing" })
    }
    message.product = parsedProduct;
    message.matchAttributes = matchAttributes;
    return {
        success: true,
        statusCode: 200,
        body: message
    }

}

let parse = async (params) => {
    const supplierName = params.supplierName;
    const textField = params.textField;
    const product = params.product
    let rules = params.rules;
    return productParser.productSupplierParser({ textField, supplierName, rules, productRecord: product })
}
