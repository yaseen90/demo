//BESTWAY MAP
const vatCodeMap = {
  A: 0.00,
  C: 20.00,
  D: 5.00
}

module.exports.vatmapping = (params) => {

  const product = params.value
  const vatCode = product.p_vatCode.transformed
  const vatRate = vatCodeMap.hasOwnProperty(vatCode) ? vatCodeMap[vatCode] : false
  product.p_vatRate = {
    transformed: parseFloat(vatRate.toFixed(2)),
    policy: "vatmapping",
    mapped: vatRate
  }


  if (product.hasOwnProperty("appliedFunctions")) {
    product.appliedFunctions.push("vatmapping")
  } else {
    product.appliedFunctions = ["vatmapping"]
  }
  return product
}
