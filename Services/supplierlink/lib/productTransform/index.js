module.exports.passthrough = require('./passthrough').passthrough
module.exports.vatmapping = require('./vatmapping').vatmapping
module.exports.priceMarkedDesc = require('./priceMarkedDesc').priceMarkedDesc
module.exports.promoIndicator = require('./promoIndicator').promoIndicator
