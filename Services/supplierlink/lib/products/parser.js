module.exports.productSupplierParser = productSupplierParser

function productSupplierParser(params) {
  const textProperty = params.textField;
  const rules = params.rules;
  const product = params.productRecord;
  parsedProductSupplierRecord = {};
  //Get Mapped attributes
  const attributeResponse = mapAttributes(product, rules);

  //Parse mapped attributes
  attributeResponse["mappedAttributes"].forEach((attribute) => {
    let ruleIndex = rules.findIndex(o => o.sourceField === attribute)

    //Temporary provisioning to deal with different json formats coming back from convertors
    if (textProperty) {
      if (product[attribute].hasOwnProperty(textProperty)) {
        parsedProductSupplierRecord[rules[ruleIndex].destField] = product[attribute][textProperty];
      }
    } else {
      if (product[attribute]) {
        parsedProductSupplierRecord[rules[ruleIndex].destField] = product[attribute];
      }
    }

  })
  return parsedProductSupplierRecord;
}

function mapAttributes(product, rules) {
  const productAttrs = Object.keys(product);
  let ruleFound;
  const unmappedAttributes = [];
  const mappedAttributes = [];
  productAttrs.forEach((attribute) => {
    ruleFound = rules.findIndex((o) => o.sourceField === attribute)
    if (ruleFound === -1) {
      unmappedAttributes.push(attribute)
    }
    else {
      mappedAttributes.push(attribute)
    }

  });
  return { mappedAttributes, unmappedAttributes };
}
