module.exports.addOrAppend = require('./addOrAppend').addOrAppend
module.exports.productSupplierParser = require('./parser').productSupplierParser
module.exports.retrieveProductsS3JSON = require('./retrieve').retrieveProductsS3JSON
module.exports.getRules = require('./getRules').getRules;
module.exports.validateProduct = require('./validateProduct').validateProduct;
