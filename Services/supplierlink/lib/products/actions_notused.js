const _ = require('lodash');
const jp = require('jsonpath');
module.exports.upsert = (params) => {

  let productStore = params.productStore;
  const parsedOrderRecord = params.parsedOrderRecord;
  const supplierName = params.supplierName;
  parsedOrderRecord.name = supplierName;

  const productQuery = `$.store[?(@.innerEAN == '${parsedOrderRecord.innerEAN}')]`
  const nestedProductQuery = `.products[?(@.outerEAN == '${parsedOrderRecord.outerEAN}')]`
  const supplierQuery = `.suppliers[?(@.name == '${parsedOrderRecord.name}')]`
  const orderQuery = `.orders[?(@.productCode == '${parsedOrderRecord.productCode}')]`

  const orderExistQuery = `${productQuery}${nestedProductQuery}${supplierQuery}${orderQuery}`
  const supplierExistQuery = `${productQuery}${nestedProductQuery}${supplierQuery}`
  const nestedProductExistQuery = `${productQuery}${nestedProductQuery}`

  //If order exist then update order
  let orderExist
  let supplierExist
  let nestedProductExist
  let productExist
  try {
    orderExist = jp.paths(productStore, orderExistQuery);
    supplierExist = jp.paths(productStore, supplierExistQuery);
    nestedProductExist = jp.paths(productStore, nestedProductExistQuery);
    productExist = jp.paths(productStore, productQuery);
  } catch (err) {
    console.log(err)
    return productStore
  }

  if (orderExist.length == 1) {
    const orderAddress = orderExist[0];
    const storeIndex = orderAddress[2]
    const productIndex = orderAddress[4]
    const supplierIndex = orderAddress[6]
    const orderIndex = orderAddress[8]
    productStore.store[storeIndex].products[productIndex].suppliers[supplierIndex].orders[orderIndex] = parsedOrderRecord
  } else if (supplierExist.length == 1) {
    const supplierAddress = supplierExist[0];
    const storeIndex = supplierAddress[2];
    const productIndex = supplierAddress[4];
    const supplierIndex = supplierAddress[6];
    productStore.store[storeIndex].products[productIndex].suppliers[supplierIndex].orders.push(parsedOrderRecord);

  } else if (nestedProductExist.length == 1) {
    const address = nestedProductExist[0];
    const storeIndex = address[2];
    const productIndex = address[4];
    let supplierRecord = { orders: [parsedOrderRecord], name: supplierName }
    productStore.store[storeIndex].products[productIndex].suppliers.push(supplierRecord);

  } else if (productExist.length == 1) {
    const address = productExist[0];
    const storeIndex = address[2];
    let supplierRecord = { orders: [parsedOrderRecord], name: supplierName }
    let nestedProductRecord = { suppliers: [supplierRecord], innerEAN: parsedOrderRecord.innerEAN, outerEAN: parsedOrderRecord.outerEAN }
    productStore.store[storeIndex].products.push(nestedProductRecord)
  } else {
    let supplierRecord = { orders: [parsedOrderRecord], name: supplierName }
    let nestedProductRecord = { suppliers: [supplierRecord], innerEAN: parsedOrderRecord.innerEAN, outerEAN: parsedOrderRecord.outerEAN }
    let productRecord = { innerEAN: parsedOrderRecord.innerEAN, products: [nestedProductRecord] }
    productStore.store.push(productRecord)
  }
  return productStore
}
