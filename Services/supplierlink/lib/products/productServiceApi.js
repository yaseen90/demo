const axios = require("axios")

module.exports = async function (params) {
    const customerName = params.customerName;
    const body = JSON.stringify(params.body);
    const PRODUCT_API_KEY = process.env.PRODUCT_API_KEY;
    const PRODUCT_BASEURL = process.env.PRODUCT_BASEURL;
    const config = { "x-api-key": PRODUCT_API_KEY };
    const res = await axios.post(`${PRODUCT_BASEURL}/getAll/${customerName}`,  body , { headers: config })
    return res.data;
}