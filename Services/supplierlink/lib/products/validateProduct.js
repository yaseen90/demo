module.exports.validateProduct = (params) => {
  const product = params.product;
  const productIdentifier = params.productIdentifier
  const textField = params.textField
  let response = false;

  if (textField) {
    if ((product.hasOwnProperty(productIdentifier)) && (product[productIdentifier].hasOwnProperty(textField))) {
      response = true;
    }
  } else {
    if (product[productIdentifier]) { response = true }
  }

  return response;
}
