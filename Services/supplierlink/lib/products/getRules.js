
const rulesLoader = require('../../lib/rules/load');

module.exports.getRules = (params) => {
  const supplierName = params.supplierName;
  const mapBucket = params.mapBucket;
  let response;
  try {
    const supplierRuleFile = `${supplierName}.csv`;
    response = rulesLoader.rulesLoaderS3({ mapBucket, mapKey: supplierRuleFile });
  } catch (err) {
    console.log(err);
    response = false;
  }

  return response
}
