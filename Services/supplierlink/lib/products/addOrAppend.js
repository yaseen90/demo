const dynamoDBUtils = require('../../utils/dynamoDBUtils');

module.exports.addOrAppend = async (params) => {
  const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
  const masterProductIdentifier = params.masterProductIdentifier;
  const supplierName = params.supplierName;
  const productRecord = params.productRecord;
  const customerName = params.customerName;
  const transformedProduct = params.transformedProduct;
  const supplierUniqueIdentifierAttribute = params.masterproductCodeIdentifier
  const supplierUniqueIdentifier = transformedProduct[supplierUniqueIdentifierAttribute].transformed;

  let innerEAN = productRecord[masterProductIdentifier]

  const dbParams = {
    TableName: DYNAMODB_TABLE,
    Key: { innerEAN, customerName }
  }
  const dynamoDBResponse = await dynamoDBUtils.get(dbParams);

  //Product exist
  if (Object.keys(dynamoDBResponse).length) {
    // Product exist
    const productOrders = dynamoDBResponse.Item.orders

    //Check Supplier exist in orders
    const supplierFound = productOrders.findIndex(obj => {
      const response = (obj.supplierName === supplierName &&
        obj[supplierUniqueIdentifierAttribute]["transformed"] === supplierUniqueIdentifier)
      if (response === false) { console.log(`${response} >> ${obj[supplierUniqueIdentifierAttribute]["transformed"]}`) }
      return response
    })

    if (supplierFound === -1) {
      // supplier not found
      productOrders.push(transformedProduct);
    } else {
      // supplier found
      //  console.log(`supplier found:${supplierFound} -- ${supplierName}`);
      productOrders[supplierFound] = transformedProduct;
    }
    productRecord.orders = productOrders
  }
  else {
    // Product dont exist
    productRecord.orders = [transformedProduct]
    //  console.log("Not found")
  }
  //console.log(productRecord)
  return productRecord;
}