module.exports.passthrough = require('./passthrough').passthrough;
module.exports.eanpad15 = require('./eanpad15').eanpad15;
module.exports.price2d = require('./price2d').price2d;
module.exports.price3d = require('./price3d').price3d;
module.exports.stddate = require('./stddate').stddate;
module.exports.mapBoolean = require('./mapBoolean').mapBoolean;
module.exports.stringCheck = require('./stringCheck').stringCheck;
