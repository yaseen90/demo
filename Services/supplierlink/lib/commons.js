module.exports.returnError = (params) => {
  return {
    statusCode: 400,
    body: JSON.stringify({message: params.message}, null, 2)
  }
}
