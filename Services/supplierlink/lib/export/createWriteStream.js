const stream = require('stream');
const s3utils = require('../../utils/s3utils');

module.exports.createWriteStream = (Bucket, Key) => {
    const writeStream = new stream.PassThrough()
    const uploadPromise = s3utils.s3UploadStream({
        Bucket,
        Key,
        Body: writeStream
    })
    return { writeStream, uploadPromise }
}
