const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const combinedStream = require('combined-stream');

module.exports.createMergedReadStreams = (params) => {
  const Bucket = params.Bucket;
  const files = params.files;
  let mergeStream = combinedStream.create();
  let input;
  for (let i = 0; i < files.length; i++) {
    const filename = files[i]
    input = S3.getObject({ Bucket, Key: filename }).createReadStream();
    mergeStream.append(input);
  }
  return mergeStream
}
