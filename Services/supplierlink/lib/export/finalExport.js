
const exportLib = require('./')
const s3utils = require('../../utils/s3utils')

module.exports.finalExport = async (params) => {

  const Bucket = params.Bucket;
  const LAST_MERGED_KEY = params.LAST_MERGED_KEY;
  const FINAL_MERGED_KEY = params.FINAL_MERGED_KEY;
  const MERGED_PREFIX = params.MERGED_PREFIX
  const prefix = params.prefix;

  let response;
  try {

    let copyResponse = await exportLib.generateProductExport({ Bucket: Bucket, sourceKey: LAST_MERGED_KEY, exportKey: FINAL_MERGED_KEY })
    let deleteMergedResponse = await s3utils.s3DeleteFolder({ bucket: Bucket, prefix: MERGED_PREFIX })
    let deleteExportResponse = await s3utils.s3DeleteFolder({ bucket: Bucket, prefix: prefix })
    response = `Report: ${FINAL_MERGED_KEY}`;
  }
  catch (err) {
    response = err;
  }
  return {
    statusCode: 200,
    body: JSON.stringify({ message: response, }, null, 2)
  }

}
