const s3utils = require('../../utils/s3utils');
const jp = require('jsonpath');

module.exports.getFileList = async (params) => {
  const Bucket = params.Bucket;
  const prefix = params.prefix;
  let files = [];
  try {
    const s3Response = await s3utils.s3List({bucket: Bucket, prefix: prefix})
    files = jp.query(s3Response,'$.Contents[*].Key')
  }
  catch (err) {
    console.error(err);
    files = [];
  }
  return files
}
