module.exports.ftp = require('./ftp').ftp
module.exports.sftp = require('./sftp').sftp
module.exports.https = require('./https').https
