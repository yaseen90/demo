
const uuid = require('uuid').v1;
const s3Utils = require('../../utils/s3utils');
const axios = require('axios')
const exportLib = require("../export/createWriteStream");


module.exports.https = async (params) => {
  const supplierName = params.supplierName;
  console.log("supplierName",supplierName);
  const Bucket = params.Bucket;
  const bucketPrefix = params.bucketPrefix
  const host = params.host
  const user = params.user
  const xmlSchema = params.xmlSchema
  const password = params.password;
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hour = date.getHours();
  const minute = date.getMinutes();
  const secound = date.getSeconds();
  const milisecounds = date.getMilliseconds();
  const uid = uuid();
  const finalDate = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + secound + "." + milisecounds;

  let response = { files: [], uploaded: [], failed: [], success: true, message: "-" }

  try {
      const xml = xmlSchema ? xmlSchema : `<?xml version="1.0" encoding="UTF-8"?>
      <!DOCTYPE ocs-b2b-document-request SYSTEM "${host}">
      <ocs-b2b-document-request> 
         <header> 
             <authentication-info> 
                 <username>${user}</username> 
                 <password>${password}</password> 
             </authentication-info> 
              <document-info> 
              <document-uid>${uid}</document-uid> 
              <document-date>${finalDate}</document-date> 
             </document-info>
         </header>
         <body> 
             <document> 
                 <docType>catalogue</docType> 
                     <partialOrFull>T</partialOrFull> 
                     <store>${user}</store> 
                     <extended>Y</extended> 
             </document> 
         </body> 
     </ocs-b2b-document-request>
     `;
    var config = {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'text/xml',
        Accept: 'text/xml'
      }
    };
    const streamRes = await axios({ method: "POST", url: host, data: xml, headers: config.headers , responseType: 'stream' })
    const Key = `feeds/${bucketPrefix}/${supplierName}.xml`
    const { writeStream, uploadPromise } = exportLib.createWriteStream(Bucket, Key)
    streamRes.data.pipe(writeStream)
    const uploadResponse = await uploadPromise
    // const uploadResponse = await s3Utils.upload({ Body: streamRes.data, Bucket, Key, ContentType: 'text/xml' })
    response.uploaded.push(Key);
  } catch (err) {
    response.success = false
    console.log("error", err);
  }

  return response
}
