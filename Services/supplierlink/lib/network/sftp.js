
const jp = require('jsonpath');
let Client = require('ssh2-sftp-client');
let sftp = new Client();
const s3Utils = require('../../utils/s3utils')
module.exports.sftp = async (params) => {

  const host = params.host
  const port = params.port
  const prefix = params.prefix
  const username = params.username
  const KeyLocation = params.KeyLocation
  const KeyLocationType = params.KeyLocationType
  const Bucket = params.Bucket;
  const bucketPrefix = params.bucketPrefix
  let response = { files: [], uploaded: [], failed: [], success: true, message: "-" }
  let sftpListResponse;
  try {
    var privateKey = await getPrivateKey({ KeyLocation: KeyLocation, KeyLocationType: KeyLocationType })

  } catch (err) {
    console.log(err);
    response.success = false
    response.message = "Unable to get private key " + err
    return response
  }
  if (!privateKey) {
    response.success = false
    response.message = "Unable to get private key"
    return response
  }
  const sftpParams = {
    host,
    port,
    username,
    privateKey
  }
  try {
    let conn = await sftp.connect(sftpParams);
  }
  catch (err) {
    response.success = false
    response.message = "sftp connection fail" + err;
    return response
  }
  sftpListResponse = await sftp.list(prefix)
  const files = jp.query(sftpListResponse, "$[*]['name']");
  response.files = files

  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    const stream = await sftp.get(file);
    const key = `feeds/${bucketPrefix}/${file}`
    const s3UploadParams = { bucket: Bucket, key: key, content: stream }
    try {
      const uploadResponse = await s3Utils.s3Upload(s3UploadParams)
      response.uploaded.push(key)
    } catch (err) {
      response.failed.push(key)
      response.message = "Error retrieving supplier feeds  " + err
      return response
    }
  }
  await sftp.end();
  return response
}

const getPrivateKey = async (params) => {
  keyFunctions = {
    s3: s3Key,
    local: localKey
  }
  const KeyLocation = params.KeyLocation;
  const KeyLocationType = params.KeyLocationType;
  let privateKey;
  if (KeyLocationType in keyFunctions) {
    privateKey = await keyFunctions[KeyLocationType](KeyLocation)
  } else {
    console.log(`${KeyLocationType} not in keyFunctions`)
    privateKey = false
  }
  return privateKey
}

const localKey = (KeyLocation) => {
  return require('fs').readFileSync(KeyLocation).toString()
}

const s3Key = (KeyLocation) => {

  const locSplit = KeyLocation.split("/");
  const bucket = locSplit.shift();
  const key = locSplit.join("/");
  return s3Utils.s3Get({ bucket, key }).then(data => {
    return data.Body.toString();
  });
}