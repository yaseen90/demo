
var PromiseFtp = require('promise-ftp');
const ftp = new PromiseFtp();
const s3Utils = require('../../utils/s3utils');
const jp = require('jsonpath');
module.exports.ftp = async (params) => {
  const prefix = params.hasOwnProperty('prefix') ? params.prefix : "."
  const Bucket = params.Bucket;
  const bucketPrefix = params.bucketPrefix
  const productFile = params.productFile
  let fileRegex = new RegExp(productFile)
  let response = { files: [], uploaded: [], failed: [], success: true , message:"-"}
  let listResponse
  try {
    const ftpConnMessage = await ftp.connect({
      host: params.host,
      user: params.user,
      password: params.password
    })
    listResponse = await ftp.list(prefix);
    console.log(listResponse);
  } catch (err) {
    response.message = err + " ftp connection fail";
    response.success = false;
    console.log(err);
  }
  const files = jp.query(listResponse, '$[*].name')

  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (fileRegex.test(file)) {
      filePath = `${prefix}/${file}`
      const stream = await ftp.get(filePath);
      const Key = `feeds/${bucketPrefix}/${file}`
      try {
        const uploadResponse = await s3Utils.upload({ Body: stream, Bucket, Key })
        response.uploaded.push(Key);
      } catch (err) {
        console.log(`S3 Upload Fail: ${err}`);
        response.message = `S3 Upload Fail: ${err}`
        response.failed.push(Key)
        response.success = false
      }
    }
  }
  await ftp.end();

  return response
}
