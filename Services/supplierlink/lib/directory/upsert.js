const axios = require('axios');

module.exports.upsert = async(params) => {
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  const type = params.type
  const record = params.record;
  let response;
  try {
    const UpdateResponse = await axios.post(`${DIRECTORY_BASEURL}/${type}/${record.name}`, record)
    response = {success: UpdateResponse.status === 200 ? true : false, response:UpdateResponse, err: {}}
    console.log(response)
  } catch (err) {
    console.log(err)
    response = {success: false, response: {}, err: err};
  }
  return response;

}
