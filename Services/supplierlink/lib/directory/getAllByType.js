const axios = require('axios');

module.exports.getAllByType = async (params) => {
  const DIRECTORY_BASEURL = process.env.DIRECTORY_BASEURL;
  const type = params.type
  const apiKey = process.env.DIRECTORY_API_KEY;
  const config = { "x-api-key": apiKey }
  let response
  try {
    const getAllResponse = await axios.get(`${DIRECTORY_BASEURL}/${type}`, { headers: config })
    if (getAllResponse.status !== 200) {
      response = { success: false, records: [], count: '-', status: getAllResponse.status, err: {} }
    }
    response = { success: true, records: getAllResponse.data.records, count: getAllResponse.data.count, status: getAllResponse.status, err: {} }
  } catch (err) {
    response = { success: false, err: err }
  }

  return response
}
