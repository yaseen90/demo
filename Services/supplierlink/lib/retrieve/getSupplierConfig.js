const dynamoDBUtils = require('../../utils/dynamoDBUtils');
const s3utils = require('../../utils/s3utils');

module.exports.getSupplierConfig = async (params) => {
  CUSTOMERSUPPLIER_DB_TABLE = process.env.CUSTOMER_DBTABLE
  const customerName = params.customerName;
  const supplierName = params.supplierName;
  const override = params.override;
  let supplierQueryResponse = {};
  let privateKey;
  const dbParams = {
    TableName: CUSTOMERSUPPLIER_DB_TABLE,
    Key: { type: "customer", name: customerName }
  }
  const customerQueryResponse = await dynamoDBUtils.get(dbParams);
  if (!(customerQueryResponse.Item)) {
    return false
  }
  try {
    supplierQueryResponse = await dynamoDBUtils.get({
      TableName: CUSTOMERSUPPLIER_DB_TABLE,
      Key: { type: "supplier", name: supplierName }
    })

  } catch (err) {
    console.log(err)
  }

  if (!(supplierQueryResponse.Item)) {
    return false;
  }
  // Get Master Supplier Configuration
  const supplier = supplierQueryResponse.Item

  //Get Customer Supplier Configuration
  const customer = customerQueryResponse.Item
  const customerSuppliers = customer.hasOwnProperty("supplierConfig") ? customer.supplierConfig : []
  const customerSupplierIndex = customerSuppliers.findIndex(s => { return s.name === supplierName });

  const customerSupplier = customerSupplierIndex === -1 ? {} : customerSuppliers[customerSupplierIndex]

  //Combined Object
  let responseObject = { ...customerSupplier, ...supplier, ...override };

  if (Object.keys(override).length !== 0) {
    updateResponse = await updateCustomerSupplierConfig({ customer, index: customerSupplierIndex, supplier: responseObject })
  }
  //Get the key for passwordless credentials
  try {
    privateKey = await getPrivateKey({ keyLocation: responseObject.KeyLocation, keyLocationType: responseObject.KeyLocationType })
  } catch (err) {
    console.log(err);
  }

  responseObject.privateKey = privateKey;
  return responseObject;
}

const updateCustomerSupplierConfig = async (params) => {
  let customer = params.customer
  const index = params.index
  const supplier = params.supplier

  if (index === -1) {
    customer.suppliers.push(supplier)
  } else {
    customer.suppliers[index] = supplier
  }

  const dbParams = {
    DYNAMODB_TABLE: CUSTOMERSUPPLIER_DB_TABLE,
    item: customer
  }
  const dbResponse = await dynamoDBUtils.create(dbParams);
  return dbResponse
}


const getPrivateKey = async (params) => {
  keyFunctions = {
    s3: s3Key,
    local: localKey
  }

  //console.log(params)
  const keyLocation = params.keyLocation;
  const keyLocationType = params.keyLocationType;
  let privateKey;
  if (keyLocationType in keyFunctions) {
    privateKey = await keyFunctions[keyLocationType](keyLocation)
  } else {
    privateKey = false
  }
  return privateKey
}



const localKey = (keyLocation) => {
  return require('fs').readFileSync(keyLocation).toString()
}

const s3Key = (keyLocation) => {

  const locSplit = keyLocation.split("/");
  const bucket = locSplit.shift();
  const key = locSplit.join("/");
  //console.log(`Getting key: ${key}`)
  return s3utils.s3Get({ bucket, key }).then(data => {
    return data.Body.toString();
  });
}