
const dynamoDBUtils = require('../../utils/dynamoDBUtils');

module.exports.getActiveSupplierList = async (params) => {

  const CUSTOMER_DBTABLE = params.CUSTOMER_DBTABLE
  const customerName = params.customerName
  const overrideSuppliers = params.overrideSuppliers;
  const overrideSchedule = params.hasOwnProperty("overrideSchedule") && Object.keys(params.overrideSchedule).length > 0 ? params.overrideSchedule : false
  let activeSuppliers = [];
  let updateCustomerRecord = false;
  let customerRecord;
  let customerId;

  const dbParams = {
    TableName: CUSTOMER_DBTABLE,
    Key: { type: "customer", name: customerName }
  }
  const dynamoDBResponse = await dynamoDBUtils.get(dbParams);
  // customer does not exist
  if (!(dynamoDBResponse.Item)) {
    customerRecord = { name: customerName, type: "customer", activeSuppliers, suppliers: [], schedule: { enabled: false } };
    updateCustomerRecord = true;
  } else {
    customerRecord = dynamoDBResponse.Item;
    customerId = customerRecord.id
  }
  // Check if supplier records are provided in post request
  if (overrideSuppliers.length) {
    customerRecord.activeSuppliers = overrideSuppliers
    updateCustomerRecord = true
  }

  if (overrideSchedule) {
    customerRecord.schedule = updateSchedule({ customerRecord, overrideSchedule })
    updateCustomerRecord = true
  }

  if (updateCustomerRecord) {
    dbUpdateResponse = await addOrUpdateCustomer({ CUSTOMER_DBTABLE, customerRecord })
    customerId = dbUpdateResponse.item.id
  }

  activeSuppliers = customerRecord.activeSuppliers;
  return { activeSuppliers, customerId };
}

function addOrUpdateCustomer(params) {
  const type = "customer";
  CUSTOMER_DBTABLE = params.CUSTOMER_DBTABLE;
  const customerRecord = params.customerRecord;
  const dbUpdateResponsePromise = dynamoDBUtils.upsert({ DYNAMODB_TABLE: CUSTOMER_DBTABLE, item: customerRecord, type: "customer" })
  return dbUpdateResponsePromise;
}

function updateSchedule(params) {
  const customerSchedule = params.customerRecord.hasOwnProperty("schedule") ? params.customerRecord.schedule : {};
  const overrideSchedule = params.overrideSchedule;
  return { ...customerSchedule, ...overrideSchedule }
}