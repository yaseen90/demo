const fixy = require('fixy');

module.exports.fixedConvert = async (params) => {
  const fullWidth = params.fullWidth
  const filter = params.filter;
  const sanitizedSource = params.source.replace(/\r\n/g, '\n').split("\n");

  const source = sanitizedSource.filter(line => {
    let response = false
    if (filter && line[filter.operation](filter.string)) {
      if (fullWidth && line.length === fullWidth) {
        response = true
      } else {
        console.log(`Invalid size: ${line.length} -- ${line}`)
      }
    }
    return response
  }).join('\n');

  const response = fixy.parse({ map: params.headers, options: { fullwidth: fullWidth, skiplines: null, format: "json" } }, source);
  return JSON.stringify({ products: response }, 0, 4);
}
