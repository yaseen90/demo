const convert = require('xml-js');

module.exports.xmlConvert = (params) => {
  return convert.xml2json(params.source, {compact: true, spaces: 4});

}
