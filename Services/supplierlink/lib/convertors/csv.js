const csv = require('csvtojson');

module.exports.csvConvert = async (params) => {
  const response = await csv({headers: params.headers,noheader:true}).fromString(params.source)
  return  JSON.stringify({products: response},0,4)
}
