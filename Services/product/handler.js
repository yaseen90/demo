'use strict';

const handlers = require("./handlers")
module.exports.insertProductHandler = handlers.insert;
module.exports.insertApiProductHandler = handlers.insertApi;
module.exports.getOneProductHandler = handlers.getOne;
module.exports.getAllProductHandler = handlers.getAll;
module.exports.updateProductHandler = handlers.update;
