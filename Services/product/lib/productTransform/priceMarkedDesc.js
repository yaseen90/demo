module.exports.priceMarkedDesc = (params) => {

  const product = params.value
  const description = product.p_longDescription.transformed
  const shortDesc = product.p_shortDescription.transformed
  var regex = /PM[P, ,£,€][ ,£,€,0-9]/;
  if (regex.test(description) || regex.test(shortDesc)) {
    product.p_prePricedFlag = { transformed: true, policy: "priceMarkedDesc", mapped: true }
  } else {
    product.p_prePricedFlag = { transformed: false, policy: "priceMarkedDesc", mapped: false }
  }
  if (product.hasOwnProperty("appliedFunctions")) {
    product.appliedFunctions.push("priceMarkedDesc")
  } else {
    product.appliedFunctions = ["priceMarkedDesc"]
  }
  return product
}
