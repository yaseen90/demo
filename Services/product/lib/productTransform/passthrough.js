function returnValue (value ) { return value}

const typeCastFunctionMap = {
  integer: parseInt,
  float: parseFloat,
  default: returnValue
}

const fieldTypeMap = {
  p_numberUnitsPack: "integer",
  p_costPrice: "float",
  p_vatRate: "float",
  p_rsp: "float",
  p_promoRSP: "float",
  p_vatRate: "float"
}

module.exports.passthrough = (params) => {
  let product = params.value;
  let attribute;
  let fieldType;
  let typeCastFunction;
  const productAttributes = Object.keys(product)

  for (let i = 0; i < productAttributes.length ; i++){
    attribute = productAttributes[i]
    if (fieldTypeMap.hasOwnProperty(attribute)) {
      fieldType = fieldTypeMap[attribute]
      typeCastFunction = typeCastFunctionMap[fieldType]
      product[attribute].transformed = typeCastFunction(product[attribute].transformed)
      product[attribute].typed = fieldTypeMap[attribute]
    }
  }

  if (product.hasOwnProperty("appliedFunctions")) {
    product.appliedFunctions.push("passthrough")
  }else {
    product.appliedFunctions = ["passthrough"]
  }
  return product
}
