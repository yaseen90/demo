const csv = require('csvtojson');
const s3utils = require('../../utils/s3utils');
module.exports.rulesLoader = rulesLoader;
module.exports.rulesLoaderS3 = rulesLoaderS3;
function rulesLoader(csvFile) {
  return csv().fromFile(csvFile);
}
function rulesLoaderS3(params) {
  const bucket = params.bucket;
  const mapKey = params.mapKey;
  return s3utils.s3Get({ bucket: bucket, key: mapKey }).then(rulesContent => {
    const csvContent = rulesContent.Body.toString();
    return csv().fromString(csvContent)
  });
}
