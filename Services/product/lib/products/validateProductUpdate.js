module.exports.validateProductUpdate = (params) => {
  const product = params.product;
  const productIdentifier = params.productIdentifier;
  const textField = params.textField;

  let response = {
    success:true,
    message:""
  };

  if (textField) {
    if ((product.hasOwnProperty(productIdentifier))) {
      response = {
        success:false,
        message:`${productIdentifier} is Not Updated`
      };
    }
  } else {
    if (product[productIdentifier]) { 
      response = {
        success:false,
        message:`${productIdentifier} is Not Updated`
      };
    }
  }


  return response;
}