module.exports.validateProduct = (params) => {
  const product = params.product;
  const mandatorySupplierList = params.mandatory;
  const productIdentifier = params.productIdentifier
  const textField = params.textField
  let response = {
    success:false,
    message:""
  };
  if (textField) {
    if ((product.hasOwnProperty(productIdentifier)) && (product[productIdentifier].hasOwnProperty(textField))) {
      response = {
        success:true,
        message:""
      };
    }else{
      response = {
        success:false,
        message:`${productIdentifier} OR ${textField} is Mandatory`
      };
    }
  } else {
    if (product[productIdentifier]) {
      response = {
        success:true,
        message:""
      };
    }else{
      response = {
        success:false,
        message:`${productIdentifier} is Mandatory`
      };
    }
  }
  if(mandatorySupplierList){
    let productKeys = Object.keys(product);
    let validate = mandatorySupplierList.every(val => productKeys.includes(val))
    if (!validate) {
      response = {
        success:false,
        message:`${mandatorySupplierList} is Mandatory`
      };
    }
  }

  return response;
}
