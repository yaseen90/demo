
const jp = require('jsonpath');
const s3Helpers = require('../../utils/s3utils');
const converter = require('../../utils/converter');
//Config Maps
const loadConfig = require('../../Config')

module.exports.retrieveProductsS3JSON = async (params) => {
  const configMap = await loadConfig();
  const supplierConfigMap = configMap.SUPPLIERS

  const supplierName = params.supplierName;
  const bucketName = params.bucketName;
  const sourceKey = params.sourceKey;
  const convert = params.convert
  // Load the Feed
  const feedResponse = await s3Helpers.s3Get({ bucket: bucketName, key: sourceKey });
  let jsonDoc;
  if (convert) {
    const responseBody = feedResponse.Body.toString();
    jsonDoc = converter.xmltojson(responseBody)
  } else {
    jsonDoc = feedResponse.Body.toString();
  }

  let response = {}
  try {
    const rawJSON = JSON.parse(jsonDoc);
    const productLoadQueryString = supplierConfigMap[supplierName].productLoadQueryString
    const productIdQueryString = supplierConfigMap[supplierName].productIdQueryString
    const products = jp.query(rawJSON, productLoadQueryString);
    const productIds = jp.query(products, productIdQueryString);
    response = { products, productIds, err: false, message: {} }

  } catch (err) {
    response = { products: [], productIds: [], error: true, message: err }
  }
  return response
}
