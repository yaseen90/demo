module.exports.stringCheck = (params) => {

  // Performs basic checks on selected strings for incorrect characters.
  // Primary checks are for � where £ should be used, generally when wrong code page
  // is set on the file.

  let response;
  let str = params.value;
  let poundRegex = /�/;

  response = str.replace(poundRegex, '£')

  return response
}
