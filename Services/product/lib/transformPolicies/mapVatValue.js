module.exports.vatValue = (params) => {
    const value = params.value;
    const vatObject = {
        A: "ABC",
        B: "BCD",
        C: "CDE",
        D: "DEF",
        E: "EFG",
        F: "FGH",
        G: "GHI",
        H: "HIJ",
        I: "IJK",
        J: "JKL",
        K: "JKL",
        L: "KLM",
        M: "LMN",
        N: "MNO",
        O: "NOP",
        P: "OPQ",
        Q: "PQR",
        R: "QRS",
        S: "RST",
        T: "STU",
        U: "TUV",
        V: "UVW",
        W: "VWX",
        X: "WXY",
        Y: "XYZ",
        Z: "ZXY"
    }
    let mapValue = vatObject.hasOwnProperty(value) ? vatObject[value] : null;
    return mapValue
}