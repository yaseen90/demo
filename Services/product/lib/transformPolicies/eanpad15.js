module.exports.eanpad15 = (params) => {
  let response ;
  try {
    response = `${params.value}`.padStart(15, '0')

  } catch (err) {
    console.log(err)
    response = false
  }
  return response
}
