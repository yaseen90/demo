module.exports = function (item) {

    let errors = {};

    if (!item.hasOwnProperty("customerName")) { errors.customerName = "customerName is Required" }
    else if (typeof (item.customerName) !== "string") { errors.customerName = "customerName is Required as a String" }
    else if (item.customerName.trim() === "") { errors.customerName = "customerName value is required" }

    if (!item.hasOwnProperty("supplierName")) { errors.supplierName = "supplierName is Required" }
    else if (typeof (item.supplierName) !== "string") { errors.suplierName = "supplierName is Required as a String" }
    else if (item.supplierName.trim() === "") { errors.supplierName = "supplierName value is required" }


    if (!item.hasOwnProperty("innerEAN")) { errors.innerEAN = "innerEAN is Required" }
    else if (typeof (item.innerEAN) !== "string") { errors.innerEAN = "innerEAN is Required as a String" }
    else if (item.innerEAN.trim() === "") { errors.innerEAN = "innerEAN value is required" }
   
    if (!item.hasOwnProperty("masterproductCodeIdentifier")) { errors.masterproductCodeIdentifier = "masterproductCodeIdentifier is Required" }
    else if (typeof (item.masterproductCodeIdentifier) !== "string") { errors.masterproductCodeIdentifier = "masterproductCodeIdentifier is Required as a String" }
    else if (item.masterproductCodeIdentifier.trim() === "") { errors.masterproductCodeIdentifier = "masterproductCodeIdentifier value is required" }
    if (!item.hasOwnProperty("masterproductCodeIdentifierValue")) { errors.masterproductCodeIdentifierValue = "masterproductCodeIdentifierValue is Required" }
    else if (typeof (item.masterproductCodeIdentifierValue) !== "string") { errors.masterproductCodeIdentifierValue = "masterproductCodeIdentifierValue is Required as a String" }
    else if (item.masterproductCodeIdentifierValue.trim() === "") { errors.masterproductCodeIdentifierValue = "masterproductCodeIdentifierValue value is required" }


    if (!item.hasOwnProperty("product")) { errors.product = "product is Required" }
    else if (typeof (item.product) !== "object") { errors.product = "product is Required as a object" }
    else if (Object.keys(item.product).length === 0) { errors.product = "product is Required as a object"  }

    return errors;
}