module.exports.insert = require("./insert").insert;
module.exports.insertApi = require("./insertApi").insertApi;
module.exports.getOne = require("./getOne").getOne;
module.exports.getAll = require("./getAll").getAll;
module.exports.update = require("./update").update;