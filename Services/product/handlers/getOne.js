// Enduser Service to Access Product Records over Paginated API (HTTPS)

const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.getOne = async (event) => {
    const customerName = event.pathParameters.customerName;
    const innerEAN = event.pathParameters.innerEAN;
    const DYNAMODB_TABLE = process.env.DYNAMODB_TABLE;
    try {
        let data = await dynamoDBUtils.get({ TableName: DYNAMODB_TABLE, Key: { customerName, innerEAN } })
        return {
            statusCode: 200,
            body: JSON.stringify({
                data
            }, null, 2)
        }
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(error.message, null, 2)
        }
    }

}
