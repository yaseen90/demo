'use strict'

// Version 10-10-19-01
const uuid = require('uuid');
const aws = require('aws-sdk');
const dynamoDB = new aws.DynamoDB.DocumentClient();

module.exports.create = create;
module.exports.createOrUpdateByDts = createOrUpdateByDts;
module.exports.getAll = getAll;
module.exports.getAllPage = getAllPage;
module.exports.query = query;
module.exports.get = get;
module.exports.delete = remove;



//For product
function create(params) {

    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    let item = params.item;

    const d = new Date();

    const timeStamp = d.getTime();
    const timeString = d.toISOString();

    item.lastUpdated_String = timeString;
    item.lastUpdated = timeStamp;
    item.createdAt = timeStamp
    item.uuid = uuid.v1()
    const dynamoDBParams = {
      TableName: DYNAMODB_TABLE,
      Item: item
    };

    return dynamoDB.put(dynamoDBParams).promise().then(data => {
      return ({success: true, item: item, error: {}})
    }).catch( error => {
      return ({success:false, item:{}, error:error})
    })

}

function createOrUpdateByDts(params) {

    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    let item = params.item;

    const d = new Date();

    const timeStamp = d.getTime();
    const timeString = d.toISOString();
    item.lastUpdated = timeStamp;
    item.lastUpdated_String = timeString;

    if (! (item.hasOwnProperty("createdAt"))) { item.createdAt = timeStamp}
    if (! (item.hasOwnProperty("uuid"))) { item.uuid = uuid.v1() }

    const dynamoDBParams = {
      TableName: DYNAMODB_TABLE,
      Item: item
    };

    return dynamoDB.put(dynamoDBParams).promise().then(data => {
      return ({success: true, item: item, error: {}})
    }).catch( error => {
      return ({success:false, item:{}, error:error})
    })

}

function getAll(params) {
  return dynamoDB.scan(params).promise().then(result => { return result.Items})
}
function getAllPage(params) {
  return dynamoDB.scan(params).promise().then(result => { return result})
}
function query(params) {
  return dynamoDB.query(params).promise().then(result => { return result})
}
function get(params) {

//  params = { TableName: "",
  //          Key: {}
  //}

  return dynamoDB.get(params).promise()
}
function remove(params) {
  return dynamoDB.delete(params).promise((response) => {
    return response
  })
}
