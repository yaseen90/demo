'use strict';
const fs = require('fs');
const aws = require('aws-sdk');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });

const csv = require('csvtojson');
const {parse} = require('json2csv')
const csvParams = {
    noheader:true,
    trim:true,
    headers: ['id','type','till-id','trn-id','trn-line-num','shift-num','Number1','Number2','Number3','Number4','Number5','Number6','Number7','Number8','Number9','Number10','Number11','Number12','Number13','Number14','Number15','Number16','Number17','Number18','Number19','Number20','Alpha1','Alpha2','Alpha3','Alpha4','Alpha5','Alpha6','Alpha7','Alpha8','Alpha9','Alpha10','Timestamp1','Timestamp2','Timestamp']
}

module.exports.mapper = async (event) => {


  const map = fs.readFileSync('data/BarcodeMappings-HC.csv')
  const parsedMap = await csv().fromString(map.toString());
  const bucket = event.Records[0].s3.bucket.name;
  const key = event.Records[0].s3.object.key;
  let rowCount = 0;
  let matchCount = 0;
  let noMatchCount = 0;
  let correctCount = 0;
  let incorrectCount = 0;

  console.log(bucket + "-" + key)

  const uploadKey = key.replace('uploads/', 'exports/')
  const archivedKey = key.replace('uploads/', 'archives/');

  console.log("Upload Key - " + uploadKey)
  console.log("Archived Key - " + archivedKey)

  const getParams = {
    Bucket: bucket,
    Key: key
    }
  const content = await s3.getObject(getParams).promise();
  const contentBody = content.Body.toString();
  const response = await csv(csvParams).fromString(contentBody)

  //Need to pass through all of the records, not just a certain type
  const filteredRecords = response
  //const filteredRecords = response.filter(record => {return record["type"] === "4"})

  let mappedRecords = []
  for (let i=0; i < filteredRecords.length; i++) {
    rowCount++
    const record = filteredRecords[i]
    const barcode = record["Alpha2"]
    // get barcode variants for searching
    const barcode13 = barcode.padStart(13, '0')

    //check if barcode is empty
    if (barcode == "") {
      mappedRecords.push(record)
    } else {

      //check if either barcode format matches matches (not efficient this way)
      let parsedMapResponse = parsedMap.find(mapRecord => { return barcode13 === mapRecord.Barcode})
      if (parsedMapResponse === undefined) {
        //look for the short version of the barcode
        parsedMapResponse = parsedMap.find(mapRecord => { return barcode === mapRecord.Barcode})
      }

      if (parsedMapResponse === undefined) {
          // Increment noMatchCount
          noMatchCount++
          mappedRecords.push(record)
      } else {
          // Increment match counter
          matchCount++
          record.L1 = parsedMapResponse.L1
          record.L2 = parsedMapResponse.L2
          mappedRecords.push(record)
      }
    }
  }
  // Add a total row for the counts
  console.log("rowCount: " + rowCount + " matchCount: " + matchCount + " noMatchCount: " + noMatchCount + " correctCount:" + correctCount)

  const csvParse = parse(mappedRecords)



  const s3PutResponse = await s3.putObject({Body: csvParse, Bucket: bucket, Key: uploadKey, ContentType: "text/csv"}).promise();
  const s3ArchiveResponse = await s3.copyObject({ Bucket: bucket, CopySource: `${bucket}/${key}`,Key: archivedKey}).promise()
  const s3DeleteResponse = await s3.deleteObject({Bucket: bucket, Key: key}).promise()

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v1.0! Your function executed successfully!',
      input: event,
    }, null, 2),
  };

};
