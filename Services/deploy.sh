if [ $# -ne 2 ]
then
  echo "Invalid Usage..."
  echo "Usage: $0 servicename stage"
  echo "Services: product|directory|supplierlink|jobstore"
  echo "Stages: dev|test|prod"
  echo "terminating..."
  exit 2
fi


SERVICE=$1
STAGE=$2


  if [[ "$SERVICE" =~ ^(product|directory|supplierlink|jobstore)$ ]]
  then
    BASEDIR=`dirname $0`
    SERVICE_DIRECTORY="$BASEDIR/$SERVICE"
    if [ -d $SERVICE_DIRECTORY ]
    then
      if [[ "$STAGE" =~ ^(dev|stage|prod)$ ]]
      then
        cd $SERVICE_DIRECTORY
        serverless deploy --stage $STAGE
        cd $BASEDIR
        exit 0
      else
        echo "Invalid Stage $STAGE"
      fi
    else
      echo "$SERVICE_DIRECTORY does not exist"
      exit 2
    fi
  else
    echo "Invalid Usage..."
    echo "Usage: $0 servicename stage"
    echo "Services: product|directory|supplierlink|jobstore"
    echo "Stages: dev|test|prod"
    echo "terminating..."
    exit 2

  fi
