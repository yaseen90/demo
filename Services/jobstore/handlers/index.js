module.exports.get = require('./get').get;
module.exports.create = require('./create').create;
module.exports.upsertSNS = require('./upsertSNS').upsertSNS;
module.exports.auditSNS = require('./auditSNS').auditSNS;
module.exports.update = require('./update').update;
