
const aws = require('aws-sdk');
const sqs = new aws.SQS({ region: "eu-west-1" })

module.exports.update = async (event) => {

  const UpsertQueue = process.env.UpsertQueue
  const CUSTOM_BASEURL = process.env.CUSTOM_BASEURL

  const id = event.pathParameters.id;
  let createdAt = parseInt(event.pathParameters.createdAt) ;
  let requestBody = event.hasOwnProperty("body") && event.body !== null ? event.body : {};
  try {
    requestBody = JSON.parse(requestBody)
  } catch (error) {
    return {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        message: error.message
      }, null),
    }
  }
  let statusCode = 200
  let response = {item: {}, success: false, message: "pending"}
  let item = (({
              active,
              status,
              statusCode,
              source,
              message}) => ({
                          active,
                          status,
                          statusCode,
                          source,
                          message}))(requestBody);

  item.id =   id;
  item.createdAt = createdAt;

  const SqsParams = {
    MessageBody: JSON.stringify({ item }),
    QueueUrl: UpsertQueue
  }


  try {
    
    const publishResponse = await sqs.sendMessage(SqsParams).promise()
    response.message = publishResponse
  }
  catch(err) {
    console.log(err)
    response.error = err
    response.success = false
  }


  response = {
    url: `${CUSTOM_BASEURL}/job/${item.id}/${item.createdAt}`
  }

  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      response,
    }, null, 2),
  }

}
