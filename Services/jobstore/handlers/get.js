'use strict';

const dynamoDBUtils = require('../utils/dynamoDBUtils');

module.exports.get = async (event) => {


  const JOBSTORE_DBTABLE = process.env.JOBSTORE_DBTABLE;
  const id = event.pathParameters.id;
  let createdAt = event.pathParameters.createdAt ;
  if (isNaN(createdAt)) {
    return {
      statusCode: 500,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        error: "Invalid request",
      }, null, 2),
    }

  }

  createdAt = parseInt(createdAt)

  console.log(`${id} -- ${createdAt}`);

  const params = {
      TableName: JOBSTORE_DBTABLE,
      Key: {id: id, createdAt: createdAt }
  }

  let dynamoDBResponse = "";
  try {
     dynamoDBResponse = await dynamoDBUtils.get(params);
  } catch (err) {
    return {
      statusCode: 500,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
      },
      body: JSON.stringify({
        error: err,
      }, null, 2),
    }

  }

  const jobObject = dynamoDBResponse.Item
  const statusCode = 200;
  return {
    statusCode: statusCode,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
    body: JSON.stringify({
      job: jobObject,
    }, null, 2),
  }

}
