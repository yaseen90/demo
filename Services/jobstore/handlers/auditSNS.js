const aws = require('aws-sdk');

// For AWS Services
const s3 = new aws.S3({ apiVersion: '2006-03-01' });
const s3Utils = require("../utils/s3utils");

module.exports.auditSNS = async (event) => {

  const auditLogsBucket = process.env.bucket;
  const messageString = event.Records[0].body
  const message = JSON.parse(messageString);
  const customerName = message.customerName;
  const jobId = message.jobId;
  const jobCreatedAt = message.jobCreatedAt;
  const log = message.log;
  const key = `auditLogs/${customerName}/${jobId}`
  let auditLog
  try {
    const s3GetParams = { Bucket: auditLogsBucket, Key: key }
    const s3Response = await s3.getObject(s3GetParams).promise();
    auditLog = JSON.parse(s3Response.Body)
  } catch (error) {
    auditLog = {
      jobId,
      audit: [],
      jobCreatedAt
    }
  }
  auditLog.audit.push(log)
  const s3UploadParams = { bucket: auditLogsBucket, key: key, content: JSON.stringify(auditLog) }
  const uploadResponse = await s3Utils.s3Upload(s3UploadParams)

  return {
    statusCode: 200,
    body: JSON.stringify({
      uploadResponse,
    }, null, 2),
  }

}
