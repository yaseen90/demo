
const uuidv1 = require('uuid/v1');
const dynamoDBUtils = require('../utils/dynamoDBUtils')

module.exports.upsertSNS = async (event) => {

  let response;
  const DYNAMODB_TABLE = process.env.JOBSTORE_DBTABLE;
  const messageString = event.Records[0].body
  const message = JSON.parse(messageString);


  console.log(message);

  let item = (({ id,
    createdAt,
    active,
    status,
    statusCode,
    source,
    message }) => ({
      id,
      createdAt,
      active,
      status,
      statusCode,
      source,
      message
    }))(message.item);


  const dbParams = {
    item,
    DYNAMODB_TABLE
  }

  console.log(dbParams);
  try {
    response = await dynamoDBUtils.createOrUpdateByDts(dbParams)
    console.log(response)
  }
  catch (err) {
    response = err
    console.log(err);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      response,
    }, null, 2),
  }

}
