
//For S3 read/writes
const aws = require('aws-sdk');

// For AWS Services
const s3 = new aws.S3({ apiVersion: '2006-03-01' });


module.exports.s3Upload = (params) => {

  const content = params.content;
  const bucket = params.bucket;
  const key = params.key;
  // upload json
  const   putParam = {
       Body: content,
       Bucket: bucket,
       Key: key,
       ContentType: "application/json"
     }
  return s3.putObject(putParam).promise();

}