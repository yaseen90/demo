'use strict'

// Version 10-10-19-01
const jp = require('jsonpath');
const uuid = require('uuid');
const aws = require('aws-sdk');
const dynamoDB = new aws.DynamoDB.DocumentClient();

module.exports.create = create;
module.exports.createOrUpdateByDts = createOrUpdateByDts;
module.exports.getAll = getAll;
module.exports.getAllPage = getAllPage;
module.exports.query = query;
module.exports.get = get;
module.exports.delete = remove;



const getItemId = async(params) => {

const idPrependChars = {
    supplier: "s",
    customer: "c",
    product: "p"
  }
  const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
  let item = params.item;
  let type = params.type;
  let char = idPrependChars[type]
  let nextId = -1;
  let responseId
  let itemIds;

  const getDBParams = {
    TableName: DYNAMODB_TABLE,
    Key: { name: item.name, type: type}
  }
  const getItemResponse = await get(getDBParams)

  if (Object.keys(getItemResponse).length){ return  getItemResponse.Item.id; }


  console.log(getItemResponse);
   // Getting Next ID
  const dbParams = {
    TableName: DYNAMODB_TABLE,
    FilterExpression: "#itype = :itemtype",
    ExpressionAttributeValues: {':itemtype': type},
    ExpressionAttributeNames: {'#itype': "type"}
  }

  try {
      const queryResponse = await getAll(dbParams);
      console.log(queryResponse);
      itemIds = jp.query(queryResponse,`$[?(@.type=="${type}")].id`)
      console.log(itemIds);
  } catch (err) { console.log(err) }

  if (Array.isArray(itemIds) && itemIds.length) {
      const itemIntIds = itemIds.map(item => {return item.substr(1)})
      console.log(itemIntIds);
      const highestValue = Math.max(...itemIntIds)
      nextId = `${highestValue + 1}`.padStart(3,'0');
  }
  else if (itemIds.hasOwnProperty("id")) {
      nextId = itemIds.id
  } else {
      nextId = '001';
  }

  nextId = `${char}${nextId}`;
  console.log(nextId);
  responseId = nextId


  return responseId;

}
function getInvalidId(){ return "-"}

const ID_FUNCTIONS = {
  "supplier": getItemId,
  "customer": getItemId,
  "invalid": getInvalidId
}

//For customer and supplier
module.exports.upsert = async (params) => {
    console.log("Upserting...")
    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    let item = params.item;
    let type = params.type;

    let getItemIdFunction = ID_FUNCTIONS.hasOwnProperty(type) ? ID_FUNCTIONS[type] : ID_FUNCTIONS.invalid
    const itemId = await getItemIdFunction({type, item, DYNAMODB_TABLE});

    item.id = itemId;
    item.uuid  = uuid.v1();

    const timeStamp = new Date().getTime();
    item.lastUpdated = timeStamp;
    item.createdAt = timeStamp;

    const dynamoDBParams = {
      TableName: DYNAMODB_TABLE,
      Item: item
    };

    return dynamoDB.put(dynamoDBParams).promise().then(data => {
      return ({success: true, item: item, error: {}})
    }).catch( error => {
      return ({success:false, item:{}, error:error})
    })

}

//For product
function create(params) {

    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    let item = params.item;

    const d = new Date();

    const timeStamp = d.getTime();
    const timeString = d.toISOString();

    item.lastUpdated_String = timeString;
    item.lastUpdated = timeStamp;
    item.createdAt = timeStamp
    item.uuid = uuid.v1()

    const dynamoDBParams = {
      TableName: DYNAMODB_TABLE,
      Item: item
    };

    return dynamoDB.put(dynamoDBParams).promise().then(data => {
      return ({success: true, item: item, error: {}})
    }).catch( error => {
      return ({success:false, item:{}, error:error})
    })

}

function createOrUpdateByDts(params) {

    const DYNAMODB_TABLE = params.DYNAMODB_TABLE;
    let item = params.item;

    const d = new Date();

    const timeStamp = d.getTime();
    const timeString = d.toISOString();
    item.lastUpdated = timeStamp;
    item.lastUpdated_String = timeString;

    if (! (item.hasOwnProperty("createdAt"))) { item.createdAt = timeStamp}
    if (! (item.hasOwnProperty("uuid"))) { item.uuid = uuid.v1() }

    const dynamoDBParams = {
      TableName: DYNAMODB_TABLE,
      Item: item
    };

    return dynamoDB.put(dynamoDBParams).promise().then(data => {
      return ({success: true, item: item, error: {}})
    }).catch( error => {
      return ({success:false, item:{}, error:error})
    })

}

function getAll(params) {
  return dynamoDB.scan(params).promise().then(result => { return result.Items})
}
function getAllPage(params) {
  return dynamoDB.scan(params).promise().then(result => { return result})
}
function query(params) {
  return dynamoDB.query(params).promise().then(result => { return result})
}
function get(params) {
  return dynamoDB.get(params).promise().then(data => {
    return data
  }).catch( error => { return error })
}
function remove(params) {
  return dynamoDB.delete(params).promise((response) => {
    return response
  })
}
