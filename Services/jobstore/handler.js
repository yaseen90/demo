'use strict';


const handlers = require('./handlers');
module.exports.getJobHandler = handlers.get
module.exports.createJobHandler = handlers.create
module.exports.upsertSNSJobHandler = handlers.upsertSNS
module.exports.auditSNSJobHandler = handlers.auditSNS

module.exports.updateJobHandler = handlers.update
