import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
import UserProfileLite from "./views/AddCustomer";
import Supplier from "./views/AddSupplier"
import ListSupplierData from "../src/components/list-supplier-data/listSupplier"
// import Errors from "./views/Errors";
// import ComponentsOverview from "./views/ComponentsOverview";
// import Tables from "./views/Tables";
// import Dashboard from "./views/Dashboard";
import CustomerAndSupplierList from "./views/customerAndSupplierList";
import CustomerDetails from "./views/CustomerDetails";
// import UserActions from "./components/layout/MainNavbar/NavbarNav/UserActions";
export default [
  // {
  //   path: "/dashboard",
  //   layout: DefaultLayout,
  //   component: Dashboard
  // },
  {
    path: "/add-customer",
    layout: DefaultLayout,
    component: UserProfileLite
  },
  {
    path: "/edit-customer/:name",
    layout: DefaultLayout,
    component: UserProfileLite
  },


  {
    path: "/add-supplier",
    layout: DefaultLayout,
    component: Supplier
  },
  {
    path: "/edit-supplier/:name",
    layout: DefaultLayout,
    component: Supplier
  },


  {
    path: "/list",
    layout: DefaultLayout,
    component: CustomerAndSupplierList
  },
 
  
  {
    path: "/customer-details/:name",
    layout: DefaultLayout,
    component: CustomerDetails
  },

  
  {
    path: "/supplier-list-data/:name",
    layout: DefaultLayout,
    component: ListSupplierData
  },
  
  
  
  


  // {
  //   path: "/add-new-post",
  //   layout: DefaultLayout,
  //   component: AddNewPost
  // },
  // {
  //   path: "/errors",
  //   layout: DefaultLayout,
  //   component: Errors
  // },
  // {
  //   path: "/components-overview",
  //   layout: DefaultLayout,
  //   component: ComponentsOverview
  // },
  // {
  //   path: "/tables",
  //   layout: DefaultLayout,
  //   component: Tables
  // },
  // {
  //   path: "/blog-posts",
  //   layout: DefaultLayout,
  //   component: BlogPosts
  // }
];
