import axios from "axios"

const url = "https://jobstore.api.htec-labs.io/dev/job";


async function jobStatusGet(params) {
    const response = await axios.get(params.url);
    return response;
}


export { jobStatusGet }