import axios from "axios"

const url = "https://supplierlink.api.htec-labs.io/dev/customer";
const apiKey = "nQbAwb6wzd5F3vpj0knxk3IRyRxmJCEeaaFff3nN";


async function retrieveAndImport(params) {
    const config = { 
        "x-api-key": apiKey,
        "customer_api_key" : params.customerKey,
    };
    const response = await axios.get(`${url}/${params.type}/${params.name}/false`, { headers: config });
    return response;
}
async function exportFeed(params) {
    const config = { 
        "x-api-key": apiKey,
        "customer_api_key" : params.customerKey,
    };
    const response = await axios.get(`${url}/export/${params.name}`, { headers: config });
    return response;
}


export { retrieveAndImport , exportFeed}