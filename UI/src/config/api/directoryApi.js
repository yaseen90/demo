import axios from "axios"

const url = "https://directory.api.htec-labs.io/dev/record";
const apiKey = "40rSbrzRQm3z03bi8EbYM5uN6KvLQOnu7ysOcU65";
const config = { "x-api-key": apiKey };

async function directory(body) {
    const response = await axios.post(url, body, { headers: config });
    return response;
}
async function directoryGet(params) {
    const response = await axios.get(`${url}/${params}`, { headers: config });
    return response;
}
async function directoryGetOne(params) {
    const response = await axios.get(`${url}/${params.type}/${params.name}`, { headers: config });
    return response;
}
async function directoryEditOne(params) {
    const response = await axios.post(`${url}/${params.type}/${params.name}`, params.body, { headers: config });
    return response;
}

export { directory , directoryGet , directoryGetOne, directoryEditOne}