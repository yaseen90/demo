import Dispatcher from '../dispatcher';

class SupplierConfigAction {
  suppConfigData(item) {
    Dispatcher.dispatch({
      actionType: 'NEW_SUPPLIER_CONFIG',
      payload: item
    })
  }
}

export default new SupplierConfigAction();