import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Container, Row, Col } from "shards-react";
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import PageTitle from "../components/common/PageTitle";
import CustomerList from "./CustomerList"
import SupplierList from "./SupplierList"
import Typography from '@material-ui/core/Typography';

const ExpansionPanel = withStyles({
    root: {
        border: '1px solid rgba(0, 0, 0, .125)',
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },
        '&$expanded': {
            margin: 'auto',
        },
    },
    expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
    root: {
        backgroundColor: 'rgba(0, 0, 0, .03)',
        borderBottom: '1px solid rgba(0, 0, 0, .125)',
        marginBottom: -1,
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiExpansionPanelDetails);

export default function CustomerAndSupplierList() {
    const [expanded, setExpanded] = React.useState('panel1');

    const handleChange = panel => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    return (
        <div>
            <Container fluid className="main-content-container px-4 pb-4">
                <Row noGutters className="page-header py-4">
                    <PageTitle sm="4" title="List Customer and Supplier" subtitle="Record" className="text-sm-left" />
                </Row>
                <Row>
                    {/* Editor */}
                    <Col lg="12" md="12">
                        <ExpansionPanel square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                            <ExpansionPanelSummary aria-controls="panel1d-content" id="panel1d-header">
                                <Typography>Customers List</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                <CustomerList />
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                        <ExpansionPanel square expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                            <ExpansionPanelSummary aria-controls="panel2d-content" id="panel2d-header">
                                <Typography>Supplier List</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails>
                                    <SupplierList />
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    </Col>
                </Row>
            </Container>

        </div>
    );
}
