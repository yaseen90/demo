import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardBody
} from "shards-react";


import Skeleton from "@material-ui/lab/Skeleton";
import { connect } from "react-redux";
import PageTitle from "../components/common/PageTitle";
import { directoryGet } from "../config/api/directoryApi";
import Table from "../components/CustomerDatatables";
import { withRouter } from "react-router-dom";

class CustomerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      records: [],
      open: false,    
    };
  }
  redirect = (data) => {
    this.props.history.push("/edit-customer/" + data[0].name)
  }
  componentDidMount() {
    if (!this.props.user) {
      this.props.history.push("/");
    }
    const response = directoryGet("customer");
    response.then(res => {
      this.setState({ records: res.data.records });
    });
  }

  millisToTimeStamp(millis) {
    const months = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
    const day = new Date(millis);
    return `${day.getDate()}-${months[day.getMonth()]}-${day.getFullYear()}  -  ${day.getHours()}:${day.getMinutes()}`;
  }

  render() {
    const { records } = this.state;
    return (
      <Container fluid className="main-content-container px-4">
        <Row>
          <Col>
            <Card small className="mb-4">
              <CardBody className="p-0 pb-3">
                {
                  records.length > 0 ? 
                  <Table redirect={this.redirect} data={records} /> : 
                  <div className='p-3'>
                    <Skeleton animation="wave" height={60} />
                    <Skeleton animation="wave" height={60} />
                    <Skeleton animation="wave" height={60} />
                  </div>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }

}

const mapStateToProps = state => {
  return { user: state.authReducers.user };
};

// const mapDispatchToProps = (dispatch) => {
//   return { updateUser: (user) => dispatch(updateUser(user)) }
// }

export default withRouter(connect(mapStateToProps, null)(CustomerList));
