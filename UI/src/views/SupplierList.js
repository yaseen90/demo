// Node modules
import React from "react";
import { Container, Row, Col, Card, CardBody } from "shards-react";
import Skeleton from "@material-ui/lab/Skeleton";
import { connect } from "react-redux";

// local modules
import { directoryGet } from "../config/api/directoryApi"
import SupplierTable from '../components/SupplierDatatable';
import { withRouter } from "react-router-dom";

class SupplierList extends React.Component {
  state = {
    record: []
  }
  redirect = (data) => {
    this.props.history.push("/edit-supplier/" + data[0].name)
  }
  componentDidMount() {
   if(!this.props.user) {
     this.props.history.push("/")
     return;
   }
    const response = directoryGet("supplier");  
    response.then((res) => { this.setState({ record: res.data.records }) })
  }
  render() {
    const { record } = this.state;
    return (
      <Container fluid className="main-content-container px-4">
        <Row>
          <Col>
            <Card small className="mb-4">
              <CardBody className="p-0 pb-3">
                {
                  record.length > 0 ? 
                  <SupplierTable redirect={this.redirect} data={record} /> :
                  <div className='p-3'>
                    <Skeleton animation="wave" height={60} />
                    <Skeleton animation="wave" height={60} />
                    <Skeleton animation="wave" height={60} />
                  </div>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = (state) => { 
  return ({ user: state.authReducers.user })
}

// const mapDispatchToProps = (dispatch) => { 
//   return { updateUser: (user) => dispatch(updateUser(user)) }
// }


export default withRouter(connect(mapStateToProps, null)(SupplierList));
