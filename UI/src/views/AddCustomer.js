import React, { useEffect } from "react";
import { Container, Row, Col } from "shards-react";
import { connect } from "react-redux";
import PageTitle from "../components/common/PageTitle";
import UserAccountDetails from "../components/user-profile-lite/UserAccountDetails";


const AddCustomer = (props) => {
  useEffect(() => {
    // check if user is authorized
    if(!props.user) {
      props.history.push("/");
      return;
    }
  });
  return (<Container fluid className="main-content-container px-4">
    <Row noGutters className="page-header py-4">
      <PageTitle title="Add Customer" subtitle="Record" md="12" className="ml-sm-auto mr-sm-auto" />
    </Row>
    <Row>
      {/* <Col lg="4">
        <UserDetails />
      </Col> */}
      <Col lg="">
        <UserAccountDetails />
      </Col>
    </Row>
  </Container>
)};

const mapStateToProps = (state) => { 
  return ({ user: state.authReducers.user })
}

export default connect(mapStateToProps, null)(AddCustomer);
