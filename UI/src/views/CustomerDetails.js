import React, { Component } from "react";
import {
  Container,
  Row,
  Col
} from "shards-react";


import { connect } from "react-redux";
import PageTitle from "../components/common/PageTitle";
import Details from "../components/customerDetail";

class CustomerDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ""
    };
  }

  componentDidMount() {
    if (!this.props.user) {
      this.props.history.push("/");
    }
    if (this.props.match.params) {
      const name = this.props.match.params.name
      this.setState({
        name
      })
    }
  }

  render() {
    const { name } = this.state;
    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row noGutters className="page-header py-4">
          <PageTitle
            sm="4"
            title={`${name}` + " Record"}
            subtitle="Details"
            className="text-sm-left"
          />
        </Row>
        <Row>
          <Col>
            {name !== "" ? <Details name={name} /> : ""}
          </Col>
        </Row>
      </Container>
    );
  }

}

const mapStateToProps = state => {
  return { user: state.authReducers.user };
};

// const mapDispatchToProps = (dispatch) => {
//   return { updateUser: (user) => dispatch(updateUser(user)) }
// }

export default connect(mapStateToProps, null)(CustomerDetails);
