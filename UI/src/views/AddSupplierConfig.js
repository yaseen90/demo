import React, { useEffect } from "react"
import { Container, Row, Col } from "shards-react"
import Tab from '@material-ui/core/Tab'

import PageTitle from "../components/common/PageTitle"
import SupplierConfig from "../components/add-supplier-config/supplierConfigForm"
import SupplierConfigJSON from "../components/add-supplier-config/SupplierConfigJSON"
import { Tabs } from "@material-ui/core"


const AddSupplierConfig = (props) => {
  const [state, setState] = React.useState({
    checkedB: true,
    config: "",
    suppData: null, 
  })

  useEffect(() => {
    if(props.editData.match.params.name) { 
      setState({ checkedB: false, isEdit: props.editData.match.params.name }) 
    }
  }, [props])

  function supplierConfigFormData(data) {
    let {checkedB, ...suppData} = data
    setState({ ...state, suppData, checkedB, isEdit: props.editData.match.params.name }, function() {
      return state
    })
  }

  function tabHandlers(checkedB) {
    setState({ checkedB })
  }


  return (
    <Container fluid className="main-content-container px-4 pb-4">
     
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle sm="4" title="Add Supplier Config" subtitle="Record" className="text-sm-left" />
      </Row>

      <Row>
        <div style={{marginLeft: '20px'}}>
          <Tabs
            value={state.checkedB == 0 ? 1 : 0}
            indicatorColor="primary"
            textColor="primary"
          >
            <Tab disabled={state.isEdit && state.isEdit.length} label="Form" onClick={() => tabHandlers(1)} />
            <Tab label="JSON" onClick={() => tabHandlers(0)} />
          </Tabs>
        </div>
      </Row>

      <Row>
        {/* Editor */}
        <Col lg="12" md="12">
          { 
            state.checkedB ? 
            <SupplierConfig  suppData={supplierConfigFormData} isEdit={state.isEdit} /> : 
            <SupplierConfigJSON suppData={state.suppData} isEdit={state.isEdit} /> 
          }
        </Col>

      </Row>
    </Container>)
}

export default AddSupplierConfig
