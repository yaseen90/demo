import React, { useEffect } from "react";
import { Container, Row, Col } from "shards-react";
import { connect } from "react-redux";

import PageTitle from "../components/common/PageTitle";
import Editor from "../components/add-supplier/supplierForm";
// import SidebarActions from "../components/add-new-post/SidebarActions";
// import SidebarCategories from "../components/add-new-post/SidebarCategories";

const AddNewPost = (props) => {
  useEffect(() => {
    // check if user is authorized
    if(!props.user) {
      props.history.push("/");
      return;
    }


  });
  return(
  <Container fluid className="main-content-container px-4 pb-4">
    {/* Page Header */}
    <Row noGutters className="page-header py-4">
      {
        props.match.params.name ? 
        <PageTitle sm="4" title="Edit Supplier" subtitle="Record" className="text-sm-left" /> :
        <PageTitle sm="4" title="Add Supplier" subtitle="Record" className="text-sm-left" />
      }
    </Row>

    <Row>
      {/* Editor */}
      <Col lg="12" md="12">
        <Editor editData={props} />
      </Col>
    </Row>
  </Container>
)};

const mapStateToProps = (state) => { 
  return ({ user: state.authReducers.user })
}

export default connect(mapStateToProps, null)(AddNewPost);
