// Node Modules
import React, { Component } from "react";
import WarningIcon from "@material-ui/icons/WarningRounded";
import CheckIcon from "@material-ui/icons/CheckCircle";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Button, Modal, ModalBody, ModalHeader, Col, FormInput, ModalFooter } from "shards-react";
import JSONInput from 'react-json-editor-ajrm'
import locale from 'react-json-editor-ajrm/locale/en'
// Local modules
import { AddConfig } from "../store/action";
import { connect } from "react-redux";
import { directoryGet } from "../config/api/directoryApi";

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

/**
 * Moves an item from one list to another list.
 */
const move = (source, destination, droppableSource, droppableDestination) => {
  const sourceClone = Array.from(source);
  const destClone = Array.from(destination);
  const [removed] = sourceClone.splice(droppableSource.index, 1);

  destClone.splice(droppableDestination.index, 0, removed);

  const result = {};
  result[droppableSource.droppableId] = sourceClone;
  result[droppableDestination.droppableId] = destClone;

  return result;
};

const grid = 6;
let suppConfigArray = [];
let allItemss = [];

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: 5,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  background: isDragging ? "#007bff" : "white",
  border: "1px solid",
  borderRadius: "5px",
  color: isDragging ? "white" : "black",
  width: "400px",
  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "#f7f7f7" : "white",
  padding: grid,
  width: 400,
  height: 300,
  // justifyContent: "center",
  alignItems: "center",
  display: "flex",
  flexDirection: "column"
});

class CustomerSupplierManage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: null,
      selected: [],
      open: false,
      errorValid: {},
      jsonConfig: {}
    };
  }

  async componentDidMount() {
    try {
      const allSuppliersList = await directoryGet("supplier");
      let items = [];
      allSuppliersList.data.records.map(data => {
        // allItemss.push({ name: data.name, id: data.id, type: data.type })
        items.push({ name: data.name, id: data.id, type: data.type })
      });
      this.setState({ items });
    } catch (error) {
      console.error(error);
    }
  }
  handleChange = (e) => {
    this.setState({
      jsonConfig: e.jsObject
    })
  }
  /**
   * A semi-generic way to handle multiple lists. Matches
   * the IDs of the droppable container to the names of the
   * source arrays stored in the state.
   */
  id2List = {
    droppable: "items",
    droppable2: "selected"
  };

  getList = id => this.state[this.id2List[id]];


  /*
   * When item has been dropped into customers supp. config array
   */
  onDragEnd = result => {
    const { source, destination } = result;

    // if dropped outside the list
    if (!destination) {
      return;
    }

    if (source.droppableId === destination.droppableId) {
      const items = reorder(
        this.getList(source.droppableId),
        source.index,
        destination.index
      );

      let state = { items };

      if (source.droppableId === "droppable2") {
        state = { selected: items };
      }

      this.setState(state);
    } else {
      const result = move(
        this.getList(source.droppableId),
        this.getList(destination.droppableId),
        source,
        destination
      );

      this.setState({
        items: result.droppable,
        selected: result.droppable2
      });
    }
  };


  componentWillReceiveProps(nextProps) {
    let selectedSuppliers = [];
    suppConfigArray = nextProps.supplierConfig.supplierConfig
    setTimeout(() => {
      this.state.items.forEach(data1 => {
        suppConfigArray.forEach(data2 => {
          if (data1.name == data2.name) { selectedSuppliers.push(data1) }
        })
      });

      // dispatching data to redux (addconfig action)
      this.props.AddConfig(suppConfigArray);
      this.setState({ selected: selectedSuppliers });
    }, 2000);
  }


  render() {
    return (
      <div className="container">
        <div className="row">
          <DragDropContext onDragEnd={this.onDragEnd}>
            {/* column 1 - all suppliers list */}

            <div className="col-md-6">
              <h5 style={{ textAlign: "center" }}>Available</h5>
              <div className="allSupplierList" style={{ height: "300px", overflowY: "scroll" }}>
                <Droppable droppableId="droppable">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      {!this.state.items
                        ? "Loading..."
                        : this.state.items.map((item, index) => (
                          <Draggable
                            key={item.id}
                            draggableId={item.id}
                            index={index}
                          >
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style
                                )}
                              >
                                {item.name}
                              </div>
                            )}
                          </Draggable>
                        ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </div>
            </div>

            {/* column 2 - active suppliers list */}

            <div className="col-md-6">
              <h5 style={{ textAlign: "center" }}>Active</h5>
              <div className="allSupplierList" style={{ height: "300px", overflowY: "scroll" }}>
                <Droppable droppableId="droppable2">
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                    >
                      {this.state.selected &&
                        this.state.selected.map((item, index) => (
                          <Draggable
                            key={item.id}
                            draggableId={item.id}
                            index={index}
                          >
                            {(provided, snapshot) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                style={getItemStyle(
                                  snapshot.isDragging,
                                  provided.draggableProps.style
                                )}
                              >
                                {item.name}
                              </div>
                            )}
                          </Draggable>
                        ))}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </div>
            </div>
            <p className="DragText">Drag left to add</p>
          </DragDropContext>
        </div>
        {this.state.selected && (
          <div className="mt-2">
            {this.renderCustomerSupplierConfigRow(this.state.selected)}
          </div>
        )}
      </div>
    );
  }

  // deleteRow = (index) => {
  //   let selected2 = [...this.state.selected];
  //   selected2.splice(index, 1)
  //   this.setState({
  //     selected: selected2
  //   })
  // }
  /*
   *  rendering customer supplier row with config button
   */
  renderCustomerSupplierConfigRow(supplierData) {
    return (
      <ol className="mt-3">
        {supplierData.map((supplier, index) => (
          <li className="mt-2 p-3 rounded row configList" key={supplier.id}>
            <span className="col-md-2">
              {
                suppConfigArray.findIndex(data => data.name == supplier.name) == -1 ?
                  (<WarningIcon style={{ color: "yellow" }} />) :
                  (<CheckIcon style={{ color: "green" }} />)
              }
            </span>
            <span className="col-md-2">
              <span style={{ fontWeight: "bold" }}>{supplier.name}</span>
            </span>

            <span className="col-md-2">
              <p style={{ display: 'none' }}>{supplier.id}</p>      {/* <-------- dont remove this line */}
              <Button onClick={this.toggle.bind(this, supplier.id)}>
                Configure
              </Button>
            </span>

            {/* Status Button for supplier config on customers */}
            {/* <span className="col-md-2">
              <Button theme="danger" onClick={() => {
                this.deleteRow(index)
              }} >Remove</Button>
            </span> */}

            {/* Status Icon for supplier config on customers */}


            <Modal centered open={this.state.open[supplier.id]}>
              {/* {(JSON.stringify(this.state.open[supplier.id]), supplier.id)} */}
              <ModalHeader>Configuration of {supplier.name}</ModalHeader>
              <ModalBody>
                <JSONInput
                  width='100%'
                  id='uuid'
                  onChange={this.handleChange}
                  theme="light_mitsuketa_tribute"
                  locale={locale}
                  height='300px'
                />
              </ModalBody>
              <ModalFooter>
                {/* {supplier.name} */}
                <Button onClick={this.onSuppConfigAdd.bind(this, supplier)}>
                  Add
                </Button>
                <Button onClick={this.toggle.bind(this, supplier.id)}>
                  Cancel
                </Button>
              </ModalFooter>
            </Modal>
          </li>
        ))}
      </ol>
    );
  }

  onSuppConfigAdd(supplier) {
    const { jsonConfig, open } = this.state;

    if (!suppConfigArray.length) {
      suppConfigArray.push(jsonConfig)
    }
    else {
      if (jsonConfig) {
        const index = suppConfigArray.findIndex(data => data.name == jsonConfig.name);
        if (index === -1) { suppConfigArray.push(jsonConfig); }
        else { suppConfigArray[index] = jsonConfig; }
      }
    }

    // dispatching data to redux (addconfig action)

    this.props.AddConfig(suppConfigArray);
    this.toggle(open[supplier.id]);
  }

  toggle(id) {
    if (Object.keys(this.state.errorValid).length) { this.setState({ errorValid: {} }) }
    this.setState({
      open: { [id]: !this.state.open[id] }
    });
  }
}

// const mapStateToProps = state => {
//   return { suppConfig: state.authReducers.suppConfig };
// };

const mapDispatchToProps = dispatch => {
  return { AddConfig: addSuppConfig => dispatch(AddConfig(addSuppConfig)) };
};

export default connect(null, mapDispatchToProps)(CustomerSupplierManage);
