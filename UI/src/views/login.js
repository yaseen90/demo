import React, { Component } from "react";
import { Form, FormGroup, FormInput, Button } from "shards-react";
import Swal from "sweetalert2";
import CircularProgress from '@material-ui/core/CircularProgress';

import { connect } from "react-redux";
import { updateUser } from "../store/action";

import { directoryGet } from "../config/api/directoryApi";
import "../assets/Login.css";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: ""
    };
  }
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      error: ""
    });
  };
  submit = () => {
    this.setState({ loader: true });
    const response = directoryGet("admin");
    response.then(res => {
      this.setState({ adminData: res.data.records[0], loader: false }, () => {
        const { email, password, adminData } = this.state;
        if (email === adminData.email && password === adminData.password) {
          this.props.updateUser(adminData);
          this.props.history.push("/list");
        } else {
          this.setState({ errorMsg: 'Please Provide Correct email Password!' })
        }
      });
    });
  };
  componentDidMount() {
    if (this.props.user) {
      this.props.history.push("/list");
      return;
    }
  }
  render() {
    const { email, password, alert } = this.state;
    return (
      <div className="LoginContainer">
        <div className="loginForm">
        
          <div className="dha-div">
            <p className="headingSignIn">Admin Login</p>
          </div>
          <form>
            <Form>
              <FormGroup>
                <label htmlFor="email">
                  Email <span style={{ color: "red" }}>*</span>
                </label>
                <FormInput
                  id="email"
                  value={email}
                  name="email"
                  type="email"
                  placeholder="example@mail.com"
                  className="mb-2"
                  size="lg"
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <label htmlFor="password">
                  Password <span style={{ color: "red" }}>*</span>
                </label>
                <FormInput
                  id="password"
                  type="password"
                  value={password}
                  name="password"
                  className="mb-2"
                  placeholder="*********"
                  size="lg"
                  onChange={this.handleChange}
                />
              </FormGroup>
            </Form>
            <div className='row'>
              <div className="col-md-8">
              <p style={{color: 'red'}}>{this.state.errorMsg && this.state.errorMsg}</p>
              </div>
              <div className="col-md-4">
                <p className="fg-password">Forgot Password?</p>
              </div>
            </div>
            
            
            {this.state.loader ? (
              <Button
                disabled={true}
                block
                outline
                style={{ fontSize: "18px" }}
                onClick={this.submit}
              >
                <CircularProgress size={20} />
              </Button>
            ) : (
              <Button
                block
                outline
                style={{ fontSize: "18px" }}
                onClick={this.submit}
              >
                Login
              </Button>
            )}
          </form>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return { user: state.authReducers.user };
};

const mapDispatchToProps = dispatch => {
  return { updateUser: user => dispatch(updateUser(user)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
