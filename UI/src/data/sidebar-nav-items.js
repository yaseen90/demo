export default function() {
  return [
    // {
    //   title: "Dashboard",
    //   to: "/dashboard",
    //   htmlBefore: '<i class="material-icons">edit</i>',
    //   htmlAfter: ""
    // },
    {
      title: "List",
      htmlBefore: '<i class="material-icons">person</i>',
      to: "/list",
    },
    {
      title: "Add Customer",
      htmlBefore: '<i class="material-icons">person</i>',
      to: "/add-customer",
    },
    {
      title: "Add Supplier",
      htmlBefore: '<i class="material-icons">person</i>',
      to: "/add-supplier",
    }
    // {
    //   title: "Forms & Components",
    //   htmlBefore: '<i class="material-icons">view_module</i>',
    //   to: "/components-overview",
    // },
    // {
    //   title: "Tables",
    //   htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/tables",
    // },
    // {
    //   title: "User Profile",
    //   htmlBefore: '<i class="material-icons">person</i>',
    //   to: "/user-profile-lite",
    // },
    // {
    //   title: "Errors",
    //   htmlBefore: '<i class="material-icons">error</i>',
    //   to: "/errors",
    // }
  ];
}
