import React from "react"
import { directoryGetOne } from "../../config/api/directoryApi"
import PageTitle from "../../components/common/PageTitle";
import {
	Container,
	Row,
	Col,
	Card,
	CardBody,
	CardFooter,
	Badge,
	Button
} from "shards-react";
class ListSupplier extends React.Component {
	state = {
		supplier: {},
		supplierConfig: {}
	}
	componentDidMount() {
		const name = this.props.match.params.name;
		try {
			const response = directoryGetOne({ type: "supplier", name });
			response.then((res) => { this.setState({ supplier: res.data.record }) })
			const res = directoryGetOne({ type: "supplierConfig", name });
			res.then((response) => { this.setState({ supplierConfig: response.data.record }) })
			
		} catch (error) {

		}
	}
	render() {
		const { supplier, supplierConfig } = this.state;
		delete supplierConfig.id;
		delete supplierConfig.uuid;
		delete supplierConfig.type;
		delete supplierConfig.name;
		delete supplierConfig.lastUpdated;
		delete supplierConfig.createdAt;
		return (
			<Container fluid className="main-content-container px-4">
				<Row noGutters className="page-header py-4">
					<PageTitle title={`Supplier ${supplier.name}`} subtitle="Info" md="12" className="ml-sm-auto mr-sm-auto" />
				</Row>

				<Row>
					<Col lg="6">
						<Card small className="card-post mb-4">
							<CardBody>
								<h5 className="card-title">Supplier</h5>
								{
									Object.keys(supplier).map((val, index) => {
										return <p key={index} className="card-text text-muted">{val + " : " + supplier[val]}</p>
									})
								}
							</CardBody>
						</Card>
					</Col>
					<Col lg="6">
						<Card small className="card-post mb-4">
							<CardBody>
								<h5 className="card-title">Supplier Config</h5>
								{
									Object.keys(supplierConfig).map((val, index) => {
										return <p key={index} className="card-text text-muted">{val + " : " + supplierConfig[val]}</p>
									})
								}
							</CardBody>
						</Card>
					</Col>
				</Row>
			</Container>
		)
	}
}
export default ListSupplier;