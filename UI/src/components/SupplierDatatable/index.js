// Node Modules
import React, { Component } from 'react'
import { Button, Modal, ModalBody, ModalHeader, ModalFooter, Container, Row, Col, Card, CardHeader, CardBody } from "shards-react";
import { withRouter } from 'react-router-dom';
// local modules
import "./index.css"
import { directoryGetOne, directoryGet } from '../../config/api/directoryApi';
const $ = require('jquery');
$.DataTable = require('datatables.net');

class SupplierTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    }
  }

  millisToTimeStamp(millis) {
    const months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const day = new Date(millis);
    return `${months[day.getMonth()]}-${day.getDate()}-${day.getFullYear()}`;
    // return `${day.getFullYear()}-${months[day.getMonth()]}-${day.getDate()} -  ${day.getHours()}:${day.getMinutes()}`;}
  }
  async componentDidMount() {
    let showData = []
    let that = this

    const suppConfig = await directoryGet("supplierConfig")
    this.setState({ supplierConfigData: suppConfig.data.records }, function () {
      this.props.data.map(data => {
        showData.push({
          actions: 'actions',
          ID: data.id,
          Name: data.name,
          AccessType: data.accessType,
          LastUpdated: this.millisToTimeStamp(data.lastUpdated),
          UUID: data.uuid,
          Status: data.status && data.status == 'true' || data.status == true ? 'Active' : 'Inactive'
        })
      })

      /* 
      ** Datatable configurations
      */
      this.$el = $(this.el)
      this.$el.DataTable({
        data: showData,
        columns: [
          { title: 'ID', data: 'ID' },
          { title: 'Name', data: 'Name' },
          { title: 'Access Type', data: 'AccessType' },
          { title: 'Last Updated', data: 'LastUpdated' },
          { title: 'UUID', data: 'UUID' },
          { title: 'Status', data: 'Status' },
          {
            title: 'Actions', data: 'actions', render: function (data, type, row) {
              return `<button type="button" value=${row.ID} class="btn btn-primary no-modal"><i style="pointer-events:none" aria-hidden="true" class="fa fa-pencil-square-o" aria-hidden="true"></i></button>`;
            }
          },
        ],
        columnDefs: [{ "targets": [0, 2, 4, 5, 6], "searchable": false }],
        language: { searchPlaceholder: "Name" },
      })

      /* 
      ** For searching by name
      */
      $('input[type = text]').on('keyup', function () {
        if (this.value.length == 0) {
          $('#exampleSupplier').DataTable().column(1).search("", 0).draw();
        } else {
          $('#exampleSupplier').DataTable().column(1).search('^' + this.value, true, false).draw();
        }
      });


      /* 
      ** For searching by date
      */
      $('#datepickerSupplier').on('change', function () {
        console.log(this.value)
        if (this.value.length == 0) {
          $('#exampleSupplier').DataTable().column(3).search("", 0).draw();
        } else {
          let realDate = that.millisToTimeStamp(this.value).substring(0, 11)
          $('#exampleSupplier').DataTable().columns(3).search('^' + realDate, true, false).draw();
        }
      });

      /* 
      ** opening modal on row click 
      */
      $('#exampleSupplier tbody').on('click', 'tr', function (evt) {
        if ($(evt.target).is("button")) {
          let editData = that.props.data.filter(data => data.id == evt.target.value);
          that.props.redirect(editData)
          return;
        }
        var data = $('#exampleSupplier').DataTable().row(this).data();
        that.props.history.push(`/supplier-list-data/${data.Name}`)
      });


    })


  }

  componentWillMount() {
    $('#exampleSupplier').DataTable().destroy(true);
  }

  render() {
    const { showSingleData, open } = this.state;
    return (
      <div className="p-4">
        <input type="date" id="datepickerSupplier" className="col-md-3 form-control float-right ml-2" />
        <input placeholder="Search By Name" type="text" id="name" className="col-md-3 form-control float-right ml-2" />
        <table id="exampleSupplier" width="100%" className="table table-responsive mb-0 table-hover" style={{ cursor: "pointer" }} ref={el => this.el = el}>
        </table>
      </div>
    )
  }

  /* 
 ** function for rendering customers supplier config data with formatting
 */
  renderSuppliersData(dataaa) {
    let data = this.props.data.find(data => dataaa.ID == data.id)
    // debugger
    let suppConfigData = this.state.supplierConfigData.find(data => data.name == dataaa.Name)
    console.log(data, suppConfigData)
    return (
      <React.Fragment>
        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>ID</span>: &nbsp;
          <span>{data['id'] ? data['id'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Name</span>: &nbsp;
          <span>{data['name'] ? data['name'] : 'Not Available'}</span>
        </div>

        {/***** sftp section **** */}

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>SFTP Port</span>: &nbsp;
          <span>{data['sftpPort'] ? data['sftpPort'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>SFTP Hostname</span>: &nbsp;
          <span>{data['sftpHostname'] ? data['sftpHostname'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>SFTP Prefix</span>: &nbsp;
          <span>{data['sftpPrefix'] ? data['sftpPrefix'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>SFTP Product File</span>: &nbsp;
          <span>{data['sftpProductFile'] ? data['sftpProductFile'] : 'Not Available'}</span>
        </div>

        {/***** ftp section **** */}

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>FTP Hostname</span>: &nbsp;
          <span>{data['ftpHostname'] ? data['ftpHostname'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>FTP Prefix</span>: &nbsp;
          <span>{data['ftpPrefix'] ? data['ftpPrefix'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>FTP Product File</span>: &nbsp;
          <span>{data['ftpProductFile'] ? data['ftpProductFile'] : 'Not Available'}</span>
        </div>


        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Type</span>: &nbsp;
          <span>{data['type'] ? data['type'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>UUID</span>: &nbsp;
          <span>{data['uuid'] ? data['uuid'] : 'Not Available'}</span>
        </div>


        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Created At</span>: &nbsp;
          <span>{this.millisToTimeStamp(data['createdAt'])}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Last Updated At</span>: &nbsp;
          <span>{this.millisToTimeStamp(data['lastUpdated'])}</span>
        </div>

        <hr />
        <h4>Supplier Configurations</h4>
        <hr />

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Supplier Config Id</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['id'] ? suppConfigData['id'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Type</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['type'] ? suppConfigData['type'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Product ID Query String</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['productIdQueryString'] ? suppConfigData['productIdQueryString'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Product Load Query String</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['productLoadQueryString'] ? suppConfigData['productLoadQueryString'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Product Identifier</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['productIdentifier'] ? suppConfigData['productIdentifier'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Text Field</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['textField'] ? JSON.stringify(suppConfigData['textField']) : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Filter</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['filter'] ? JSON.stringify(suppConfigData['filter']) : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Mandatory</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['mandatory'] ? suppConfigData['mandatory'].join(', ') : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Transform</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['transform'] ? suppConfigData['transform'].join(', ') : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>UUID</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['uuid'] ? suppConfigData['uuid'] : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Last Updated</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['lastUpdated'] ? this.millisToTimeStamp(data['lastUpdated']) : 'Not Available'}</span>
        </div>

        <div className="card-text text-muted">
          <span style={{ fontWeight: 'bold' }}>Created At</span>: &nbsp;
          <span>{suppConfigData && suppConfigData['createdAt'] ? this.millisToTimeStamp(data['createdAt']) : 'Not Available'}</span>
        </div>

      </React.Fragment>

    )
  }

  /* 
  ** For openinng modal and closing
  */
} 

export default withRouter(SupplierTable)