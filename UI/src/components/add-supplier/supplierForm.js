// Node Modules
import React from "react";
import Swal from "sweetalert2";
import { Card, CardBody, Form, FormInput, FormGroup, FormSelect, Button, Row, Col } from "shards-react";
import "react-quill/dist/quill.snow.css";
import "../../assets/quill.css";

// Local modules
import SupplierConfig from "../../views/AddSupplierConfig";
import { directory, directoryGetOne, directoryEditOne } from "../../config/api/directoryApi";


class Editor extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      accessType: "sftp",
      type: "supplier",
      sftpHostname: "",
      sftpProductFile: "",
      sftpPrefix: "",
      sftpPort: "",
      ftpHostname: "",
      ftpProductFile: "",
      ftpPrefix: "",
      ftpPort: "",
      errorValid: {
        name: false,
  
        sftpHostname: false,
        sftpProductFile: false,
        sftpPrefix: false,
        sftpPort: false,
  
        ftpHostname: false,
        ftpProductFile: false,
        ftpPrefix: false,
        ftpPort: false
      }
    };
  
  }

  async componentDidMount() {
    if(!this.props.editData.match.params.name) { 
      // this.props.editData.history.push('/supplier-list'); 
      return; 
    } 
    else {
      this.setState({ editMode: true })
      let name = this.props.editData.match.params.name
      try {
        const suppResp = await directoryGetOne({ type: 'supplier', name })
        this.setState({ isEdit: suppResp.data.record })
        this.editData(this.state.isEdit, this.state.isEdit.accessType)
      } catch (error) {
        console.error(error);
        this.props.editData.history.push('/supplier-list'); 
      } 
      // const suppConfigResp = await directoryGetOne({ type: 'supplierConfig', name })
    }  
  }
  
  
  validateName = ($name) => {
    var nameReg = /^\S*$/;
    if (!nameReg.test($name)) {  return false; } 
    else {  return true; }
  };

  handleChange = e => {
    if (!this.validateName(e.target.value)) { return false } 
    this.setState({ [e.target.name]: e.target.value });
  };


  PostSupplierData = async (data) => {
    let params = { type: 'supplier', name: this.state.name, body: JSON.stringify(data)  }
    try {
      const response = (this.state.isEdit) ? await directoryEditOne(params) : await directory(JSON.stringify(data))
      if (response.data.response.success) {
        this.setState({ isLoading: false })
        Swal.fire("Successfull", "Supplier Record Updated", "success")
        this.resetState()
        this.props.editData.history.push('/supplier-list'); 
      }
    } catch (error) {    
      Swal.fire("Failed", "There's something wrong", "error")
      this.resetState()
    }
  }

  handleValidation = async () => {
    this.setState({ isLoading: true })
    if(Object.values(this.state.errorValid).includes(true)) {     
      this.setState({errorValid: {
        name: false,
        sftpHostname: false,
        sftpProductFile: false,
        sftpPrefix: false,
        sftpPort: false,
        ftpHostname: false,
        ftpProductFile: false,
        ftpPrefix: false,
        ftpPort: false
      }})
    }
    let { accessType } = this.state;
    let errors = {};
    let data = {
      name: this.state.name,
      accessType: this.state.accessType,
      type: this.state.type,
      [accessType + "Hostname"]: this.state[accessType + "Hostname"],
      [accessType + "ProductFile"]: this.state[accessType + "ProductFile"],
      [accessType + "Prefix"]: this.state[accessType + "Prefix"],
      [accessType + "Port"]: this.state[accessType + "Port"]
    };
    for (let value of Object.entries(data)) {
      if (value[1] == null || value[1] == "") {
        errors[value[0]] = true;
        this.setState({ errorValid: errors });
      }
    }
    setTimeout(() => {
      if(Object.values(this.state.errorValid).includes(true)) {     
        return;
      } else {
        this.PostSupplierData(data)
      }
    }, 2000);
    
  };

  render() {
    const { name, accessType, sftpObj, ftpObj, errorValid } = this.state;
    return (
      <div>
        {/* {JSON.stringify(this.state.errorValid)} */}
        <Card small className="mb-3">
          <CardBody>
            <Form>
              <Row>
                <Col md="6">
                  <FormGroup>
                    <label htmlFor="name">
                      Name <span style={{ color: "red" }}>*</span>
                    </label>
                    <FormInput
                      value={this.state.name}
                      invalid={errorValid.name}
                      id="name"
                      name="name"
                      value={name}
                      type="text"
                      placeholder="enter supplier name"
                      className="mb-2"
                      onChange={this.handleChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="6">
                  <FormGroup>
                    <label htmlFor="accessType">
                      Access Type <span style={{ color: "red" }}>*</span>
                    </label>
                    <FormSelect name="accessType" onChange={(e) => { 
                          this.handleChange(e)
                          this.editData(this.state.isEdit, e.target.value)
                        }
                      } 
                    >
                      {/* <option value="">Please Select Server Type</option> */}
                      <option value="sftp">sftp</option>
                      <option value="ftp">ftp</option>
                    </FormSelect>
                  </FormGroup>
                </Col>
              </Row>

              <div>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="name">
                        {accessType} Host Name{" "}
                        <span style={{ color: "red" }}>*</span>
                      </label>
                      <FormInput
                        value={this.state[accessType + "Hostname"]}
                        invalid={this.state.errorValid[accessType + "Hostname"]}
                        id={accessType + "Hostname"}
                        name={accessType + "Hostname"}
                        type="text"
                        placeholder="enter sftp Host Name"
                        className="mb-2"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="name">
                        {accessType} Product File{" "}
                        <span style={{ color: "red" }}>*</span>
                      </label>
                      <FormInput
                        value={this.state[accessType + "ProductFile"]}
                        invalid={
                          this.state.errorValid[accessType + "ProductFile"]
                        }
                        id={accessType + "ProductFile"}
                        name={accessType + "ProductFile"}
                        type="text"
                        placeholder="enter sftp Product File"
                        className="mb-2"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="name">
                        {accessType} Prefix{" "}
                        <span style={{ color: "red" }}>*</span>
                      </label>
                      <FormInput
                        value={this.state[accessType + "Prefix"]}
                        invalid={this.state.errorValid[accessType + "Prefix"]}
                        id={accessType + "Prefix"}
                        name={accessType + "Prefix"}
                        type="text"
                        placeholder="enter sftp Prefix"
                        className="mb-2"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                  <Col md="6">
                    <FormGroup>
                      <label htmlFor="name">
                        {accessType} Port{" "}
                        <span style={{ color: "red" }}>*</span>
                      </label>
                      <FormInput
                        value={this.state[accessType + "Port"]}
                        invalid={this.state.errorValid[accessType + "Port"]}
                        id={accessType + "Port"}
                        name={accessType + "Port"}
                        type="number"
                        placeholder="enter sftp Port"
                        className="mb-2"
                        onChange={this.handleChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </div>
            </Form>
            {
              this.state.editMode ? 
              <Button disabled={this.state.isLoading} onClick={this.handleValidation}>Update Record</Button> : 
              <Button disabled={this.state.isLoading} onClick={this.handleValidation}>Add Record</Button>
            }
            
          </CardBody>
        </Card>
        <SupplierConfig editData={this.props.editData} />
      </div>
    );
  }


  resetState() {
    this.setState({
      name: "",
      accessType: "sftp",
      type: "supplier",
      sftpHostname: "",
      sftpProductFile: "",
      sftpPrefix: "",
      sftpPort: "",
      ftpHostname: "",
      ftpProductFile: "",
      ftpPrefix: "",
      ftpPort: "",
      errorValid: {
        name: false,
  
        sftpHostname: false,
        sftpProductFile: false,
        sftpPrefix: false,
        sftpPort: false,
  
        ftpHostname: false,
        ftpProductFile: false,
        ftpPrefix: false,
        ftpPort: false
      }
    })
  }

  editData(supplier, accessType) {
    if(supplier && accessType) {
      this.setState({
        name: supplier.name,
        accessType: accessType,
        [accessType + "Hostname"]: supplier[accessType + "Hostname"],
        [accessType + "ProductFile"]: supplier[accessType + "ProductFile"],
        [accessType + "Prefix"]: supplier[accessType + "Prefix"],
        [accessType + "Port"]: supplier[accessType + "Port"],
      })
    } 
  }
}

export default Editor;