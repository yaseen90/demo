import React, { Component } from 'react'
import "./index.css"
import { withRouter } from 'react-router-dom';

const $ = require('jquery');
$.DataTable = require('datatables.net');


 class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    }  
  }

  millisToTimeStamp(millis) {
    const months = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sep","Oct","Nov","Dec"];
    const day = new Date(millis);
    return `${months[day.getMonth()]}-${day.getDate()}-${day.getFullYear()}`;
  }
  componentDidMount() {
    let showData = []
    let that = this
    this.props.data.map(data => {
      showData.push({
        actions: 'actions',
        ID: data.id,
        Name: data.name,
        CreatedAt: this.millisToTimeStamp(data.createdAt),
        LastUpdated: this.millisToTimeStamp(data.lastUpdated),
        UUID: data.uuid,   
        Status: data.status && data.status == 'true' || data.status == true ? 'Active' : 'Inactive'
      })
    })

     /* 
    ** Datatable configurations
    */
    this.$el = $(this.el)
    this.$el.DataTable({
      data: showData,
      columns: [
        { title: 'ID', data: 'ID' },
        { title: 'Name', data: 'Name' },
        { title: 'Created At', data: 'CreatedAt' },
        { title: 'Last Updated', data: 'LastUpdated' },
        { title: 'UUID', data: 'UUID' },
        { title: 'Status', data: 'Status' },
        { title: 'Actions', data: 'actions', render: function (data, type, row) {
            return `<button type="button" value=${row.ID} class="btn btn-primary no-modal"><i style="pointer-events:none" class="fa fa-pencil-square-o" aria-hidden="true"></i></button>`;
          } 
        },
      ],
      columnDefs: [ { "targets": [0,4,5,6], "searchable": false } ],
      language: { searchPlaceholder: "Name" },
    })

    /* 
    ** For searching by name
    */
    $('input[type = text]').on( 'keyup', function () {
      if (this.value.length == 0) {
        $('#example').DataTable().column(1).search("", 0).draw();
      } else {
        $('#example').DataTable().column(1).search('^'+this.value, true, false).draw();
      }     
    }); 

    /* 
    ** For searching by date
    */
   $('#datepicker').on( 'change', function () {
    if (this.value.length == 0) {
      $('#example').DataTable().column(2).search("", 0).draw();
    } else {      
      let realDate = that.millisToTimeStamp(this.value).substring(0, 11)
      $('#example').DataTable().columns(2).search('^'+realDate, true, false).draw();
    }
  });

    /* 
    ** opening modal on row click 
    */
    $('#example tbody').on( 'click', 'tr', function (evt) {
      if ( $(evt.target).is("button") ) {
        let editData = that.props.data.filter(data => data.id == evt.target.value);
        that.props.redirect(editData)
        return;
      }
      var data = $('#example').DataTable().row(this).data();
      that.props.history.push(`/customer-details/${data.Name}`)
    });
  }

  componentWillMount() {
    $('#example').DataTable().destroy(true);
  }
    
  render() {
    return (
      <div className="p-4">
        <input type="date" id="datepicker" className="col-md-3 form-control float-right ml-2" />
        <input placeholder="Search By Name" type="text" id="name" className="col-md-3 form-control float-right ml-2" />   
        <table id="example" width="100%" className="table table-responsive mb-0 table-hover" style={{ cursor: "pointer" }} ref={el => this.el = el}>
        </table>
      </div>
    )
  }
} 
export default withRouter(Table)