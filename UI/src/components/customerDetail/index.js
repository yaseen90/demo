import React from "react"
import { directoryGetOne, directoryEditOne } from "../../config/api/directoryApi"
import { retrieveAndImport, exportFeed } from "../../config/api/supplierlink"
import {
    Row,
    Col,
    Button
} from "shards-react";
import "../../assets/customerDetail.css"
import Paper from '@material-ui/core/Paper';
import Jobstore from "../jobstore"
import EdiText from 'react-editext'
import Keygen from "../../utils/randomKey"
class Details extends React.Component {
    state = {
        record: {},
        name: "",
        editing: false,
        key: false,
        toggle: false
    }
    async componentDidMount() {
        const getRecord = await directoryGetOne({ type: "customer", name: this.props.name })
        this.setState({
            record: getRecord.data.record,
            name: this.props.name
        })
    }
    millisToTimeStamp(millis) {
        const months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];
        const day = new Date(millis);
        return `${day.getDate()}-${months[day.getMonth()]}-${day.getFullYear()}  :  ${day.getHours()}:${day.getMinutes()}`;
    }
    handleSaveEmail = async (value) => {
        let record = this.state.record;
        record.email = value;
        const response = await directoryEditOne({ type: "customer", name: record.name, body: record })
        if (response.status === 200) {
            this.setState({
                record
            })
        }
    }
    handleSavePassword = async (value) => {
        this.setState({
            editing: false
        })
        let record = this.state.record;
        record.password = value;
        const response = await directoryEditOne({ type: "customer", name: record.name, body: record })
        if (response.status === 200) {
            this.setState({
                record
            })
        }
    }
    updateKey = async () => {
        let record = this.state.record;
        record.Api_Key = Keygen(20);
        const response = await directoryEditOne({ type: "customer", name: record.name, body: record })
        if (response.status === 200) {
            this.setState({
                record
            })
        }
    }
    retrieve = async () => {
        const { record } = this.state;
        const response = await retrieveAndImport({ type: "retrieve", customerKey: record.Api_Key, name: record.name });
        if (response.data) {
            this.setState({
                toggle: true,
                jobData: response.data
            })
        }
    }
    export = async () => {
        const { record } = this.state;
        const response = await exportFeed({ customerKey: record.Api_Key, name: record.name });
        if (response.data) {
            this.setState({
                toggle: true,
                jobData: response.data
            })
        }
    }
    import = async () => {
        const { record } = this.state;
        const response = await retrieveAndImport({ type: "import", customerKey: record.Api_Key, name: record.name });
        if (response.data) {
            this.setState({
                toggle: true,
                jobData: response.data
            })
        }
    }
    closed = () => {
        this.setState({
            toggle: false
        })
    }
    render() {
        let { toggle, record, editing, key, jobData } = this.state;
        console.log(record.schedule);
        return (
            <div>
                <Paper elevation={3} className="BoxPanel">
                    <div className="inputDivCol">
                        <Row>
                            <Col md="4">
                                <div>
                                    <p>Customer Name : {record.name}</p>
                                    <p className="p_bottom">Last Updated : {this.millisToTimeStamp(record.lastUpdated)}</p>
                                </div>
                            </Col>
                            <Col md="6">
                                <div className="Btn-div">
                                    <Button active theme="success" disabled={true} >{record.status ? "Active" : "Deactive"}</Button>
                                    <Button active theme="success" disabled={true} >Schedule OK</Button>
                                    <Button active theme="success" disabled={true} >Retrieve OK</Button>
                                    <Button active theme="success" disabled={true} >Export OK</Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Paper>
                <Paper elevation={3} className="BoxPanel">
                    <div className="inputDivCol">
                        <Row>
                            <Col md="8">
                                <div>
                                    <div className="divInput">
                                        <p className="p_bottom" style={{ marginTop: "5px" }}>Email</p>
                                        <EdiText
                                            type="email"
                                            value={record.email}
                                            onSave={this.handleSaveEmail}
                                            editButtonClassName="editTextBtn"
                                            editButtonContent="Update"
                                        />
                                    </div>
                                    <br />
                                    <div className="divInput">
                                        <p className="p_bottom" style={{ marginTop: "5px" }}>Password</p>
                                        <span style={{ display: "flex" }}>
                                            <EdiText
                                                type="password"
                                                value={editing ? record.password : "*********"}
                                                onSave={this.handleSavePassword}
                                                editButtonClassName="editDisplayNone"
                                                editButtonContent="Reset"
                                                editing={editing}
                                                onCancel={() => this.setState({ editing: false })}
                                            />
                                            <button onClick={() => this.setState({ editing: true })} className={editing ? "editDisplayNone" : "editTextBtn"}>Reset</button>
                                        </span>
                                    </div>
                                    <div className="divInput" style={{ marginTop: 20 }}>
                                        <p className="p_bottom">Api Key</p>
                                        <span style={{ display: "flex" }}>
                                            <p className="p_bottom">{key ? record.Api_Key : "***********"}</p>
                                            <button onClick={this.updateKey} className="editTextBtn">Generate</button>
                                            <button className="editTextBtn" onClick={() => this.setState({ key: !key })}>{key ? "Hide" : "Show"}</button>
                                        </span>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Paper>
                <Paper elevation={3} className="BoxPanel">
                    <div className="inputDivCol">
                        <Row>
                            <Col md="4">
                                <div>
                                    <p className="p_bottom" style={{ marginTop: "7px" }}>API Actions</p>
                                </div>
                            </Col>
                            <Col md="6">
                                <div className="Btn-div" style={{ marginTop: "0px" }}>
                                    <Button onClick={this.retrieve} theme="primary" >Retrieve</Button>
                                    <Button onClick={this.import} theme="primary" >Import</Button>
                                    <Button onClick={this.export} theme="primary" >Export</Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Paper>
                {toggle ? <Jobstore collapse={toggle} closed={this.closed} jobData={jobData} /> : null}
                <Paper elevation={3} className="BoxPanel" style={{ marginBottom: "100px" }}>
                    <div className="inputDivCol">
                        <Row>
                            <Col md="12">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th scope="row">Schedule Information</th>
                                            <th scope="row"></th>
                                            <th scope="row"></th>
                                            <th scope="row"></th>
                                            <th scope="row"></th>
                                            <th scope="row"><Button>Update</Button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{record.schedule ? record.schedule.enable ? "Enabled" : "Disabled" : ""}</td>
                                            <td>Interval : {record.schedule ? record.schedule.interval : ""}</td>
                                            <td></td>
                                            <td>Last Iteration : {record.schedule ? record.schedule.lastIteration + "  :  " + record.schedule.lastIterationStatus  : ""}</td>
                                            <td></td>
                                            <td>Next Iteration : {record.schedule ? record.schedule.nextIteration : ""}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>
                </Paper>
            </div>
        )
    }
}
export default Details;