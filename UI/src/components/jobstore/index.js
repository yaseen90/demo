import React from "react"
import { Collapse, Button } from "shards-react"
import { jobStatusGet } from "../../config/api/jobstore"
import Refresh from "@material-ui/icons/Refresh";
import Cancel from "@material-ui/icons/Cancel";

class Jobstore extends React.Component {
    state = {
        job: {}
    }
    jobUpdate = async () => {
        const data = this.props.jobData;
        const response = await jobStatusGet({ url: data.JobStatusURL });
        if (response.data) {
            this.setState({
                job: response.data.job
            })
        }
    }
    componentDidMount() {
        this.jobUpdate()
    }
    render() {
        const { job } = this.state;
        return (
            <Collapse open={this.props.collapse}>
                <div className="p-3 mt-3 border rounded" style={{ display: "flex", flexDirection: "column" }}>
                    <table className="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Last Updated</th>
                                <th scope="col">Active</th>
                                <th style={{ width: "110px" }} scope="col">Status Code</th>
                                <th scope="col">Status</th>
                                <th scope="col">Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{job.lastUpdated_String}</td>
                                <td>{job.active}</td>
                                <td>{job.statusCode}</td>
                                <td>{job.status}</td>
                                <td>{job.message}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div style={{ display: "flex", width: "150px", justifyContent: "space-between", alignSelf: "flex-end" }}>
                        <Button theme="info" onClick={() => this.jobUpdate()}><Refresh /></Button>
                        <Button theme="danger" onClick={() => this.props.closed()}><Cancel /></Button>
                    </div>
                </div>
            </Collapse>
        )
    }
}
export default Jobstore