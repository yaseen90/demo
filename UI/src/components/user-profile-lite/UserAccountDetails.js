import React from "react";
import PropTypes from "prop-types";
import {
  Row,
  Col,
  FormInput,
  Button
} from "shards-react";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Paper from '@material-ui/core/Paper';
import { directory, directoryGetOne, directoryEditOne } from "../../config/api/directoryApi";
import { connect } from "react-redux";
import CustomerSupplierManage from "../../views/CustomerSupplierManage";
import { withRouter } from "react-router-dom";
import "../../assets/addCustomer.css"
import Switch from '@material-ui/core/Switch'
import Keygen from "../../utils/randomKey"

class UserAccountDetails extends React.Component {
  state = {
    keyToggle: false,
    passwordToggle: false,
    supplierArray: [],
    collapse: false,
    enabled: false,
    errorData: null,
    errorValid: {
      name: false,
      email: false,
      password: false,
      Api_Key: false,
      activeSuppliers: false,
      interval: false
    },
    suppConfigSelectError: false
  };

  validateName = ($name) => {
    var nameReg = /^\S*$/;
    if (!nameReg.test($name)) { return false; }
    else { return true; }
  };

  handleChange = e => {
    if (!this.validateName(e.target.value)) { return false }
    this.setState({ [e.target.name]: e.target.value });
  };

  async componentDidMount() {
    if (!this.props.match.params.name) { return; }
    else {
      this.setState({ editMode: true })
      let name = this.props.match.params.name
      try {
        const suppResp = await directoryGetOne({ type: 'customer', name })
        this.setState({ isEdit: suppResp.data.record }, () => this.editData(this.state.isEdit))
      } catch (error) {
        console.error(error)
        // this.props.history.push('/customers-list'); 
      }
    }

  }


  handleSubmit = async () => {
    this.setState({ isLoading: true })
    let { suppConfigSelectError, errorValid, collapse, supplierArray, editMode, isEdit, isLoading, errorData, ...data } = this.state;
    let errors = {};
    let customer = {};

    customer.name = data.name;
    customer.email = data.email
    customer.password = data.password
    customer.Api_Key = data.Api_Key
    customer.schedule = {
      running: false,
      enabled: data.enabled,
      lastIterationStatus: "-",
      lastIteration: "-",
      nextIteration: "-",
      interval: data.interval ? parseInt(data.interval) : "-"
    };

    customer.type = "customer";

    for (let value of Object.entries(customer)) {
      if (value[1] == undefined || value[1] == null || value[1] == "") {
        errors[value[0]] = true;
        this.setState({ ...this.state, errorValid: errors, isLoading: false });
      }
    }

    for (let value of Object.entries(customer.schedule)) {
      if (value[1] == undefined || value[1] == null || value[1] == "") {
        errors[value[0]] = true;
        this.setState({ ...this.state, errorValid: errors, isLoading: false });
      }
    }

    let activeSuppliers = this.props.suppConfig.map(a => a.name)

    // checking if all goes well, then do post the form data
    if (!activeSuppliers.length) {
      this.setState({ suppConfigSelectError: true })
      setTimeout(() => { this.setState({ suppConfigSelectError: false }) }, 5000);
      return;
    }
    else if (
      !customer.name
    ) {
      return;
    } else {
      let customerData = {
        name: customer.name,
        type: 'customer',
        Api_Key: customer.Api_Key,
        email: customer.email,
        password: customer.password,
        activeSuppliers: activeSuppliers,
        supplierConfig: this.props.suppConfig,
        schedule: customer.schedule,
        status: true
      }
      try {
        let params = { type: 'customer', name: this.props.match.params.name, body: JSON.stringify(customerData) }
        const res = (this.state.editMode) ? await directoryEditOne(params) : await directory(params.body);
        if (res.data.response.success) {
          this.setState({ isLoading: false })
          this.props.history.push("/list");
        }
      } catch (error) {
        if (error && error.response) {
          this.setState({ errorData: error.response.data, isLoading: false });
          window.scrollTo(0, 0);
        }
      }
    }
  };

  // function for showing errors  
  showError() {
    let { errorData } = this.state;
    if (errorData && errorData.error.supplierName) {
      return errorData.error.supplierName;
    } else if (errorData && errorData.message) {
      return `${errorData.message} Please provide all values correctly!`;
    }
  }

  // show hide supplier toggle buttons
  toggle = () => {
    this.setState({ collapse: !this.state.collapse });
  };

  // reseting state after response
  resetState = () => {
    this.setState({
      keyToggle: false,
      passwordToggle: false,
      supplierArray: [],
      collapse: false,
      enabled: false,
      errorData: null,
      name: "",
      email: "",
      password: "",
      Api_Key: "",
      interval: "",
      minutes: "",
      errorValid: {
        name: false,
        email: false,
        password: false,
        key: false,
        activeSuppliers: false,
        interval: false
      }
    });
  }

  render() {
    const { keyToggle, supplierArray, errorValid, suppConfigError, suppConfigSelectError, isEdit } = this.state;
    return (
      <div>
        <Paper elevation={3} className="BoxPanel">
          <p className="paraBottom">Essential Information</p>
          <div className="inputDivCol">
            <Row>
              <Col md="2">
                <label htmlFor="feFirstName" className="labelStyle">Name</label>
              </Col>
              <Col md="6">
                <FormInput
                  value={this.state.name}
                  invalid={errorValid.name}
                  id="name"
                  name="name"
                  placeholder="Enter Customer Name"
                  onChange={this.handleChange}
                  style={{ marginBottom: "15px" }}
                />
              </Col>
            </Row>
            <Row>
              <Col md="2">
                <label htmlFor="feFirstName" className="labelStyle">Email</label>
              </Col>
              <Col md="6" className="form-group">
                <FormInput
                  value={this.state.email}
                  invalid={errorValid.email}
                  id="email"
                  name="email"
                  placeholder="example@mail.com"
                  onChange={this.handleChange}
                />
              </Col>
            </Row>
            <Row>
              <Col md="2">
                <label htmlFor="feFirstName" className="labelStyle">Password</label>
              </Col>
              <Col md="6" className="form-group">
                <FormInput
                  value={this.state.password}
                  invalid={errorValid.password}
                  id="password"
                  name="password"
                  type={this.state.passwordToggle ? "text" : "password"}
                  placeholder="**********"
                  onChange={this.handleChange}
                  readOnly={this.state.passwordToggle ? true : false}
                />
              </Col>
              <a href="javascript:void(0)" style={{ textDecoration: "none" }} onClick={() => {
                const password = Keygen(10)
                this.setState({
                  password,
                  passwordToggle: !this.state.passwordToggle
                })
              }}>{this.state.passwordToggle ? "Cancel" : "Auto generate"}</a>
            </Row>
            <Row>
              <Col md="2">
                <label htmlFor="feFirstName" className="labelStyle">Api Key</label>
              </Col>
              <Col md="2">
                <Button theme="accent" onClick={() => {
                  const key = Keygen(20)
                  this.setState({
                    Api_Key: key
                  })
                }}>
                  Generate
                </Button>
              </Col>
              <Col md="4" className="form-group">
                <FormInput
                  type={keyToggle ? "text" : "password"}
                  class="form-control"
                  value={this.state.Api_Key}
                  id="exampleInput"
                  invalid={errorValid.Api_Key}
                  readOnly
                  style={{ fontSize: 15 }}
                />
              </Col>
              <a href="javascript:void(0)" style={{ textDecoration: "none" }} onClick={() => this.setState({ keyToggle: !keyToggle })}>{keyToggle ? "Hide" : "Show"}</a>
            </Row>
          </div>
        </Paper>
        <Paper elevation={3} className="BoxPanel" style={{ marginTop: "15px" }}>
          <p className="paraBottom">Setup Retrieve Schedule</p>
          <div className="inputDivCol">
            <Row>
              <Col md="2">
                <Switch
                  onChange={(e) => {
                    this.setState({ enabled: e.target.checked })
                  }}
                  color="primary"
                  name="enabled"
                  checked={this.state.enabled}
                  inputProps={{ 'aria-label': 'primary checkbox' }}
                />
              </Col>
              <Col md="2">
                <label htmlFor="feFirstName">Interval :</label>
              </Col>
              {/* <Col md="1" className="form-group">
                <FormInput
                  value={this.state.day}
                  invalid={errorValid.password}
                  id="day"
                  name="day"
                  type="number"
                  placeholder="**********"
                  onChange={this.handleChange}
                  readOnly={!this.state.enabled}
                />
              </Col> */}
              {/* <Col md="1">
                <label htmlFor="feFirstName">Day</label>
              </Col>
              <Col md="1" className="form-group">
                <FormInput
                  value={this.state.hour}
                  invalid={errorValid.hour}
                  id="hour"
                  name="hour"
                  type="number"
                  placeholder="**********"
                  onChange={this.handleChange}
                  readOnly={!this.state.enabled}
                />
              </Col>
              <Col md="1">
                <label htmlFor="feFirstName">Hour</label>
              </Col>*/}
              <Col md="2" className="form-group">
                <FormInput
                  value={this.state.minutes}
                  invalid={errorValid.minutes}
                  id="minutes"
                  name="minutes"
                  type="number"
                  onChange={(e) => {
                    const secounds = e.target.value * 60;
                    this.setState({
                      interval: secounds
                    })
                  }}
                  readOnly={!this.state.enabled}
                />
              </Col>
              <Col md="1">
                <label htmlFor="feFirstName" className="">Minutes</label>
              </Col>
            </Row>
          </div>
        </Paper>
        <Paper elevation={3} className="BoxPanel" style={{ marginTop: "15px" }}>
          <p className="paraBottom">Add Suppliers</p>
          <div className="inputDivCol">
            <Row form>
              <Col className="form-group">
                {
                  isEdit ? <CustomerSupplierManage supplierConfig={isEdit} /> : <CustomerSupplierManage />
                }
              </Col>
            </Row>
          </div>
        </Paper>
        <div className="BoxPanelBottom" >
          <Row style={{ float: "right", width: "350px" }}>
            <Snackbar open={suppConfigSelectError}>
              <Alert variant="filled" severity="error">
                Please Provide Configuration on Atleast one Customer Supplier!
              </Alert>
            </Snackbar>
            <Col style={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                disabled={this.state.isLoading}
                onClick={this.handleSubmit}
                style={{ marginTop: "10px" }}
                theme="accent"
              >
                Create
                </Button>
              <Button
                disabled={this.state.isLoading}
                onClick={this.resetState}
                style={{ marginTop: "10px" }}
                theme="danger"
              >
                Clear
                </Button>
              <Button
                disabled={this.state.isLoading}
                onClick={() => this.props.history.push("/customers-list")}
                style={{ marginTop: "10px" }}
                theme="warning"
              >
                Cancel
                </Button>
            </Col>
          </Row>
        </div>
      </div >

    );
  }


  editData(supplier) {
    if (supplier) {
      this.setState({
        name: supplier.name,
        running: supplier.schedule.running,
        lastIterationStatus: supplier.schedule.lastIterationStatus,
        interval: supplier.schedule.interval,
        enabled: supplier.schedule.enabled,
        lastIteration: supplier.schedule.lastIteration,
        nextIteration: supplier.schedule.nextIteration,
      })
    }
  }
}

UserAccountDetails.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

const mapStateToProps = state => {
  return { suppConfig: state.SuppConfigReducer.suppConfig };
};

export default withRouter(connect(mapStateToProps, null)(UserAccountDetails));
  // <Card small className="mb-4">

  // <ListGroup flush>
  // <ListGroupItem className="p-3">
  // <Row>
  // <Col>
  // <Form>
  //   <p style={{ color: "red", fontWeight: "bold" }}>
  //     {this.showError()}
  //   </p>


  //   {supplierArray &&
  //     supplierArray.map((supplierName, index) => {
  //       return (
  //         <div key={index}>
  //           <Button
  //             theme="info"
  //             style={{ margin: "10px 0px" }}
  //             onClick={this.toggle}
  //           >
  //             {"Add  " + supplierName + "  Config"}
  //           </Button>

  //           <Collapse open={this.state.collapse}>
  //             <div className="p-3 mt-3 border rounded">
  //               <div style={{ display: "flex" }}>

  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="name">Supplier Name</label>
  //                   <FormInput
  //                     disabled={true}
  //                     value={supplierName}
  //                     id={supplierName + "supplierName"}
  //                     name={supplierName + "supplierName"}
  //                     type="text"
  //                     placeholder="Enter Supplier Name"
  //                     className="mb-2"
  //                   />
  //                 </Col>

  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="KeyLocation">
  //                     Key Location
  //                   </label>
  //                   <FormInput
  //                     invalid={
  //                       errorValid[supplierName + "KeyLocation"]
  //                     }
  //                     id={supplierName + "KeyLocation"}
  //                     name={supplierName + "KeyLocation"}
  //                     type="text"
  //                     placeholder="Enter Key Location"
  //                     className="mb-2"
  //                     onChange={e => {
  //                       this.handleChange(e);
  //                       this.setState({
  //                         [supplierName +
  //                         "supplierName"]: supplierName
  //                       });
  //                     }}
  //                   />
  //                 </Col>
  //               </div>

  //               <div style={{ display: "flex" }}>
  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="name">
  //                     Key Location Type
  //                   </label>
  //                   <FormInput
  //                     disabled={true}
  //                     value="s3"
  //                     id={supplierName + "KeyLocationType"}
  //                     name={supplierName + "KeyLocationType"}
  //                     type="text"
  //                     placeholder="Key Location Type"
  //                     className="mb-2"
  //                   />
  //                 </Col>
  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="KeyLocation">
  //                     sftp Prefix
  //                   </label>
  //                   <FormInput
  //                     id={supplierName + "sftpPrefix"}
  //                     name={supplierName + "sftpPrefix"}
  //                     type="text"
  //                     placeholder="Enter sftp Prefix"
  //                     className="mb-2"
  //                     onChange={e => {
  //                       this.handleChange(e);
  //                       this.setState({
  //                         [supplierName + "KeyLocationType"]: "s3"
  //                       });
  //                     }}
  //                   />
  //                 </Col>
  //               </div>

  //               <div style={{ display: "flex" }}>
  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="name">sftp Username</label>
  //                   <FormInput
  //                     id={supplierName + "sftpUsername"}
  //                     name={supplierName + "sftpUsername"}
  //                     type="text"
  //                     placeholder="Enter sftp Username"
  //                     className="mb-2"
  //                     onChange={this.handleChange}
  //                   />
  //                 </Col>
  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="KeyLocation">
  //                     ftp Username
  //                   </label>
  //                   <FormInput
  //                     invalid={
  //                       suppConfigError &&
  //                       suppConfigError.ftpUsername
  //                     }
  //                     id={supplierName + "ftpUsername"}
  //                     name={supplierName + "ftpUsername"}
  //                     type="text"
  //                     placeholder="Enter ftp Username"
  //                     className="mb-2"
  //                     onChange={this.handleChange}
  //                   />
  //                 </Col>
  //               </div>

  //               <div style={{ display: "flex" }}>
  //                 <Col md="6" className="form-group">
  //                   <label htmlFor="name">ftp Password</label>
  //                   <FormInput
  //                     invalid={
  //                       suppConfigError &&
  //                       suppConfigError.ftpPassword
  //                     }
  //                     id={supplierName + "ftpPassword"}
  //                     name={supplierName + "ftpPassword"}
  //                     type="text"
  //                     placeholder="Enter ftp Password"
  //                     className="mb-2"
  //                     onChange={this.handleChange}
  //                   />
  //                 </Col>
  //               </div>
  //             </div>
  //           </Collapse>
  //         </div>
  //       );
  //     })}

  //   {/* ******* Schedule Section********** */}
  //   <div className="p-3 mt-3 border rounded">     


  //     <h4 className='m-3'>Schedule</h4>

  //     <div style={{ display: "flex" }}>
  //       <Col md="6" className="form-group">
  //         <label htmlFor="accessType">Running </label>
  //         <FormSelect name="running" onChange={this.handleChange}>
  //           <option value={true}>true</option>
  //           <option value={false}>false</option>
  //         </FormSelect>
  //       </Col>

  //       <Col md="6" className="form-group">
  //         <label htmlFor="accessType">Enabled </label>
  //         <FormSelect name="enabled" onChange={this.handleChange}>
  //           <option value={true}>true</option>
  //           <option value={false}>false</option>
  //         </FormSelect>
  //       </Col>
  //     </div>

  //     <div style={{ display: "flex" }}>
  //       <Col md="6" className="form-group">
  //         <label htmlFor="name">Last Iteration Status</label>
  //         <FormInput
  //           invalid={errorValid.lastIterationStatus}
  //           value={this.state.lastIterationStatus}
  //           id="lastIterationStatus"
  //           name="lastIterationStatus"
  //           type="text"
  //           placeholder="Enter Last Iteration Status"
  //           className="mb-2"
  //           onChange={this.handleChange}
  //         />
  //       </Col>

  //       <Col md="6" className="form-group">
  //         <label htmlFor="name">Next Iteration</label>
  //         <FormInput
  //           invalid={errorValid.nextIteration}
  //           value={this.state.nextIteration}
  //           id="nextIteration"
  //           name="nextIteration"
  //           type="number"
  //           placeholder="Enter Next Iteration"
  //           className="mb-2"
  //           onChange={this.handleChange}
  //         />
  //       </Col>
  //     </div>

  //     <div style={{ display: "flex" }}>
  //       <Col md="6" className="form-group">
  //         <label htmlFor="name">Last Iteration</label>
  //         <FormInput
  //           invalid={errorValid.lastIteration}
  //           value={this.state.lastIteration}
  //           id="lastIteration"
  //           name="lastIteration"
  //           type="number"
  //           placeholder="Enter Last Iteration"
  //           className="mb-2"
  //           onChange={this.handleChange}
  //         />
  //       </Col>

  //       <Col md="6" className="form-group">
  //         <label htmlFor="name">Interval</label>
  //         <FormInput
  //           invalid={errorValid.interval}
  //           value={this.state.interval}
  //           id="interval"
  //           name="interval"
  //           type="number"
  //           placeholder="Enter Next Interval"
  //           className="mb-2"
  //           onChange={this.handleChange}
  //         />
  //       </Col>
  //     </div>

  //   </div>
  //   <Button
  //     disabled={this.state.isLoading}
  //     onClick={this.handleSubmit}
  //     style={{ marginTop: "10px" }}
  //     theme="accent"
  //   >
  //     Submit Record
  //   </Button>
  // </Form>

  // {/* showing snackbar if customer supplier is not seleted */}


  // </Col>
  // </Row>
  // </ListGroupItem>
  // </ListGroup>
  // </Card>