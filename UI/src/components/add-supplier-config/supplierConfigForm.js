import React, { useEffect } from "react"
import { Card, CardBody, Form, FormInput, FormGroup, Col, Button, Row } from "shards-react"

import "react-quill/dist/quill.snow.css"
import "../../assets/quill.css"


const SupplierConfig = props => {
  const [state, setState] = React.useState({
    name: '',
    productLoadQueryString: '',
    productIdQueryString: '',
    productIdentifier: '',
    textField: '',
    filter: '',
    convert: '',
    headers: '',
    mandatory: '',
    transform: '',
    errorValid: {
      productLoadQueryString: false,
      productIdQueryString: false,
      productIdentifier: false,
      textField: false,
      filter: false,
      convert: false,
      headers: false,
      mandatory: false,
      transform: false,
    }
  })
  
  const validateName = ($name) => {
    var nameReg = /^\S*$/;
    if (!nameReg.test($name)) {  return false; } 
    else {  return true; }
  };

  const handleInput = e => {
    if (!validateName(e.target.value)) { return false } 
    setState({ ...state, [e.target.name]: e.target.value })
  }; 

  // const handleInput = e => {
  //   setState({ ...state, [e.target.name]: e.target.value })
  // }

  const handleSubmit = e => {

    let { errorValid, ...data } = state
    let errors = {};

    let body = data
    body.headers = state.headers && state.headers.split(",")
    body.mandatory = state.mandatory && state.mandatory.split(",")
    body.transform = state.transform && state.transform.split(",")
    body.type = "supplierConfig"
    body.checkedB = false

    for (let value of Object.entries(body)) {
      if (value[1] == null || value[1] == "") {
        errors[value[0]] = true;
        setState({ ...state, errorValid: errors });
      }
    }

    // if any error exist then don't go to editor component.
    if(Object.values(errors).length < 2) {
      props.suppData(body)
    } 
    // else {
    //   console.error(state.errorValid, errors)
    // }
  }
  
  return (
    <Card small className="mb-3">
      <CardBody>
        <Form>
          <Row>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Name <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.name}
                  invalid={state.errorValid.name}
                  id="name"
                  name="name"
                  type="text"
                  placeholder="Enter Supplier Name"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label htmlFor="productLoadQueryString">Product Load Query String <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.productLoadQueryString}
                  invalid={state.errorValid.productLoadQueryString}
                  id="productLoadQueryString"
                  name="productLoadQueryString"
                  type="text"
                  placeholder="Enter Product Load Query String"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Product Id Query String<span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.productIdQueryString}
                  invalid={state.errorValid.productIdQueryString}
                  id="productIdQueryString"
                  name="productIdQueryString"
                  type="text"
                  placeholder="Enter Product Id Query String"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Product Identifier <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.productIdentifier}
                  invalid={state.errorValid.productIdentifier}
                  id="productIdentifier"
                  name="productIdentifier"
                  type="text"
                  placeholder="Enter Product Identifier"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Text Field <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.textField}
                  invalid={state.errorValid.textField}
                  id="textField"
                  name="textField"
                  type="text"
                  placeholder="Enter Text Field"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Convert <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.convert}
                  invalid={state.errorValid.convert}
                  id="convert"
                  name="convert"
                  type="text"
                  placeholder="Enter Convert Ext"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Filter <span style={{ color: "red" }}>*</span></label>
                <FormInput
                  value={state.filter}
                  invalid={state.errorValid.filter}
                  id="filter"
                  name="filter"
                  type="text"
                  placeholder="Enter filter"
                  className="mb-2"
                  onChange={handleInput}
                />
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Headers</label>
                <FormInput
                  value={state.headers}
                  invalid={state.errorValid.headers}
                  id="headers"
                  name="headers"
                  type="text"
                  placeholder="Enter Headers"
                  className="mb-2"
                  onChange={handleInput}
                />
                <label>Format : value,value,value,.... </label>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Mandatory</label>
                <FormInput
                  value={state.mandatory}
                  invalid={state.errorValid.mandatory}
                  id="mandatory"
                  name="mandatory"
                  type="text"
                  placeholder="Enter Mandatory"
                  className="mb-2"
                  onChange={handleInput}
                />
                <label>Format : value,value,value,.... </label>
              </FormGroup>
            </Col>
            <Col md="6">
              <FormGroup>
                <label htmlFor="name">Transform</label>
                <FormInput
                  value={state.transform}
                  invalid={state.errorValid.transform}
                  id="transform"
                  name="transform"
                  type="text"
                  placeholder="Enter Transform"
                  className="mb-2"
                  onChange={handleInput}
                />
                <label>Format : value,value,value,.... </label>
              </FormGroup>
            </Col>
          </Row>
        </Form>
        <Button onClick={handleSubmit}>Next</Button>
      </CardBody>
    </Card>
  )
}


export default SupplierConfig
