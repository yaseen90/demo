import React, { useState, useEffect } from 'react'

import { Card, CardBody, Button } from "shards-react"
import Swal from 'sweetalert2'
import JSONInput from 'react-json-editor-ajrm'
import locale from 'react-json-editor-ajrm/locale/en'
 
import { directory, directoryGetOne, directoryEditOne } from "../../config/api/directoryApi"


let loading = false
const SupplierConfigJSON = props => {
	const [state, setState] = useState({ suppData: null, loading: false })

	useEffect(() => {
		if(props.isEdit) { 
			let name = props.isEdit
			async function fetchSuppConfigData() {
				const suppConfigResp = await directoryGetOne({ type: 'supplierConfig', name })
				setState(state => ({ ...state, suppData: suppConfigResp.data.record }))
			}
			fetchSuppConfigData()
		} else {
			setState(state => ({ ...state, suppData: props.suppData }))
		}
	}, [])


	function handleChange(params) {
		setState({ ...state, suppData: params.jsObject})
	}

	const handleSubmit = async (e) => {
		setState({ ...state, loading: true })
		let params = { type: 'supplierConfig', name: state.suppData.name, body: JSON.stringify(state.suppData)  }
    try {
      const response = (props.isEdit) ? await directoryEditOne(params) : await directory(params.body)
      if (response.data.response.success) {
				setState({ ...state, loading: false })
				Swal.fire("Successfull", "Supplier Record Inserted", "success")
				window.location.href = '/supplier-list';
      }
    } catch (error) {    
      Swal.fire("Failed", "There's something wrong", "error")
		}

	}

	return (
		<Card>
			<CardBody>
				<JSONInput
					width='100%'
					id='uuid'
					placeholder={state.suppData}
					onChange={handleChange}
					theme="light_mitsuketa_tribute"
					locale={locale}
					height='500px'
				/>
				<Button disabled={state.loading} onClick={handleSubmit}>Submit</Button>
			</CardBody>
		</Card>
	)
}

export default SupplierConfigJSON