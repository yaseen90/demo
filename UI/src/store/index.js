import { createStore, applyMiddleware } from 'redux'
import rootReducer from './reducer/rootReducer'
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import SuppConfigReducer from './reducer/SuppConfigReducer'

const persistConfig = {
    key: 'root',
    blacklist: ['SuppConfigReducer'],
    storage,
}
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(persistedReducer, applyMiddleware(thunk))
const persistor = persistStore(store, { blacklist: ['SuppConfigReducer'] })

export {
    store, 
    persistor
}