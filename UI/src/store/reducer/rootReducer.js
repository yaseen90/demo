import { combineReducers } from 'redux'
import authReducers from './index'
import SuppConfigReducer from "./SuppConfigReducer";

export default combineReducers({
    authReducers,
    SuppConfigReducer
})