const initial_state = {
  suppConfig: []
};

const SuppConfigReducer = (state = initial_state, action) => {
  switch (action.type) {
    case "ADD_SUPPLIER_CONFIG": {
      console.log('Actions...', action.suppConfig)
      return { ...state, suppConfig: action.suppConfig };
    }
    case "REMOVE_SUPPLIER_CONFIG": {
      return { ...state, suppConfig: null };
    }
    default: {
      return state;
    }
  }
};

export default SuppConfigReducer;
