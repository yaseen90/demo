const updateUser = user => {
  return {
    type: "UPDATE_USER",
    user
  };
};

const removeUser = () => {
  return {
    type: "REMOVE_USER"
  };
};

const AddConfig = suppConfig => {
  return {
    type: "ADD_SUPPLIER_CONFIG",
    suppConfig
  };
};

const RemoveConfig = suppConfig => {
  return {
    type: "REMOVE_SUPPLIER_CONFIG",
  };
};
// const AwardData = awardData => {
//   return {
//     type: "Award_Data",
//     awardData
//   };
// };

export { updateUser, removeUser, AddConfig, RemoveConfig };
