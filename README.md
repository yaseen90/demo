**Dev Deployment Product**
./deploy product dev

**Dev Deployment Jobstore**
./deploy jobstore dev

**Dev Deployment Directory**
./deploy directory dev

**Dev Deployment SupplierLink**
./deploy supplierlink dev


**Test Deployment Product**
./deploy product test

**Test Deployment Jobstore**
./deploy jobstore test

**Test Deployment Directory**
./deploy directory test

**Test Deployment SupplierLink**
./deploy supplierlink test


**Production Deployment Product**
./deploy product prod

**Production Deployment Jobstore**
./deploy jobstore prod

**Production Deployment Directory**
./deploy directory prod

**Production Deployment SupplierLink**
./deploy supplierlink prod



Export (Since DateTime):
Method: POST 
URL: /customer/export
Body: { "customerName": "CustomerName", "query": "since", "startDate": "2019-07-24 00:00:00"}   